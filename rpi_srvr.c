/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_srvr.c
 *  Purpose:            Dynamic HTTP server
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/


#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_http.h"
#include "http_func.h"
#include "rpi_func.h"
#include "rpi_page.h"
#include "rpi_json.h"
//
#include "rpi_srvr.h"

#define USE_PRINTF
#include <printx.h>

//
// Local prototypes
//
static void    srvr_Execute                  (void);
static bool    srvr_DynPageCncCommand        (NETCL *, int);
//
static bool    srvr_DynBuildHtmlHeader       (NETCL *);
static bool    srvr_DynBuildHtmlEnd          (NETCL *);
//
static bool    srvr_CallbackConnection       (NETCL *);
static void    srvr_HandleCompletion         (NETCL *);
static int     srvr_HandleHttpReply          (NETCL *);
static int     srvr_HandleHttpReplyHtml      (NETCL *);
static int     srvr_HandleHttpReplyJson      (NETCL *);
static int     srvr_HandleHttpRequest        (NETCL *);
static void    srvr_InitDynamicPages         (int);
//
static void    srvr_ReceiveSignalInt         (int);
static void    srvr_ReceiveSignalTerm        (int);
static void    srvr_ReceiveSignalUser1       (int);
static void    srvr_ReceiveSignalUser2       (int);
static void    srvr_ReceiveSignalSegmnt      (int);
static void    srvr_ReceiveSignalPipe        (int);
static bool    srvr_SignalRegister           (sigset_t *);
//
// Enums and dynamic LUTs (Global DYN pages)
//
enum
{
   #define  EXTRACT_DYN(a,b,c,d,e,f)   a,
   #include "pages.h"
//
// Enums and dynamic LUTs (Local DYN pages)
//
   #include "pagescnc.h"
   #undef EXTRACT_DYN
   NUM_HUE_DYNS
};
//
// Local DYN pages
//
static const NETDYN stDynamicPages[] =
{
//    tUrl,    tType,   iTimeout,   iFlags,  pcUrl,   pfDynCb;
   #define  EXTRACT_DYN(a,b,c,d,e,f)   {a,b,c,d,e,f},
   #include "pagescnc.h"
   #undef EXTRACT_DYN
   {  -1,      0,       0,          0,       NULL,    NULL  }
};

//
// External HTTP data
//
extern const char *pcHttpResponseHeader;
extern const char *pcWebPageStart;
extern const char *pcWebPagePreStart;
extern const char *pcWebPagePreEnd;
extern const char *pcWebPageStartRefresh;
extern const char *pcWebPageCenter;
extern const char *pcWebPageDefault;
extern const char *pcWebPageFontStart;
extern const char *pcWebPageFontEnd;

extern const char *pcWebPageNotImplemented;
extern const char *pcWebPageLineBreak;
extern const char *pcWebPageEnd;
extern const char *pcWebPageTitle;
//
// Misc tex strings/legends
//
static const char *pcCourierNew        = "Courier New";
static const char *pcWebPageTitleCnc   = "Rpi CNC Draw";
//
static bool    fThreadRunning          = TRUE;

/*------  Local functions separator ------------------------------------
_______________GLOBAL_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//
//  Function:   SRVR_Init
//  Purpose:    HTTP server init
//
//  Parms:      
//  Returns:    Startup code GLOBAL_XXX_INI
//
int SRVR_Init(void)
{
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         GLOBAL_PidPut(PID_SRVR, tPid);
         break;

      case 0:
         // Child: RPI HTTP-server
         GEN_Sleep(500);
         srvr_Execute();
         LOG_Report(0, "SVR", "srvr-Init():Exit normally");
         exit(EXIT_CC_OKEE);
         break;

      case -1:
         // Error
         GEN_Printf("srvr-Init(): Error!"CRLF);
         LOG_Report(errno, "SVR", "srvr-Init():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(GLOBAL_SVR_INI);
}

/*------  Local functions separator ------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:   srvr_Execute
//  Purpose:    Run the Raspberry Pi HTTP mini-server as a deamon
//
//  Parms:
//  Returns:
//
static void srvr_Execute(void)
{
   int         iPort, iCnxs, iOpt;
   sigset_t    tBlockset;
   char        pcPort[6];
   NETCON     *pstNet=NULL;

   GLOBAL_SemaphoreInit(PID_SRVR);
   if(srvr_SignalRegister(&tBlockset) == FALSE) 
   {
      LOG_Report(errno, "SVR", "srvr-Execute():Exit fork ERROR:");
      exit(EXIT_CC_GEN_ERROR);
   }
   //
   // Set up server socket 
   //
   iPort = *((int*) GLOBAL_GetParameter(PAR_HTTP_PORT));
   GEN_SNPRINTF(pcPort, 6, "%d", iPort);
   //
   pstNet = NET_Init(pstMap->G_pcWwwDir, pstMap->G_pcWwwDir);
   if( NET_ServerConnect(pstNet, pcPort, HTTP_PROTOCOL) == -1)
   {
      LOG_printf("srvr-Execute():Server connect failed on port %d: exiting" CRLF, iPort);
      LOG_Report(errno, "SVR", "srvr-Execute():Exit NET connect port %d ERROR:", iPort);
      exit(EXIT_CC_GEN_ERROR);
   }
   //
   // Turn ON verbose messages
   //
   pstNet->iVerbose = NET_VERBOSE_TPKT;
   HTTP_InitDynamicPages(iPort);
   srvr_InitDynamicPages(iPort);
   GLOBAL_GetHostName();
   GLOBAL_PidSaveGuard(PID_SRVR, GLOBAL_SVR_INI);
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_SVR_INI);
   //
   // Init ready
   // Wait for Host Run/Execute
   //
   while(fThreadRunning)
   {
      iOpt = GLOBAL_SignalWait(PID_SRVR, GLOBAL_HST_ALL_RUN, 1000);
      if(iOpt == 0) break;
   }
   PRINTF("srvr-Execute():Running" CRLF);
   //
   //
   // Run/Execute
   // Main loop of the Guard: wait for the semaphore posted by the parent 
   //
   //
   // Wait for data
   //
   while(fThreadRunning)
   {
      NET_BuildConnectionList(pstNet);
      //
      // Wait for http data
      //
      if(NET_WaitForConnection(pstNet, HTTP_CONNECTION_TIMEOUT_MSECS) > 0)
      {
         //PRINTF("srvr-Execute(): Socket has data" CRLF); 
         //
         // There is data on some of the sockets
         //
         GLOBAL_SetSignalNotification(PID_UDP, GLOBAL_SVR_UDP_RUN);
         NET_HandleSessions(pstNet, &srvr_CallbackConnection, pstMap->G_pcHostIp, MAX_ADDR_LEN);
         PRINTF("srvr-Execute():Connected to IP [%s]" CRLF, pstMap->G_pcHostIp);
      }
      else
      {
         //
         // Timeout: check 24 hour activity log
         // Check network stack
         //
         if((iCnxs = NET_ReportServerStatus(pstNet, NET_CHECK_COUNT)) == 0)
         {
            //
            // No connections available: reset all expired ones !
            //
            iCnxs = NET_ReportServerStatus(pstNet, NET_CHECK_LOG|NET_CHECK_FREE);
            LOG_Report(0, "SVR", "srvr-Execute(): Now %d connections free again", iCnxs);
         }
      }
   }
   //
   // Close all socket connections
   //
   NET_ServerTerminate(pstNet);
   GLOBAL_SemaphoreDelete(PID_SRVR);
}

//
// Function:   srvr_DynPageCncCommand
// Purpose:    Handle the CNC commands
//             all
//             cnc
//             cnc.json
//             cnc.html
// Parms:
// Returns:    
// Note:       
//
static bool srvr_DynPageCncCommand(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         // 
         // Put out the HTML header, start tag and the rest
         //         
         srvr_DynBuildHtmlHeader(pstCl);
         srvr_DynBuildHtmlEnd(pstCl);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         //
         // Show Hue JSON parameters
         //
         GEN_STRCPY(pstMap->G_pcCommand, "cnc");
         fCc = RPI_BuildJsonMessageArgs(pstCl, PAR_CNC);
         break;
   }
   return(fCc);
}


/*------  Local functions separator ------------------------------------
____HTML_BUILD_FUNCTIONS_______(){};
------------------------------x----------------------------------------*/

//
// Function:   srvr_DynBuildHtmlHeader
// Purpose:    Build the HUE header data to be send back through the socket
//
// Parms:      Socket descriptor
// Returns:    TRUE
//
static bool srvr_DynBuildHtmlHeader(NETCL *pstCl)
{
   // 
   // Put out the HTML header, start tag and the rest
   //         
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitleCnc);
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   HTTP_BuildLineBreaks(pstCl, 1);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontStart, pcCourierNew);
   HTTP_BuildGeneric(pstCl, pcWebPageFontEnd);
   HTTP_BuildLineBreaks(pstCl, 1);
   return(TRUE);
}

//
// Function:   srvr_DynBuildHtmlEnd
// Purpose:    Build the HUE end data to be send back through the socket
//
// Parms:      Socket descriptor
// Returns:    TRUE if OKee
//
static bool srvr_DynBuildHtmlEnd(NETCL *pstCl)
{
   bool     fCc;

   // 
   // Put out the HTML end
   //         
   fCc = HTTP_BuildGeneric(pstCl, pcWebPageEnd);
   return(fCc);
}


/*------  Local functions separator ------------------------------------
___________JSON_BUILD_FUNCTIONS(){};
------------------------------x----------------------------------------*/


/*------  Local functions separator ------------------------------------
_________________MISC_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//  
// Function    :  srvr_CallbackConnection
// Description :  Handle the connection list
// 
// Parameters  :  Socket descriptor
// Returns     :  TRUE if OKee
// Note:          NET_HandleSessions(..) will callback here as soon as a complete
//                HTTP Get/POST request has been acquired through the socket.
//                This Callback will verify if the HTTP request concerns:
//                o a STATIC  page: Try to read it from file and return to the socket
//                o a DYNAMIC page: Lookup the page and forward to the registered handler
//
//                The handler will respond back with a SIGUSR2 signal with the appropriate
//                notification set.
//  
//  
static bool srvr_CallbackConnection(NETCL *pstCl)
{
   bool     fCc=TRUE;
   int      iOpt, iTimeout;

   PRINTF("srvr-CallbackConnection()" CRLF);
   //
   // Data came in from the socket: check if we have something to do
   //
   if( (iTimeout = srvr_HandleHttpRequest(pstCl)) > 0) 
   {
      //
      // Command has been forwarded to a handler. It will report back through the semaphore (via SIGUSR2) 
      // Wait the timeout and act accordingly
      //
      iOpt = GLOBAL_SemaphoreWait(PID_SRVR, iTimeout);
      switch(iOpt)
      {
         case 0:
            //
            // Signals for the HTTP Server
            //
            GLOBAL_GetSignalNotification(PID_SRVR, GLOBAL_GRD_ALL_RUN);
            break;

         default:
         case -1:
            LOG_Report(errno, "SVR", "srvr-CallbackConnection():ERROR sem_timedwait");
            PRINTF("srvr-CallbackConnection():ERROR Sem_timedwait(errno=%d)" CRLF, errno);
            fCc = FALSE;
            break;

         case 1:
            //
            // Timeout:
            //
            LOG_Report(errno, "SVR", "srvr-CallbackConnection():sem_timedwait");
            PRINTF("srvr-CallbackConnection():Timeout(errno=%d)" CRLF, errno);
            //
            if( srvr_HandleHttpReply(pstCl) )
            {
               //
               // The command timed out: specific reply has already been send back to the app
               //
            }
            else
            {
               //
               // Nothing specific can be done: generate a generic response
               //
               srvr_HandleCompletion(pstCl);
            }
            break;
      }
   }
   NET_FlushCache(pstCl);
   //
   // Session is finished: close connection
   //
   PRINTF("srvr-CallbackConnection():Ready, socket disconnected." CRLF);
   return(fCc);
}

//  
//  Function    : srvr_HandleCompletion
//  Description : Handle the HTTP server completion code
//  
//  Parameters  : Socket descriptor
//  Returns     : Timeout in msecs
//  
static void srvr_HandleCompletion(NETCL *pstCl)
{
   switch(GLOBAL_Status(GEN_STATUS_ASK))
   {
      case GEN_STATUS_TIMEOUT:
         //PRINTF("srvr-HandleCompletion():TIMEOUT" CRLF);
         RPI_ReportError(pstCl);
         break;

      case GEN_STATUS_ERROR:
         //PRINTF("srvr-HandleCompletion():ERROR" CRLF);
         RPI_ReportError(pstCl);
         break;

      case GEN_STATUS_REJECTED:
         //PRINTF("srvr-HandleCompletion():REJECTED" CRLF);
         RPI_ReportBusy(pstCl);
         break;

      case GEN_STATUS_REDIRECT:
         //PRINTF("srvr-HandleCompletion():REDIRECTED" CRLF);
         GLOBAL_Status(GEN_STATUS_IDLE);
         HTTP_DynamicPageHandler(pstCl, pstMap->G_iCurDynPage);
         break;

      default:
         //PRINTF("srvr-HandleCompletion():OKee" CRLF);
         srvr_HandleHttpReply(pstCl);
         break;
   }
}

//  
//  Function    : srvr_HandleHttpReply
//  Description : Request has been completed: handle the reply back to the browser/application
//  
//  Parameters  : Socket descriptor
//  Returns     : 
//  
static int srvr_HandleHttpReply(NETCL *pstCl)
{
   bool     fCc=FALSE;
   FTYPE    tType;

   tType = GEN_GetDynamicPageType(pstMap->G_iCurDynPage);
   switch(tType)
   {
      default:
      case HTTP_HTML:
         fCc = srvr_HandleHttpReplyHtml(pstCl);
         break;

      case HTTP_JSON:
         fCc = srvr_HandleHttpReplyJson(pstCl);
         break;
   }
   return(fCc);
}

//  
//  Function    : srvr_HandleHttpReplyHtml
//  Description : Request has been completed: handle HTML reply back to the browser/application
//  
//  Parameters  : Socket descriptor
//  Returns     : True if handled
//  
static int srvr_HandleHttpReplyHtml(NETCL *pstCl)
{
   //PRINTF("srvr-HandleHttpReplyHtml()" CRLF);
   return(FALSE);
}

//  
//  Function    : srvr_HandleHttpReplyJson
//  Description : Request has been completed: handle JSON reply back to the browser/application
//  
//  Parameters  : Socket descriptor
//  Returns     : True if handled
//  
static int srvr_HandleHttpReplyJson(NETCL *pstCl)
{
   //PRINTF("srvr-HandleHttpReplyJson()" CRLF);
   return(FALSE);
}

//  
//  Function    : srvr_HandleHttpRequest
//  Description : Data came in through the socket: handle it
//  
//  Parameters  : Socket descriptor
//  Returns     : Timeout in msecs
//  
static int srvr_HandleHttpRequest(NETCL *pstCl)
{
   int      iWait=0, iIdx;
   FTYPE    tType;
   pid_t    tPid;
   int      ePid;

   //
   // Handle incoming HTTP GET request:
   //
   //   "GET /index.html HTTP/1.1" CR,LF
   //   "Host: 10.0.0.231" CR,LF
   //   "User-Agent: Mozilla/5.0 (Windows; U; WinGecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)" CR,LF
   //   "Accept: image/png,image/*;q=0.8,*/*;q=0.5" CR,LF
   //   "Accept-Encoding: gzip,deflate" CR,LF
   //   "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7" CR,LF
   //   "Keep-Alive: 115" CR,LF
   //   "Connection: keep-alive" CR,LF
   //   CR,LF
   //
   // Check if the requested URL is a dynamically generated page or if it should
   // be on the filesystem.
   //
   if( (iIdx = HTTP_PageIsDynamic(pstCl)) < 0 )
   {
      // DFD-0-3.3 (RPI Xmt STATIC page)
      // This page is a STATIC page (needs to come from the file system)
      //
      pstMap->G_iCurDynPage = -1;
      PRINTF("srvr-HandleHttpRequest():STATIC page (%s)" CRLF, pstCl->pcUrl);
      HTTP_RequestStaticPage(pstCl);
   }
   else
   {
      //
      // This page is an internal (DYNAMICALLY build) page
      // Check if we can do this ourselves here, or if it need additional
      // handling.
      //
      PRINTF("srvr-HandleHttpRequest():DYNAMIC page (%s)" CRLF, pstCl->pcUrl);
      pstMap->G_iCurDynPage = iIdx;
      //
      tType        = GEN_GetDynamicPageType(iIdx);
      ePid         = GEN_GetDynamicPagePid(iIdx);
      iWait        = GEN_GetDynamicPageTimeout(iIdx);
      //
      // Type is HTTP_HTML or HTTP_JSON
      // HTTP_HTML: ...cam.html?xxxx&-p17-p2....
      // HTTP_JSON: ...cam.jsom?command=xxxx&para1=yyyy&....
      //
      tPid = GLOBAL_PidGet(ePid);
      RPI_CollectParms(pstCl, tType);
      //
      PRINTF("srvr-HandleHttpRequest():Dynamic URL:Idx=%d, Pid=%d, Wait=%d, URL=%s" CRLF, iIdx, tPid, iWait, GEN_GetDynamicPageName(iIdx) );
      //
      // Signal the thread to handle it (if it is not us)
      //
      if(tPid > 0)
      {
         PRINTF("srvr-HandleHttpRequest():Forward request to Pid=%d, [%s]-[%s]" CRLF, tPid, pstCl->pcUrl, pstMap->G_pcCommand);
         GLOBAL_SetSignalNotification(ePid, GLOBAL_SVR_ALL_NFY);
      }
      else
      {
         if(HTTP_DynamicPageHandler(pstCl, iIdx))
         {
            //
            // Dynamic page returns TRUE if the current page request should complete the HTTP request
            //
            PRINTF("srvr-HandleHttpRequest():OKee :[%s]-[%s]" CRLF, pstCl->pcUrl, pstMap->G_pcCommand);
         }
         else
         {
            //
            // No reply was set up: send back error
            //
            PRINTF("srvr-HandleHttpRequest():ERROR:[%s]-[%s]" CRLF, pstCl->pcUrl, pstMap->G_pcCommand);
         }
      }
   }
   return(iWait);
}

// 
// Function:   hue_InitDynamicPages
// Purpose:    Register dynamic webpages
// 
// Parameters: Port
// Returns:    
// Note:       
//
static void srvr_InitDynamicPages(int iPort)
{
   const NETDYN  *pstDyn=stDynamicPages;

   PRINTF("srvr-InitDynamicPages(): port=%d" CRLF, iPort);
   //
   while(pstDyn->pcUrl)
   {
      //PRINTF("srvr-InitDynamicPages():Url=%s" CRLF, pstDyn->pcUrl);
      if(pstDyn->iFlags & DYN_FLAG_PORT)
      {
         //
         // Register for this specific port
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, -1, pstDyn->tType, pstDyn->iTimeout, iPort, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      else
      {
         //
         // Register for all ports
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, -1, pstDyn->tType, pstDyn->iTimeout,     0, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      pstDyn++;
   }
}


/*------  Local functions separator ------------------------------------
_______________SIGNAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:   srvr_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void srvr_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "SVR", "srvr-ReceiveSignalTerm()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_SRVR);
}

//
//  Function:   srvr_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void srvr_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "SVR", "srvr-ReceiveSignalInt()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_SRVR);
}

//
//  Function:   srvr_ReceiveSignalSegmnt
//  Purpose:    Add the SIGSEGV command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void srvr_ReceiveSignalSegmnt(int iSignal)
{
   GLOBAL_SegmentationFault(__FILE__, __LINE__);
   LOG_SegmentationFault(__FILE__, __LINE__);
   exit(EXIT_CC_GEN_ERROR);
}

//
//  Function:   srvr_ReceiveSignalPipe
//  Purpose:    Add the SIGPIPE command in the buffer (Broken pipe)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void srvr_ReceiveSignalPipe(int iSignal)
{
   LOG_Report(errno, "SVR", "srvr-ReceiveSignalPipe()");
}

//
// Function:   srvr_ReceiveSignalUser1
// Purpose:    SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR1 is used to 
//
static void srvr_ReceiveSignalUser1(int iSignal)
{
   GLOBAL_SemaphorePost(PID_SRVR);
}

//
// Function:   srvr_ReceiveSignalUser2
// Purpose:    System callback on receiving the SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR2 is not used
//
static void srvr_ReceiveSignalUser2(int iSignal)
{
}

//
//  Function:   srvr_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool srvr_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGUSR1:
  if( signal(SIGUSR1, &srvr_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &srvr_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &srvr_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &srvr_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  // Segmentation fault
  if( signal(SIGSEGV, &srvr_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr-ReceiveSignalSegmnt(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // Pipe error
  if( signal(SIGPIPE, &srvr_ReceiveSignalPipe) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr-ReceiveSignalPipe(): SIGPIPE ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGPIPE);
  }
  return(fCC);
}

