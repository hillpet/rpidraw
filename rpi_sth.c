/*  (c) Copyright:  2022 Patrn, Confidential Data
 *
 *  Workfile:           rpi_sth.c
 *  Purpose:            Finite statehandler functions
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    30 Apr 2022:      Ported from rpicnc
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *               
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "cnc_func.h"
//
#include "rpi_sth.h"
//
#define USE_PRINTF
#include <printx.h>

/*------ functions separator ------------------------------------------------
____Global_Functions()  {};
----------------------------------------------------------------------------*/
static STHSTACK stSthStack = {};


//
//  Function:  STH_Init
//  Purpose:   Statehandler init
//
//  Parms:     State handler top struct
//  Returns:   Init state
//
CNCSTH STH_Init(const STH *pstTop)
{
   stSthStack.iMax = STH_MAX_STACKSIZE;
   PRINTF("STH-Init():Idx=%d, Max=%d" CRLF, stSthStack.iIndex, stSthStack.iMax);
   return(pstTop->tCurState);
}

//
//  Function:  STH_Execute
//  Purpose:   Statehandler exec
//
//  Parms:     Top State handler struct, state, parameter
//  Returns:   New state
//
CNCSTH STH_Execute(const STH *pstTop, CNCSTH tState, void *pvParm)
{
   const STH *pstSth;

   PRINTF("STH-Execute():Idx=%d, Max=%d" CRLF, stSthStack.iIndex, stSthStack.iMax);
   pstSth = &pstTop[tState];
   if(tState != pstSth->tNxtState) PRINTF("STH-Execute():[%s]" CRLF, pstSth->pcHelper);
   //
   return( pstSth->pfHandler(pstSth, pvParm) );
}

//
//  Function:  STH_Pull
//  Purpose:   Statehandler pull from STH stack
//
//  Parms:     State handler struct, void ptr
//  Returns:   New state
//
CNCSTH STH_Pull(const STH *pstSth, void *pvData)
{
   CNCSTH   tState;

   PRINTF("STH-Pull():Idx=%d, Max=%d" CRLF, stSthStack.iIndex, stSthStack.iMax);
   if(stSthStack.iIndex > 0)
   {
      stSthStack.iIndex--;
      tState = stSthStack.piStack[stSthStack.iIndex];
      PRINTF("STH-Pull():Return to state %d" CRLF, tState);
   }
   else tState = pstSth->tErrState;
   return(tState);
}

//
//  Function:  STH_Push
//  Purpose:   Statehandler push on STH stack
//
//  Parms:     State handler struct, void ptr
//  Returns:   New state
//
CNCSTH STH_Push(const STH *pstSth, void *pvData)
{
   CNCSTH   tState;
   int      iTemp;

   PRINTF("STH-Push():Idx=%d, Max=%d" CRLF, stSthStack.iIndex, stSthStack.iMax);
   if(stSthStack.iIndex < stSthStack.iMax)
   {
      //
      // Store return state on stack
      //
      iTemp = (int) pstSth->tCurState;
      stSthStack.piStack[stSthStack.iIndex] = iTemp + 1;
      stSthStack.iIndex++;
      tState = pstSth->tNxtState;
      PRINTF("STH-Push():Call state %d" CRLF, tState);
   }
   else tState = pstSth->tErrState;
   return(tState);
}
