/*  (c) Copyright:  2022 Patrn, Confidential Data
 *
 *  Workfile:           rpi_sth.h
 *  Purpose:            Finite statehandler header file
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    30 Apr 2022:      Ported from rpicnc
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *               
 *
 *
**/

#ifndef _RPI_STH_H_
#define _RPI_STH_H_

typedef struct _sth_
{
   CNCSTH      tCurState;
   CNCSTH      tNxtState;
   CNCSTH      tErrState;
   CNCSTH    (*pfHandler)(const struct _sth_ *, void *);
   const char *pcHelper;
}  STH;
//
// STH Stack
//
#define  STH_MAX_STACKSIZE    10
//
typedef struct _sth_stack_
{
   int   iIndex;
   int   iMax;
   int   piStack[STH_MAX_STACKSIZE];
}  STHSTACK;
//
// STH global functs
//
CNCSTH   STH_Init       (const STH *);
CNCSTH   STH_Execute    (const STH *, CNCSTH , void *);
CNCSTH   STH_Pull       (const STH *, void *);
CNCSTH   STH_Push       (const STH *, void *);

#endif   //_RPI_STH_H_
