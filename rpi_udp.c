/*  (c) Copyright:  2022 Patrn, Confidential Data
 *
 *  Workfile:           rpi_udp.c
 *  Purpose:            This thread 
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Created from template
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
//
#include "rpi_func.h"
#include "rpi_cnc.h"
#include "rpi_udp.h"
//
#define USE_PRINTF
#include <printx.h>

//
//
// Local functions
//
static int     udp_Execute                (void);
//
static int     udp_SendDatagram           (void);
//
static bool    udp_SignalRegister         (sigset_t *);
static void    udp_ReceiveSignalSegmnt    (int);
static void    udp_ReceiveSignalInt       (int);
static void    udp_ReceiveSignalTerm      (int);
static void    udp_ReceiveSignalUser1     (int);
static void    udp_ReceiveSignalUser2     (int);

//
// Local arguments
//
static int  fThreadRunning = TRUE;
static int  fConnected     = FALSE;

/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   UDP_Init
// Purpose:    Handle rpidraw UDP
//
// Parms:      
// Returns:    Exit codes
// Note:       Called from main() to split off the UDP thread
//
int UDP_Init()
{
   int      iCc;
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         GLOBAL_PidPut(PID_UDP, tPid);
         break;

      case 0:
         // Child:
         GEN_Sleep(500);
         iCc = udp_Execute();
         LOG_Report(0, "UDP", "UDP-Init():Exit normally, cc=%d", iCc);
         exit(iCc);
         break;

      case -1:
         // Error
         GEN_Printf("UDP-Init(): Error!" CRLF);
         LOG_Report(errno, "UDP", "UDP-Init():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(GLOBAL_UDP_INI);
}

/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   udp_Execute
// Purpose:    Handle CncDraw running state monitor
//
// Parms:      
// Returns:    Exit codes
// Note:       
//
static int udp_Execute()
{
   int         iOpt, iCc=EXIT_CC_OKEE;
   int         iTimeout=2000;
   sigset_t    tBlockset;

   if(udp_SignalRegister(&tBlockset) == FALSE)
   {
      LOG_Report(errno, "UDP", "udp-Execute():Exit Register ERROR:");
      return(EXIT_CC_GEN_ERROR);
   }
   GLOBAL_SemaphoreInit(PID_UDP);
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_UDP_INI);
   //
   // Init ready
   // Wait for Host Run/Execute
   //
   while(fThreadRunning)
   {
      iOpt = GLOBAL_SignalWait(PID_UDP, GLOBAL_HST_ALL_RUN, 1000);
      if(iOpt == 0) break;
   }
   PRINTF("udp-Execute():Running" CRLF);
   //
   // Run/Execute
   //
   while(fThreadRunning)
   {
      iOpt = GLOBAL_SemaphoreWait(PID_UDP, iTimeout);
      switch(iOpt)
      {
         case 0:
            if(GLOBAL_GetSignalNotification(PID_UDP, GLOBAL_HST_ALL_MID)) 
            {
               // Midnight
               PRINTF("udp-Execute():Midnight" CRLF);
               LOG_Report(0, "UDP", "udp-Execute():Midnight");
            }
            if(GLOBAL_GetSignalNotification(PID_UDP, GLOBAL_SVR_UDP_RUN)) 
            {
               char *pcIp;

               //
               // Server reports a connection
               // PAR_HOST_IP    : The connected IP Address
               // PAR_UDP_PORT   :
               //
               fConnected = TRUE;
               //iTimeout   = 500;
               pcIp = (char *)GLOBAL_GetParameter(PAR_HOST_IP);
               PRINTF("udp-Execute():Connected [%s]" CRLF, pcIp);
               LOG_Report(0, "UDP", "udp-Execute():Connected [%s]", pcIp);
            }
            break;

         case 1:
            // Timeout: 
            if(fConnected)
            {
               udp_SendDatagram();
            }  
            break;

         default:
         case -1:
            // Error
            LOG_Report(errno, "UDP", "udp-Execute():ERROR GLOBAL_SemaphoreWait");
            PRINTF("udp-Execute():ERROR GLOBAL_SemaphoreWait(errno=%d)" CRLF, errno);
            break;
      }
   }
   GLOBAL_SemaphoreDelete(PID_UDP);
   return(iCc);
}

/*----------------------------------------------------------------------
__________________UDP_FUNCTIONS(){}
------------------------------x----------------------------------------*/


//
//  Function:   udp_SendDatagram
//  Purpose:    Send out UDP Datagrams containing JSON CNC parameters
//
//  Parms:      
//  Returns:    0=Noting to send
//             +1=OKee send
//             -1=Socket error
//
static int udp_SendDatagram()
{
   int      iCc=0, iSocket, iPort, iIdx=0;
   int      iXmtSize, iNewSize, iDgrSize=MAX_DATAGRAM_SIZE;
   char    *pcJson;
   char    *pcAddr;
   RPICNC  *pstRpiCnc=&pstMap->G_stRpiCnc;
   
   //
   // Someone has connected to the CNC: setup an UDP connection to signal a-synchronous events
   //
   pcAddr = (char *)GLOBAL_GetParameter(PAR_HOST_IP);
   iPort  = *((int *)GLOBAL_GetParameter(PAR_UDP_PORT));
   //
   //PRINTF("udp-SendDatagram(): UDP start socket..." CRLF);
   iSocket = NET_ClientConnect(pcAddr, iPort, UDP_PROTOCOL);
   if(iSocket == -1)
   {
      PRINTF("udp-SendDatagram(): UDP connect failed: exiting" CRLF);
      LOG_Report(errno, "UDP", "udp-SendDatagram(): Server connect failed: exiting.");
      iCc = -1;
   }
   PRINTF("udp-SendDatagram(): connected to %s:%d" CRLF, pcAddr, iPort);
   //LOG_Report(0, "UDP", "udp-SendDatagram(): connected to %s:%d", pcAddr, iPort);
   //
   // Wait for signal to put out data
   //
   pcJson = (char *) safemalloc(iDgrSize);
   pstRpiCnc->iSeq++;
   //
   // Collect and allocate the JSON object
   //
   while(1)
   {
      iNewSize = CNC_BuildJsonObject(pcJson, iDgrSize);
      if(iNewSize > iDgrSize)
      {
         //
         // JSON buffer too small: enlarge and retry
         //
         PRINTF("udp-SendDatagram():UDP resize datagram[%d], have[%d], need[%d]" CRLF, pstRpiCnc->iSeq, iDgrSize, iNewSize);
         iDgrSize += iNewSize;
         pcJson = (char *) saferemalloc(pcJson, iDgrSize);
      }
      else break;
   }
   //PRINTF("udp-SendDatagram(): datagram[%d] size=%d data=[%s]" CRLF, pstRpiCnc->iSeq, iNewSize, pcJson);
   //
   // Send datagram(s)
   //
   while(iNewSize > 0)
   {
      if(iNewSize > MAX_DATAGRAM_SIZE) iXmtSize = MAX_DATAGRAM_SIZE;
      else                             iXmtSize = iNewSize;
      //
      iXmtSize = NET_Write(iSocket, &pcJson[iIdx], iXmtSize);
      if(iXmtSize < 0)
      {
         LOG_Report(errno, "UDP", "udp-Deamon(): UDP send ERROR");
         PRINTF("udp-SendDatagram():UDP send ERROR" CRLF);
         break;
      }
      else
      {
         PRINTF("udp-SendDatagram():UDP send datagram, chunk=[%d]" CRLF, iXmtSize);
         iIdx      += iXmtSize;
         iNewSize  -= iXmtSize;
      }
   }
   PRINTF("udp-SendDatagram():Done." CRLF);
   //
   // Exit: close socket
   //
   NET_ClientDisconnect(iSocket); 
   safefree(pcJson);
   return(iCc);
}


/*------  Local functions separator ------------------------------------
__SIGNAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

// 
// Function:   udp_SignalRegister
// Purpose:    Register all SIGxxx
// 
// Parameters: Blockset
// Returns:    TRUE if delivered
// 
static bool udp_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGSEGV:
  if( signal(SIGSEGV, &udp_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "UDP", "udp-SignalRegister(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // SIGUSR1:
  if( signal(SIGUSR1, &udp_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "UDP", "udp-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &udp_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "UDP", "udp-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &udp_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "UDP", "udp-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &udp_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "UDP", "udp-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
  }
  return(fCC);
}

//
// Function:   udp_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void udp_ReceiveSignalSegmnt(int iSignal)
{
   GLOBAL_SegmentationFault(__FILE__, __LINE__);
   LOG_SegmentationFault(__FILE__, __LINE__);
   exit(EXIT_CC_GEN_ERROR);
}

// 
// Function:   udp_ReceiveSignalTerm
// Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
// 
// Parameters: 
// Returns:    TRUE if delivered
// 
static void udp_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "UDP", "udp-ReceiveSignalTerm()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_UDP);
}

//
// Function:   udp_ReceiveSignalInt
// Purpose:    System callback for SIGINT (Ctl-C)
//
// Parms:
// Returns:    TRUE if delivered
//
static void udp_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "UDP", "udp-ReceiveSignalInt()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_UDP);
}

//
// Function:   udp_ReceiveSignalUser1
// Purpose:    System callback for SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:
//
static void udp_ReceiveSignalUser1(int iSignal)
{
}

//
// Function:   udp_ReceiveSignalUser2
// Purpose:    System callback for SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:
//
static void udp_ReceiveSignalUser2(int iSignal)
{
}


/*----------------------------------------------------------------------
______________COMMENT_FUNCTIONS(){}
------------------------------x----------------------------------------*/
#ifdef COMMENT


#endif   //COMMENT
