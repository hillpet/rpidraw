/*  (c) Copyright:  2022 Patrn, Confidential Data
 *
 *  Workfile:           cncstates.h
 *  Purpose:            CNC STH states
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    30 Apr 2022:      Ported from rpicnc
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *               
 *
 *
**/

//
// CNC states
//
//          Current-State        Next-State              Error-State          State-Exec              Helper-Text
EXTRACT_STH(STH_CNC_READY,       -1,                     STH_CNC_ERROR,       cnc_SthReady,           "STH:Ready"             )
EXTRACT_STH(STH_CNC_HOME,        STH_CNC_READY,          STH_CNC_ERROR,       cnc_SthHome,            "STH:Home"              )
EXTRACT_STH(STH_CNC_INIT,        STH_CNC_SETTING1,       STH_CNC_ERROR,       STH_Push,               "STH:Init"              )
EXTRACT_STH(STH_CNC_INITX,       STH_CNC_READY,          STH_CNC_ERROR,       cnc_SthReady,           "STH:InitX"             )
//
EXTRACT_STH(STH_CNC_OPEN,        STH_CNC_READ,           STH_CNC_ERROR,       cnc_SthFileOpen,        "STH:Open File"         )
EXTRACT_STH(STH_CNC_READ,        STH_CNC_CLOSE,          STH_CNC_ERROR,       cnc_SthFileRead,        "STH:Read File"         )
EXTRACT_STH(STH_CNC_CLOSE,       STH_CNC_READY,          STH_CNC_ERROR,       cnc_SthFileClose,       "STH:Close File"        )
//
// Sub-STH Calls
//
EXTRACT_STH(STH_CNC_SETTING1,    STH_CNC_SETTING2,       STH_CNC_ERROR,       cnc_SthSetting1,        "STH:Setting1"          )
EXTRACT_STH(STH_CNC_SETTING2,    STH_CNC_SETTING3,       STH_CNC_ERROR,       cnc_SthSetting2,        "STH:Setting2"          )
EXTRACT_STH(STH_CNC_SETTING3,    STH_CNC_SETTINGX,       STH_CNC_ERROR,       cnc_SthSetting3,        "STH:Setting3"          )
EXTRACT_STH(STH_CNC_SETTINGX,    STH_CNC_READY,          STH_CNC_ERROR,       STH_Pull,               "STH:SettingX"          )
//
EXTRACT_STH(STH_CNC_ERROR,       -1,                     -1,                  cnc_SthError,           "STH:Error !!"          )
