/*  (c) Copyright:  2022 Patrn, Confidential Data
 *
 *  Workfile:           rpi_func.c
 *  Purpose:            Misc functions
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <fnmatch.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
//
#include "rpi_main.h"
#include "rpi_func.h"
//
//#define USE_PRINTF
#include <printx.h>

//#warning slash-slash-pwjh-Temp-changes 

//
// Local functions
//

/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

// 
// Function:   RPI_DirectoryCreate
// Purpose:    Create directory
// 
// Parameters: Dir path, Permissions, log yesno
// Returns:    TRUE if okee
// Note:       
// 
bool RPI_DirectoryCreate(char *pcDir, mode_t tPerm, bool fLog)
{
   bool  fCc=TRUE;
   int   iRes;

   if((iRes = mkdir(pcDir, tPerm)) != 0)
   {
      if(fLog)
      {
         PRINTF("RPI-DirectoryCreate():ERROR creating directory [%s]:%s" CRLF, pcDir, strerror(errno));
         LOG_Report(errno, "FUN", "RPI-DirectoryCreate():ERROR creating directory [%s]:", pcDir);
      }
      fCc = FALSE;
   }
   else 
   {
      if(fLog)
      {
         LOG_Report(0, "FUN", "RPI-DirectoryCreate():Dir [%s] created [0%o]:", pcDir, tPerm);
         PRINTF("RPI-DirectoryCreate():Dir [%s] created [0%o]" CRLF, pcDir, tPerm);
      }
   }
   return(fCc);
}

// 
// Function:   RPI_DirectoryExists
// Purpose:    Check if directory exists
// 
// Parameters: Dir path
// Returns:    TRUE if exists
// Note:       
// 
bool RPI_DirectoryExists(char *pcDir)
{
   bool        fCc=FALSE;
   int         iRes;
   struct stat stStat;

   iRes = stat(pcDir, &stStat);
   if(iRes == 0)
   {
      if(S_ISDIR(stStat.st_mode)) 
      {
         // Fine: Dir exists
         fCc = TRUE;
      }
   }
   return(fCc);
}

// 
// Function:   RPI_FileExists
// Purpose:    Check if file exists
// 
// Parameters: File path
// Returns:    TRUE if exists
// Note:       
// 
bool RPI_FileExists(char *pcFilePath)
{
   bool        fCc=FALSE;
   int         iRes;
   struct stat stStat;

   iRes = stat(pcFilePath, &stStat);
   if(iRes == 0)
   {
      if(S_ISREG(stStat.st_mode)) 
      {
         // Fine: File exists
         fCc = TRUE;
      }
   }
   return(fCc);
}

// 
// Function:   RPI_GetFlag
// Purpose:    Decode the flag from the G_ PAR_ area
// 
// Parameters: Flag
// Returns:    Flag value
// Note:       
//
bool RPI_GetFlag(int iFlag)
{
   int      iFlags;

   iFlags = RPI_GetFlags();
   return((iFlags & iFlag) == iFlag);
}

// 
// Function:   RPI_GetFlags
// Purpose:    Get all flags from the G_ PAR_ area
// 
// Parameters: 
// Returns:    Flags
// Note:       
//
int RPI_GetFlags()
{
   int     *piFlags;
   int      iFlags;

   piFlags = GLOBAL_GetParameter(PAR_VERBOSE);
   iFlags  = *piFlags;
   iFlags &= ~VERBOSE_LEVEL_MASK;
   return(iFlags);
}

// 
// Function:   RPI_GetVerboseLevel
// Purpose:    Decode the verbose level from the G_ PAR_ area
// 
// Parameters: 
// Returns:    Verbose level
// Note:       
//
int RPI_GetVerboseLevel()
{
   int     *piFlags;
   int      iVerbose, iFlags;

   piFlags   = GLOBAL_GetParameter(PAR_VERBOSE);
   iFlags    = *piFlags;
   iVerbose  = (iFlags & VERBOSE_LEVEL_MASK) >> VERBOSE_LEVEL_SHIFT;
   return(iVerbose);
}


