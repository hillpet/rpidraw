/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           par_pico.h
 *  Purpose:            Pico CNC variables
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    15 May 2022:      New
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
//    Pico variables from the USB Debug port:
//
//       SECact= 119
//       CNCmin= 1                      
//       CNCact= 990                    
//       CNCmax= 1069                   
//       RCVget= 810                    
//       RCVput= 810                    
//       RCVmax= 1024                   
//       CMDget= 0                      
//       CMDput= 0                      
//       CMDmax= 256                    
//       RUNact= 0, 0, 0
//       POScur= 0, 0, 0
//       POSnew= 0, 0, 0
//       STPexa= 0, 0
//       SPDact=       0.00,       0.00
//       SPDlin=     400.00,     400.00
//       SPDset=     600.00,     600.00
//       SPDmax=     600.00,     600.00
//       SCLset=       0.00,       0.00
//       ACCset=     800.00,     800.00
//       TIMrup=       0.75,       0.75
//       TIMlin=      33.33,      33.33
//       TIMrdn=      34.08,      34.08
//       MOVrup= 225, 225
//       MOVlin= 19775, 19775
//       MOVrdn= 20000, 20000
//
//             enum           pcParm      iElements   iOffset
EXTRACT_PICO(VAR_PICO_SECS,   "SECact",   1,          offsetof(CNCPICO, iSeconds),     pico_Integer   )
EXTRACT_PICO(VAR_PICO_CNCM,   "CNCmin",   1,          -1,                              NULL           )
EXTRACT_PICO(VAR_PICO_CNCA,   "CNCact",   1,          -1,                              NULL           )
EXTRACT_PICO(VAR_PICO_CNCP,   "CNCmax",   1,          -1,                              NULL           )
EXTRACT_PICO(VAR_PICO_RCVG,   "RCVget",   1,          -1,                              NULL           )
EXTRACT_PICO(VAR_PICO_RCVP,   "RCVput",   1,          -1,                              NULL           )
EXTRACT_PICO(VAR_PICO_RCVM,   "RCVmax",   1,          -1,                              NULL           )
EXTRACT_PICO(VAR_PICO_CMDG,   "CMDget",   1,          -1,                              NULL           )
EXTRACT_PICO(VAR_PICO_CMDP,   "CMDput",   1,          -1,                              NULL           )
EXTRACT_PICO(VAR_PICO_CMDM,   "CMDmax",   1,          -1,                              NULL           )
EXTRACT_PICO(VAR_PICO_RUNA,   "RUNact",   3,          offsetof(CNCPICO, iRunAxis),     pico_Integer   )
EXTRACT_PICO(VAR_PICO_POSC,   "POScur",   3,          offsetof(CNCPICO, iPosCur),      pico_Integer   )
EXTRACT_PICO(VAR_PICO_POSN,   "POSnew",   3,          offsetof(CNCPICO, iPosNew),      pico_Integer   )
EXTRACT_PICO(VAR_PICO_STOP,   "STPexa",   2,          offsetof(CNCPICO, iStopDel),     pico_Integer   )
EXTRACT_PICO(VAR_PICO_SPDA,   "SPDact",   2,          offsetof(CNCPICO, flSpeedAct),   pico_Double    )
EXTRACT_PICO(VAR_PICO_SPDL,   "SPDlin",   2,          offsetof(CNCPICO, flSpeedLin),   pico_Double    )
EXTRACT_PICO(VAR_PICO_SPDS,   "SPDset",   2,          offsetof(CNCPICO, flSpeedSet),   pico_Double    )
EXTRACT_PICO(VAR_PICO_SPDM,   "SPDmax",   2,          offsetof(CNCPICO, flSpeedMax),   pico_Double    )
EXTRACT_PICO(VAR_PICO_SCLS,   "SCLset",   2,          offsetof(CNCPICO, flScaleSet),   pico_Double    )
EXTRACT_PICO(VAR_PICO_ACCS,   "ACCset",   2,          offsetof(CNCPICO, flAccelSet),   pico_Double    )
EXTRACT_PICO(VAR_PICO_MOVU,   "MOVrup",   2,          offsetof(CNCPICO, iMoveRup),     pico_Integer   )
EXTRACT_PICO(VAR_PICO_MOVL,   "MOVlin",   2,          offsetof(CNCPICO, iMoveLin),     pico_Integer   )
EXTRACT_PICO(VAR_PICO_MOVD,   "MOVrdn",   2,          offsetof(CNCPICO, iMoveRdn),     pico_Integer   )
EXTRACT_PICO(VAR_PICO_TIMU,   "TIMrup",   2,          offsetof(CNCPICO, fTimeRup),     pico_Double    )
EXTRACT_PICO(VAR_PICO_TIML,   "TIMlin",   2,          offsetof(CNCPICO, fTimeLin),     pico_Double    )
EXTRACT_PICO(VAR_PICO_TIMD,   "TIMrdn",   2,          offsetof(CNCPICO, fTimeRdn),     pico_Double    )
