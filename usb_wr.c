/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           usb_wr.c
 *  Purpose:            USB read/write functions
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from rpicnc
 *    29 May 2022:      Separate USB Rd/Wr threads
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <errno.h>
#include <sys/select.h>
#include <sys/signal.h>
#include <sys/types.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_func.h"
#include "rpi_usb.h"
//
#include "usb_wr.h"

//#define USE_PRINTF
#include "printf.h"

//
// Local prototypes
//
static int     usb_ExecuteWrite        ();
//
static void    usb_ReceiveSignalUser1  (int);
static void    usb_ReceiveSignalUser2  (int);
static void    usb_ReceiveSignalInt    (int);
static void    usb_ReceiveSignalTerm   (int);
static bool    usb_SignalRegister      (sigset_t *);
//
// Global data
//
static bool    fThreadRunning       = TRUE;

/*------  Local functions separator ------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:  USB_InitWrite
//  Purpose:   Init communicating with the USB port
//
//  Parms:     
//  Returns:   Completion code
//
int USB_InitWrite()
{
   int      iCc;
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         GLOBAL_PidPut(PID_USBWR, tPid);
         break;

      case 0:
         // Child:
         GEN_Sleep(500);
         iCc = usb_ExecuteWrite();
         LOG_Report(0, "UWR", "USB-InitWrite():Exit normally, cc=%d", iCc);
         exit(iCc);
         break;

      case -1:
         // Error
         GEN_Printf("USB-InitWrite(): Error!" CRLF);
         LOG_Report(errno, "UWR", "USB-InitWrite():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(GLOBAL_UWR_INI);
}

/*------  Local functions separator ------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:  usb_ExecuteWrite
//  Purpose:   Init writing to the USB port
//
//  Parms:    
//  Returns:   Completion code
//
static int usb_ExecuteWrite()
{
   int         iReported=0;
   int         iNr, iOpt, iCc=EXIT_CC_GEN_ERROR;
   sigset_t    tBlockset;
   RPIUSB     *pstUsb;

   if(usb_SignalRegister(&tBlockset) == FALSE) return(iCc);
   //
   pstUsb = USB_GetDeviceInfo(USB_DEV_WR);
   GLOBAL_SemaphoreInit(PID_USBWR);
   //
   // Init ready
   //
   GLOBAL_PidSaveGuard(PID_USBWR, GLOBAL_UWR_INI);
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_UWR_INI);
   //
   // Wait for Host Run/Execute
   //
   while(fThreadRunning)
   {
      iOpt = GLOBAL_SignalWait(PID_USBWR, GLOBAL_HST_ALL_RUN, 1000);
      if(iOpt == 0) break;
   }
   PRINTF("usb-ExecuteWrite():Init OKee: [%s]" CRLF, pstUsb->pcDevPath);
   PRINTF("usb-ExecuteWrite():Running" CRLF);
   //
   // Run/Execute
   //
   while(fThreadRunning)
   {
      if(USB_ConnectedRead(pstUsb))
      {
         //
         //====================================================================
         // CNC Command Deamon RUN loop :
         //====================================================================
         iOpt = GLOBAL_SemaphoreWait(PID_USBWR, 50);
         switch(iOpt)
         {
            case 0:
               //
               // We return here after the reception of :
               //    o SIGUSR1
               //    o SIGUSR2
               //    o SIGINT 
               //    o SIGTERM
               //    
               if(GLOBAL_GetSignalNotification(PID_USBWR, GLOBAL_HST_ALL_MID)) 
               {
                  // Midnight
                  PRINTF("usb-ExecuteWrite():Midnight" CRLF);
                  LOG_Report(0, "UWR", "usb-ExecuteWrite():Midnight");
               }
               break;

            case 1:
               // Timeout: 
               // Blocking Read CNC Command USB serial port data until EOL
               //
               iNr = USB_Read(pstUsb);
               if(iNr > 0)
               {
                  //if(RPI_GetFlag(FLAGS_LOG_CNC)) LOG_ListData("usb-ExecuteWrite()", pstUsb->cRcv, CIR_RCV_LENZ);
                  //if(RPI_GetFlag(FLAGS_PRN_CNC)) LOG_DumpData("usb-ExecuteWrite()", pstUsb->cRcv, CIR_RCV_LENZ);
                  //
                  // USB CNC Command port has incoming data stored in the circular RCV buffer
                  //    pstMap->G_stCmd.cRcv        : All received data
                  //    pstMap->G_stCmd.iRcvGet/Put : Get/Put index into the circular buffer
                  //
                  // Somebody MUST get these out before the buffer is full!
                  //
                  GLOBAL_SetSignalNotification(PID_CNC, GLOBAL_CMD_CNC_REC);
               }
               else if(iNr < 0) 
               {
                  PRINTF("cnc-ExecuteWrite():ERROR from USB" CRLF);
               }
               else PRINTF("cnc-ExecuteWrite():No data from USB" CRLF);
               break;

            default:
            case -1:
               // Error
               LOG_Report(errno, "UWR", "usb-ExecuteWrite():ERROR GLOBAL_SemaphoreWait");
               PRINTF("usb-ExecuteWrite():ERROR GLOBAL_SemaphoreWait(errno=%d)" CRLF, errno);
               break;
         }
      }
      else // NOT Connected
      {
         if(iReported > 0) 
         {
            //Only Once
            iReported = -1;
            USB_CloseRead(pstUsb);
            PRINTF("usb-ExecuteWrite():Disconnected:Retry open...." CRLF);
         }
         if( USB_OpenRead(pstUsb) )
         {
            //Only Once
            iReported = +1;
            LOG_Report(0, "UDB", "usb-ExecuteWrite():USB Device Connected!");
            PRINTF("usb-ExecuteWrite():USB Device Connected!" CRLF);
            GLOBAL_SetSignalNotification(PID_CNC, GLOBAL_CMD_CNC_CNX);
         }
         else // USB Port (still) not connected: wait till it is
         {
            if(iReported >= 0)
            {
               //Only Once
               iReported = -1;
               LOG_Report(0, "UDB", "usb-ExecuteWrite():USB Device NOT connected!");
               PRINTF("usb-ExecuteWrite():USB Device NOT connected!" CRLF);
               GLOBAL_SetSignalNotification(PID_CNC, GLOBAL_CMD_CNC_CNX);
            }
            GEN_Sleep(USB_CNX_TIMEOUT);
         }
      }
   }
   //
   // Thread exit
   //
   PRINTF("usb-ExecuteWrite():Exit" CRLF);
   USB_CloseRead(pstUsb);
   GLOBAL_SemaphoreDelete(PID_USBWR);
   return(iCc);
}

/*------  Local functions separator ------------------------------------
_______________SIGNAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:  usb_ReceiveSignalUser1
//  Purpose:   SIGUSR1 signal
//
//  Parms:
//  Returns:    
//  Note:      SIGUSR1 to accept a new GCode sequence
//
static void usb_ReceiveSignalUser1(int iSignal)
{
   PRINTF ("usb-ReceiveSignalUser1()" CRLF);
   GLOBAL_SemaphorePost(PID_USBWR);
}

//
//  Function:  usb_ReceiveSignalUser2
//  Purpose:   SIGUSR2 signal
//
//  Parms:
//  Returns:    
//  Note:      SIGUSR2 
//
static void usb_ReceiveSignalUser2(int iSignal)
{
   PRINTF ("usb-ReceiveSignalUser2()" CRLF);
   GLOBAL_SemaphorePost(PID_USBWR);
}

//
//  Function:  usb_ReceiveSignalInt
//  Purpose:   SIGINT signal
//
//  Parms:
//  Returns:    
//  Note:      SIGINT to exit
//
static void usb_ReceiveSignalInt(int iSignal)
{
   PRINTF ("usb-ReceiveSignalInt()" CRLF);
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_USBWR);
}

//
//  Function:  usb_ReceiveSignalTerm
//  Purpose:   SIGTERM signal
//
//  Parms:
//  Returns:    
//  Note:      SIGTERM: Stops ALL CNC motion
//
static void usb_ReceiveSignalTerm(int iSignal)
{
   PRINTF ("usb-ReceiveSignalTerm()" CRLF);
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_USBWR);
}

//
//  Function:  usb_SignalRegister
//  Purpose:   Register all SIGxxx
//
//  Parms:     Blockset
//  Returns:   TRUE if delivered
//
static bool usb_SignalRegister(sigset_t *ptBlockset)
{
  bool fCc = TRUE;

  //
  // SIGUSR1 is used to talk between parent and child 
  //
  if( signal(SIGUSR1, &usb_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "UWR", "usb-SignalRegister(): SIGUSR1 ERROR");
     fCc = FALSE;
  }
  //
  // SIGUSR2 is not used
  //
  if( signal(SIGUSR2, &usb_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "UWR", "usb-SignalRegister(): SIGUSR2 ERROR");
     fCc = FALSE;
  }
  //
  // SIGINT  is used to exit this thread
  //
  if( signal(SIGINT, &usb_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "UWR", "usb-SignalRegister(): SIGINT ERROR");
     fCc = FALSE;
  }
  //
  // SIGTERM is used to stop CNC movement
  //
  if( signal(SIGTERM, &usb_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "UWR", "usb-SignalRegister(): SIGTERM ERROR");
     fCc = FALSE;
  }
  //
  if(fCc)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGTERM);
  }
  return(fCc);
}

