/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           gen_http.h
 *  Purpose:            HTTP functions header file
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _GEN_HTTP_H_
#define _GEN_HTTP_H_

//
// Dynamic page registered URLs
//
typedef bool(*PFVOIDPI)(void *, int);
//
char    *GEN_FindDynamicPageName       (int);
int      GEN_GetDynamicPageUrl         (int);
char    *GEN_GetDynamicPageName        (int);
int      GEN_GetDynamicPagePid         (int);
int      GEN_GetDynamicPagePort        (int);
int      GEN_GetDynamicPageTimeout     (int);
FTYPE    GEN_GetDynamicPageType        (int);
PFVOIDPI GEN_GetDynamicPageCallback    (int);
//
int      GEN_RegisterDynamicPage       (int, int, FTYPE, int, int, const char *, PFVOIDPI);


#endif /* _GEN_HTTP_H_ */
