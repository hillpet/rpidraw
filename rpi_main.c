/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_main.c
 *  Purpose:            Rpi Draw utility for Pico CncDraw
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <pwd.h>
//
#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_func.h"
#include "rpi_guard.h"
#include "rpi_srvr.h"
#include "rpi_cnc.h"
#include "rpi_udp.h"
#include "rpi_usb.h"
#include "usb_wr.h"
#include "usb_db.h"
//
#include "rpi_main.h"
//
#define USE_PRINTF
#include <printx.h>

//#warning slash-slash-pwjh-Temp-changes 

//
// Global constants
//
const char             *pcLogfile         = RPI_LOG_PATH;
const char             *pcCsvFile         = RPI_CSV_PATH;

#ifdef   FEATURE_RUN_USERSPACE
const char             *pcNewUser         = "peter";
#else    //FEATURE_RUN_USERSPACE
const char             *pcNewUser         = NULL;
#endif   //FEATURE_RUN_USERSPACE

const char             *pcGroupWww        = "www-data";
//
// Local arguments
//
static bool             fRpiRunning       = TRUE;
//
static const char      *pcShellReboot     = "sudo shutdown -r +5";
static const char      *pcCacheDir        = RPI_WORK_DIR;
static const char      *pcSeplines        = "------------------------------------------------------------------------------------";
//
// Local functions
//
#ifdef   DEBUG_ASSERT
  #define  RPI_DAEMON()                   LOG_printf("rpi-Daemon():Debug mode !" CRLF);

#else    //DEBUG_ASSERT
   static pid_t   rpi_Daemon();
  #define RPI_DAEMON()                    rpi_Daemon()

#endif   //DEBUG_ASSERT
//
static int     rpi_Execute                (void);
//
static bool    rpi_CheckGuard             (u_int32);
static bool    rpi_CheckMidnight          (u_int32);
static bool    rpi_RequestReboot          (void);
static void    rpi_SetupGpio              (void);
static bool    rpi_SetupUser              (const char *);
static int     rpi_Startup                (void);
static void    rpi_TestMmap               (void);
static bool    rpi_WaitStartup            (int);
//
static bool    rpi_SignalRegister         (sigset_t *);
static void    rpi_ReceiveSignalInt       (int);
static void    rpi_ReceiveSignalTerm      (int);
static void    rpi_ReceiveSignalUser1     (int);
static void    rpi_ReceiveSignalUser2     (int);
static void    rpi_ReceiveSignalSegmnt    (int);

//
// Function:   main
// Purpose:    Main entry point for the Raspberry Pi CNC Draw utility
//
// Parms:      Commandline options
// Returns:    Exit codes
// Note:       
//
int main(int argc, char **argv)
{
   int      iOpt, iCc=EXIT_CC_OKEE;
   u_int32  ulSecsNow;
   char    *pcBuffer;

   //
   // At this stage, we should be ROOT.
   // If we use IO, it probably has to init before switching to user mode !
   //
   rpi_SetupGpio();
   //
   // Pending FEATURE_RUN_USERSPACE, we run as root (pcNewUser=NULL) or normal user
   //
   if(!rpi_SetupUser(pcNewUser))
   {
      printf("main():Error setting up user [%s]" CRLF, pcNewUser);
      exit(EXIT_CC_GEN_ERROR);
   }
   //==============================================================================================
   // Truncate reports file
   //==============================================================================================
   if(GEN_FileTruncate((char *)pcLogfile, MAX_NUM_REPORT_LINES) != 0)
   {
      printf("main():Error truncating reports file [%s]" CRLF, pcLogfile);
   }
   //
   // Open logfile
   //
   if( LOG_ReportOpenFile((char *)pcLogfile) == FALSE)
   {
      LOG_printf("main():Error opening log file [%s]" CRLF, pcLogfile);
   }
   else 
   {
      //
      // Track the LOG fileposition, we must update the Global G_ variable when we have the MMAP 
      //
      pstMap->G_llLogFilePos = LOG_ReportFilePosition();
      LOG_printf("main():Logfile is %s (at pos %ld)" CRLF, pcLogfile, pstMap->G_llLogFilePos);
   }
   //
   LOG_Report(0, "HST", (char *)pcSeplines);
   LOG_Report(0, "HST", "Main():RpiDraw Version=%s", VERSION);
   //
   // Check if the MMAP file was properly restored or not
   //
   if(pstMap->G_fInit) 
   {
      LOG_Report(0, "HST", "Main():MMAP file signature or DB version mismatch, created new Mapping:");
      LOG_Report(0, "HST", "Main():DB version now v%d.%d", pstMap->G_iVersionMajor, pstMap->G_iVersionMinor);
   }
   else LOG_Report(0, "HST", "Main():MMAP file OKee: DB-v%d.%d", pstMap->G_iVersionMajor, pstMap->G_iVersionMinor);
   //
   PRINTF("main():RpiDraw has %d args" CRLF, argc);
   //
   // RpiDraw Command line options:
   // -T x  : Test MMAP vars
   // -d    : Test DST
   // -v x  : Overrule G_iVerbose flag
   //
   while ((iOpt = getopt(argc, argv, "T:dv:")) != -1)
   {
      switch (iOpt)
      {
         case 'T':
            LOG_printf("rpi-main():Option -T: Test MMAP: Args = [%s]" CRLF, optarg);
            fRpiRunning = (bool)strtol(optarg, NULL, 0);
            rpi_TestMmap();
            break;

         case 'd':
            LOG_printf("rpi-main():Option -d: Test DST" CRLF);
            pcBuffer = safemalloc(128);
            for(int iYr=1970; iYr<2024; iYr++)
            {
               ulSecsNow = RTC_GetSecs(iYr, 1, 1);
               LOG_printf("rpi-main():RTC_GetSecs() Year=%4d  = %10d Secs" CRLF, iYr, ulSecsNow);
            }
            //
            // Get SystemSecs and show if DST is correctly included
            //
            ulSecsNow = RTC_GetSystemSecs();
            LOG_printf("rpi-main():RTC_GetSystemSecs()      = %d Secs" CRLF, ulSecsNow);
            LOG_printf("rpi-main():RTC_GetMidnight()        = %d Secs" CRLF, RTC_GetMidnight(ulSecsNow));
            LOG_printf("rpi-main():RTC_GetSecs() Midnight   = %d Secs" CRLF, RTC_GetSecs(RTC_GetYear(ulSecsNow), RTC_GetMonth(ulSecsNow), RTC_GetDay(ulSecsNow)));
            //
            LOG_printf("rpi-main():RTC_GetYear()            = %d" CRLF, RTC_GetYear(ulSecsNow));
            LOG_printf("rpi-main():RTC_GetMonth()           = %d" CRLF, RTC_GetMonth(ulSecsNow));
            LOG_printf("rpi-main():RTC_GetDay()             = %d" CRLF, RTC_GetDay(ulSecsNow));
            LOG_printf("rpi-main():RTC_GetHour()            = %d" CRLF, RTC_GetHour(ulSecsNow));
            LOG_printf("rpi-main():RTC_GetMinute()          = %d" CRLF, RTC_GetMinute(ulSecsNow));
            LOG_printf("rpi-main():RTC_GetSecond()          = %d" CRLF, RTC_GetSecond(ulSecsNow));
            //
            RTC_ConvertDateTime(TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM_SS, pcBuffer, ulSecsNow);
            LOG_printf("rpi-main():RTC_ConvertDateTime()    = %s" CRLF, pcBuffer);
            safefree(pcBuffer);
            fRpiRunning = FALSE;
            break;

         case 'v':
            LOG_printf("rpi-main():Option -v: Overrule Verbose to [%s]" CRLF, optarg);
            LOG_Report(0, "HST", "rpi-main():Option -v %s", optarg);
            pstMap->G_iVerbose = (int)strtol(optarg, NULL, 0);
            break;

         default:
            LOG_printf("rpi-main(): Invalid option %c" CRLF, (char) iOpt);
            LOG_Report(0, "HST", "rpi-main():Invalid option %c", (char) iOpt);
            break;
      }
   }
   //
   // If still enabled to run:
   //
   if(fRpiRunning)
   {
      // ---------------------------------------------------------------------------------------------
      // Run threads
      // ---------------------------------------------------------------------------------------------
      // Host     : Main thread: start new threads for:
      // CNC      : CNC Daemon
      // CNC Wr   : CNC Write
      // CNC Db   : CNC Debug
      // Guard    : Guard
      // Server   : HTTP Server
      // UDP      : UDP daemon
      // 
      // ---------------------------------------------------------------------------------------------
      RPI_DAEMON();
      //
      // If CL debug, the CLI will return here and spin of additional threads to handle motion 
      // and archiving. If run as service, the parent has already exited and the child will 
      // resume here.
      //
      GLOBAL_PidPut(PID_HOST, getpid());
      GLOBAL_SemaphoreInit(PID_HOST);
      //
      pstMap->G_iGuards = rpi_Startup();
      fRpiRunning       = rpi_WaitStartup(pstMap->G_iGuards);
      //
      GLOBAL_PidClearGuards();
      //
      // Display all pids in log file
      //
      GLOBAL_PidsLog();
      PRINTF("rpi-main():Running..." CRLF);
      LOG_Report(0, "HST", (char *)pcSeplines);
      //
      rpi_Execute();
      //
      // Exit 
      //
      GLOBAL_PidsTerminate(2000);
      GLOBAL_SemaphoreDelete(PID_HOST);
   }
   LOG_printf("Main():SafeMalloc balance=%d (Should be zero)" CRLF, GLOBAL_GetMallocs());
   LOG_Report(0, "HST", "rpi-main():SafeMalloc balance=%d (Should be zero)", GLOBAL_GetMallocs());
   LOG_printf("Main():RpiDraw Version=%s now EXIT" CRLF, VERSION);
   LOG_Report(0, "HST", "Main():RpiDraw Version=%s now EXIT", VERSION);
   LOG_ReportCloseFile();
   //==============================================================================================
   // From here on :
   // CANNOT Use functions which use MMAP anymore:
   // CANNOT Use LOG_printf(), PRINTFx() or LOG_Report() due to the use of the G_tmutex !
   //==============================================================================================
   GLOBAL_Sync();
   GLOBAL_Close(TRUE);
   exit(iCc);
}

/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/


#ifndef  DEBUG_ASSERT
//
// Function:   rpi_Daemon
// Purpose:    Daemonize the PiLrellCam on_motion_begin
//             
// Parms:       
// Returns:     
// Note:       Do NOT use PRINTF macro's here. Use LOG_printf() instead !!
//
static pid_t rpi_Daemon(void)
{
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent (Commandline or service) 
         exit(EXIT_CC_OKEE);
         break;

      case 0:
         // Child:
         // Continue as separeate thread
         break;

      case -1:
         // Error
         LOG_Report(errno, "HST", "rpi-Daemon():ERROR ");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(tPid);
}
#endif   //DEBUG_ASSERT

//
// Function:   rpi_Execute
// Purpose:    Handle Rpi Draw utility
//
// Parms:      
// Returns:    Exit codes
// Note:       
//
static int rpi_Execute()
{
   int      iOpt, iCc=EXIT_CC_GEN_ERROR;
   u_int32  ulSecs;
   sigset_t tBlockset;

   if(rpi_SignalRegister(&tBlockset) == FALSE) return(iCc);
   //
   // Execute all threads now
   //
   GEN_Sleep(2000);
   PRINTF("rpi-Execute():Run threads" CRLF);
   GLOBAL_SetSignalNotification(PID_GUARD, GLOBAL_HST_ALL_RUN);
   GLOBAL_SetSignalNotification(PID_SRVR,  GLOBAL_HST_ALL_RUN);
   GLOBAL_SetSignalNotification(PID_CNC,   GLOBAL_HST_ALL_RUN);
   GLOBAL_SetSignalNotification(PID_USBWR, GLOBAL_HST_ALL_RUN);
   GLOBAL_SetSignalNotification(PID_USBDB, GLOBAL_HST_ALL_RUN);
   GLOBAL_SetSignalNotification(PID_UDP,   GLOBAL_HST_ALL_RUN);
   iCc = EXIT_CC_OKEE;

/*----------------------------------------------------------------------
_______________________RUN_LOOP(){}
------------------------------x----------------------------------------*/
   while(fRpiRunning)
   {
      //
      // Wait until triggered by:
      //    o SIGINT:   Terminate normally
      //    o SIGTERM:  Terminate normally
      //    o SIGSEGV:  Segmentation fault
      //    o SIGUSR1:  Run command
      //    o SIGUSR2:  
      //
      iOpt = GLOBAL_SemaphoreWait(PID_HOST, TIMEOUT_MSECS);
      switch(iOpt)
      {
         case 0:
            //
            // Sem unlocked
            // Check Reboot request from one of the threads
            //
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_ALL_HST_RBT))
            {
               LOG_Report(0, "HST", "rpi-Execute():REBOOT Request !!");
               fRpiRunning = rpi_RequestReboot();
            }
            break;

         case 1:
            //
            // Timeout: 
            //
            ulSecs = RTC_GetSystemSecs();
            rpi_CheckGuard(ulSecs);
            rpi_CheckMidnight(ulSecs);
            break;

         default:
         case -1:
            LOG_Report(errno, "HST", "rpi-Execute():ERROR GLOBAL_SemaphoreWait");
            PRINTF("rpi-Execute():ERROR GLOBAL_SemaphoreWait(errno=%d)" CRLF, errno);
            iCc = EXIT_CC_GEN_ERROR;
            fRpiRunning = FALSE;
            break;
      }
   }
   PRINTF("rpi-Execute():Exit threads" CRLF);
   return(iCc);
}

//
//  Function:   rpi_CheckGuard
//  Purpose:    Trigger the guard if it's time to check the running dtste of theApp
//
//  Parms:      Current secs timestamp
//  Returns:    TRUE if OKee
//
static bool rpi_CheckGuard(u_int32 ulSecs)
{
   static u_int32 ulGuardSecs=0;
   //
   bool  fCc=TRUE;

   if(ulGuardSecs == 0) 
   {
      ulGuardSecs = ulSecs + GUARD_CHECK_SECS;
   }
   else if(ulGuardSecs < ulSecs)
   {
      GLOBAL_SetSignalNotification(PID_GUARD, GLOBAL_HST_GRD_RUN);
      ulGuardSecs = ulSecs + GUARD_CHECK_SECS;
   }
   return(fCc);
}

//
//  Function:   rpi_CheckMidnight
//  Purpose:    Trigger all threads on midnight, called every TIMEOUT_MSECS msecs.
//
//  Parms:      Current secs timestamp
//  Returns:    TRUE if midnight
//
static bool rpi_CheckMidnight(u_int32 ulSecs)
{
   static u_int32 ulMidnightSecs=0;
   //
   bool  fCc=FALSE;

   if(ulMidnightSecs == 0) 
   {
      ulMidnightSecs = RTC_GetMidnight(ulSecs) + (24*3600);
      PRINTF("rpi-CheckMidnight():SET Midnight" CRLF);
   }
   else if(ulSecs > ulMidnightSecs)
   {
      LOG_Report(0, "RPI", "rpi-CheckMidnight():Midnight" CRLF);
      if(GLOBAL_FlushLog() != EXIT_CC_OKEE)
      {
         PRINTF("rpi_CheckMidnight(): ERROR saving app files: %s" CRLF, strerror(errno));
         LOG_Report(errno, "RPI", "rpi_CheckMidnight(): ERROR saving app files: ");
      }
      //
      // Midnight: Notify the other threads and send summary email
      //
      GLOBAL_SetSignalNotification(PID_USBWR, GLOBAL_HST_ALL_MID);
      GLOBAL_SetSignalNotification(PID_USBDB, GLOBAL_HST_ALL_MID);
      GLOBAL_SetSignalNotification(PID_GUARD, GLOBAL_HST_ALL_MID);
      GLOBAL_SetSignalNotification(PID_SRVR,  GLOBAL_HST_ALL_MID);
      GLOBAL_SetSignalNotification(PID_UDP,   GLOBAL_HST_ALL_MID);
      GLOBAL_SetSignalNotification(PID_CNC,   GLOBAL_HST_ALL_MID);
      ulMidnightSecs = ulSecs + (24 * 3600);
      fCc = TRUE;
   }
   return(fCc);
}

// 
// Function:   rpi_RequestReboot
// Purpose:    System request to reboot
// 
// Parameters: 
// Returns:    New Running flag (FALSE if reboot needed)
// Note:       Without an active Watchdog, poweroff will NOT reboot the system, so we need 
//             a dedicated one here
// 
static bool rpi_RequestReboot()
{
   PRINTF("rpi-RequestReboot():[%s]" CRLF, pcShellReboot);
   LOG_Report(0, "HST", "rpi-RequestReboot():[%s]" CRLF, pcShellReboot);
   system(pcShellReboot);
   return(FALSE);
}

//
//  Function:   rpi_SetupGpio()
//  Purpose:    Setup GPIO for various IO alternatives
//
//  Parms:      
//  Returns:    
//
static void rpi_SetupGpio()
{
   printf("rpi-SetupGpio():" RPI_IO_BOARD CRLF);

#ifdef FEATURE_IO_NONE
   //=============================================================================
   // Use NO I/O board
   //=============================================================================
#endif


#ifdef FEATURE_IO_PATRN
   //=============================================================================
   // Used Patrn.nl IO board (wiringPi API)
   //=============================================================================
   if( (wiringPiSetup() == -1) )
   {
      printf("rpi-SetupGpio(): wiringPiSetup failed!" CRLF);
   }
   //
   INP_GPIO(LED_Y); OUT_GPIO(LED_Y);
   INP_GPIO(LED_G); OUT_GPIO(LED_G);
#endif
}

//
//  Function:   rpi_SetupUser
//  Purpose:    Setup the actual User for the app
//
//  Parms:      Username (NULL if we stay root)
//  Returns:    TRUE if OKee
//
static bool rpi_SetupUser(const char *pcUser)
{
   bool  fOkee=TRUE;
   uid_t tUid;
   gid_t tGid;

   if(pcUser)
   {
      #define FEATURE_SHOW_USER
      #ifdef FEATURE_SHOW_USER
      struct passwd *pstPwd;

      errno  = 0;
      pstPwd = getpwnam(pcUser);
      if (pstPwd == NULL) 
      {
         printf("rpi_SetupUser(): Error using <%s>:%s"  CRLF, pcUser, strerror(errno));
      }
      else
      {
         printf("rpi_SetupUser(): Name  = %s" CRLF, pstPwd->pw_name);
         printf("rpi_SetupUser(): Psw   = %s" CRLF, pstPwd->pw_passwd);
         printf("rpi_SetupUser(): Gecos = %s" CRLF, pstPwd->pw_gecos);
         printf("rpi_SetupUser(): Dir   = %s" CRLF, pstPwd->pw_dir);
         printf("rpi_SetupUser(): Shell = %s" CRLF, pstPwd->pw_shell);
      }
      #endif   //FEATURE_SHOW_USER
   }
   //
   // Some action require root permission
   //
   if(getuid() == 0)
   {
      //
      // We are ROOT
      //
      if(!RPI_DirectoryExists((char *)pcCacheDir))  
      {
         printf("rpi_SetupUser(): Create directory [%s]" CRLF, pcCacheDir);
         //
         // Cache dir does not yet exist: create it
         //
         if( (fOkee = RPI_DirectoryCreate((char *)pcCacheDir, ATTR_RWXR_XR_X, FALSE)) )
         {
            if(pcUser)
            {
               //
               // We need to run in userspace
               //
               if((fOkee = GEN_ChangeOwner(pcCacheDir, pcNewUser, pcGroupWww, ATTR_RWXR_XR_X, TRUE)) )
               {
                  int   iFd;
                  //
                  // Create the CSV file
                  //
                  if( (iFd = safeopen2((char *)pcCsvFile, O_RDWR|O_CREAT|O_TRUNC, 0640)) < 0) fOkee = FALSE;
                  else
                  {
                     safeclose(iFd);
                     fOkee = GEN_ChangeOwner(pcCsvFile, pcNewUser, pcGroupWww, ATTR_RW_R__R__, FALSE);
                  }
               }
            }
         }
         else printf("rpi_SetupUser(): ERROR creating directory [%s]" CRLF, pcCacheDir);
      }
      //
      if(fOkee)
      {
         printf("rpi_SetupUser(): Directory [%s] OKee" CRLF, pcCacheDir);
         //
         // Cache Dir exists, attributes changed. 
         // Create/copy mapfile as root, then switch to userspace if required
         //
         GLOBAL_Init();
         if(pcUser)
         {
            fOkee = GEN_ChangeOwner(pcMapFile, pcNewUser, pcGroupWww, ATTR_RW_R__R__, FALSE);
            //
            // All dirs and files have the correct user/group ID
            // Switch to new user
            //
            tUid = GEN_GetUserIdByName(pcUser);
            tGid = GEN_GetGroupIdByName(pcGroupWww);
            //
            if( (tUid > 0) && (tGid > 0) )
            {
               if( setgid(tGid) == -1)
               {
                  printf("rpi-SetupUser():ERROR setting Group %s (%d): %s" CRLF, pcGroupWww, (int)tGid, strerror(errno));
                  fOkee = FALSE;
               }
               else 
               {
                  printf("rpi-SetupUser():Now Group %s (%d)" CRLF, pcGroupWww, (int)tGid);
               }
               if( setuid(tUid) == -1)
               {
                  printf("rpi-SetupUser():ERROR setting User %s (%d): %s" CRLF, pcUser, (int)tUid, strerror(errno));
                  fOkee = FALSE;
               }
               else 
               {
                  printf("rpi-SetupUser():Now User %s (%d)" CRLF, pcUser, (int)tUid);
               }
            }
            else 
            {
               printf("rpi-SetupUser():Error setting up User %s" CRLF, pcUser);
               fOkee = FALSE;
            }
         }
      }
      else printf("rpi-SetupUser():Error changing attributes for User %s" CRLF, pcUser);
   }
   else
   {
      printf("rpi-SetupUser():RpiDraw must be started as Root !" CRLF);
      fOkee = FALSE;
   }
   return(fOkee);
}

//
//  Function:  rpi_Startup
//  Purpose:   Startup all threads
//
//  Parms:     
//  Returns:   Init markers 
//
static int rpi_Startup()
{
   int   iStup=0;

   //
   // Start aux threads:
   //
   iStup |= USB_InitWrite();
   iStup |= USB_InitDebug();
   iStup |= GRD_Init();
   iStup |= CNC_Init();
   iStup |= UDP_Init();
   iStup |= SRVR_Init();
   //
   PRINTF("rpi-Startup():Startup flags = 0x%08X" CRLF, iStup);
   return(iStup);
}

//
//  Function:  rpi_WaitStartup
//  Purpose:   Wait until all threads are doing fine
//
//  Parms:     Initial flags of all threads which started
//  Returns:   True if all OK
//
static bool rpi_WaitStartup(int iInit)
{
   bool     fWaiting=TRUE;
   bool     fCc=FALSE;
   int      iStartup=0, iHalfSecs = 20;

   //
   // Mask only INIT threads
   //
   iInit &= GLOBAL_XXX_INI;
   //
   while(fWaiting)
   {
      switch( GLOBAL_SemaphoreWait(PID_HOST, 500) )
      {
         case 0:
            // Thread signalled: verify completion
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_GRD_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():Guard  OKee");  iStartup |= GLOBAL_GRD_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_SVR_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():Server OKee");  iStartup |= GLOBAL_SVR_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_UWR_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():USB Wr OKee");  iStartup |= GLOBAL_UWR_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_UDB_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():USB Db OKee");  iStartup |= GLOBAL_UDB_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_UDP_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():UDP    OKee");  iStartup |= GLOBAL_UDP_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_CNC_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():CNC    OKee");  iStartup |= GLOBAL_CNC_INI;}
            //
            if(iStartup == iInit)
            {
               PRINTF("rpi-WaitStartup():All inits OKee" CRLF);
               LOG_Report(0, "HST", "rpi-WaitStartup():All inits OKee");
               fWaiting = FALSE;
               fCc      = TRUE;
            }
            else
            {
               PRINTF("rpi-WaitStartup(): Waiting for 0x%08X (now 0x%08X)" CRLF, iInit, iStartup);
            }
            break;

         case -1:
            // Error
            fWaiting = FALSE;
            break;

         case 1:
            if(iHalfSecs-- == 0)
            {
               // Timeout: problems
               LOG_Report(0, "HST", "rpi-WaitStartup(): Timeout: Init=0x%08X, Actual==0x%08X", iInit, iStartup);
               PRINTF("rpi-WaitStartup(): Timeout: Init=0x%08X, Actual==0x%08X" CRLF, iInit, iStartup);
               fWaiting = FALSE;
            }
            break;
      }
      if(iStartup == iInit)
      {
         LOG_Report(0, "HST", "rpi-WaitStartup():All inits OKee");
         fWaiting = FALSE;
         fCc      = TRUE;
      }
      else
      {
         PRINTF("rpi-WaitStartup(): Waiting for 0x%08X (now 0x%08X)" CRLF, iInit, iStartup);
      }
   }
   return(fCc);
}


/* ======   Local Functions separator ===========================================
__PARENT_FUNCTIONS(){}
==============================================================================*/

//
//  Function:   rpi_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool rpi_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGUSR1:
  if( signal(SIGUSR1, &rpi_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "HST", "rpi-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &rpi_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "HST", "rpi-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &rpi_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "HST", "rpi-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &rpi_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "HST", "rpi-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  // Segmentation fault
  if( signal(SIGSEGV, &rpi_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "HST", "rpi-ReceiveSignalSegmnt(): SIGSEGV ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGSEGV);
  }
  return(fCC);
}

//
//  Function:   rpi_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalTerm(int iSignal)
{
   PRINTF("rpi-ReceiveSignalTerm()" CRLF);
   LOG_Report(0, "HST", "rpi-ReceiveSignalTerm()");
   //
   fRpiRunning = FALSE;
}

//
//  Function:   rpi_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalInt(int iSignal)
{
   PRINTF("rpi-ReceiveSignalInt()" CRLF);
   LOG_Report(0, "HST", "rpi-ReceiveSignalInt()");
   //
   fRpiRunning = FALSE;
}

//
// Function:   rpi_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void rpi_ReceiveSignalSegmnt(int iSignal)
{
   GLOBAL_SegmentationFault(__FILE__, __LINE__);
   LOG_SegmentationFault(__FILE__, __LINE__);
   exit(EXIT_CC_GEN_ERROR);
}

//
// Function:   rpi_ReceiveSignalUser1
// Purpose:    SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR1:
//
static void rpi_ReceiveSignalUser1(int iSignal)
{
   PRINTF("rpi-ReceiveSignalUser1()" CRLF);
}

//
// Function:   rpi_ReceiveSignalUser2
// Purpose:    System callback on receiving the SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       
//
static void rpi_ReceiveSignalUser2(int iSignal)
{
   PRINTF("rpi-ReceiveSignalUser2()" CRLF);
}

/*------  Local functions separator ------------------------------------
_____________________TEST_DEBUG(){};
------------------------------x----------------------------------------*/

//
//  Function:  rpi_TestMmap
//  Purpose:   Test/list all MMAP vars
//
//  Parms:     
//  Returns:   
//
static void rpi_TestMmap()
{
   void    *pvVar;
   int      iSize, iVal;
   GLOPAR   x;
   const GLOBALS *pstGloPar;

   pstGloPar = GLOBAL_GetParameters();
   //
   GEN_Printf(CRLF);
   for(x=0; x<NUM_GLOBAL_DEFS; x++)
   {
      pvVar = GLOBAL_GetParameter(x);
      iSize = GLOBAL_GetParameterSize(x);
      iVal  = pstGloPar[x].iValueOffset;
      //
      GEN_Printf("%2d: Map=%p,  Offset=%4X (%5d)  Addr=%p  Size=%3d   ", x, pstMap, iVal, iVal, pvVar, iSize);
      //
      switch(GLOBAL_GetParameterType(x))
      {
         case PAR_A: 
            // Parm is ASCII
            GEN_Printf("(A):%s" CRLF, (char *)pvVar);
            break;

         case PAR_B: 
            // Parm is BCD
            GEN_Printf("(B):%d" CRLF, *(int *)pvVar);
            break;

         case PAR_F: 
            // Parm is FLOAT
            GEN_Printf("(F):%f" CRLF, *(double *)pvVar);
            break;

         case PAR_H: 
            // Parm is HEX
            GEN_Printf("(H):0x%x" CRLF, *(int *)pvVar);
            break;

         default:
            GEN_Printf("(-)" CRLF);
            break;
      }
   }
   GEN_Printf(CRLF);
}


