/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           config.h
 *  Purpose:            Configuration headerfile
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *    02 Jun 2022:      Add Log Flush at midnight
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CONFIG_H_
#define _CONFIG_H_

//=============================================================================
// Feature switches
//
// makefile:
// If make debug:
//    -D DEBUG_ASSERT
//    -D FEATURE_USE_PRINTF (or overrule here)
//
// #define FEATURE_USE_PRINTF                // Release mode: disable PRINTFs
   #define FEATURE_USB_DEBUG_TRACES          // Use USB debug traces
// #define FEATURE_RUN_USERSPACE             // Run app in userspace
// #define FEATURE_COMM_NON_BLOCKING         // USB Serial ports

//=============================================================================
//
// Verbose flags
// +-------+-------+-------+-------+
// | Level |     24 Flags          |
// +-------+-------+-------+-------+
//
#define VERBOSE_LEVEL_MASK    0xFF000000  // Verbose level mask
#define VERBOSE_LEVEL_SHIFT   24          // Verbose level shift
//
//                                           Persistent G_iVerbose
//                                           | Overrule: rpidraw -v 0x240
#define FLAGS_LOG_JSON        0x00000001  // LOG JSON object data
#define FLAGS_LOG_DRAW        0x00000002  // LOG DRAW  data
//                            0x00000004  // 
//                            0x00000008  // 
#define FLAGS_LOG_CNC         0x00000010  // LOG CNC   incoming serial port data
#define FLAGS_LOG_DEB         0x00000020  // LOG Debug incoming serial port data
#define FLAGS_PRN_CNC         0x00000040  // PRN CNC   incoming serial port data
#define FLAGS_PRN_DEB         0x00000080  // PRN Debug incoming serial port data
//
// IO-Options are now defined in the makefile and the actual board itself:
// Rpi-Proj root: file RPIBOARD --> decimal number 1...?
//
// IO:   select define $(INOUT):
//       FEATURE_IO_NONE                  // IO : No IO used !
//       FEATURE_IO_PATRN                 // IO : Patrn.nl board
//       FEATURE_IO_IQAUDIO               // IO : IO from IQaudIO-Zero board
//

//=============================================================================
#ifdef   FEATURE_IO_NONE      // IO : NO IO board used
//=============================================================================
#define  DIO_USE_NONE         //No IO
#define  LCD_USE_NONE         //No LCD screen
//
#define  RPI_IO_BOARD         "RpiDraw uses NO IO"
//
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  IO_BUTTON
//
#define  INP_GPIO(x)        
#define  OUT_GPIO(x)     
#define  IN(x)                FALSE
#define  OUT(x, y)
#endif

//=============================================================================
#ifdef   FEATURE_IO_PATRN     // IO : Use Patrn.nl board
//=============================================================================
#include <wiringPi.h>
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_NONE         //No LCD screen
//
#define  RPI_IO_BOARD         "RpiDraw uses PatrnBoard"
//
#define  LED_R                0
#define  LED_Y                7
#define  LED_G                3
#define  LED_W                6
//
#define  IO_BUTTON            1
//
#define  INP_GPIO(x)          pinMode(x, INPUT)
#define  OUT_GPIO(x)          pinMode(x, OUTPUT)
#define  IN(x)                !digitalRead(x)
#define  OUT(x, y)            digitalWrite(x, y)
#endif

//=============================================================================
#ifdef   FEATURE_IO_IQAUDIO   // IO : IO from IQaudIO-Zero board
//=============================================================================
#include <wiringPi.h>
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_NONE         //No LCD screen
//
#define  RPI_IO_BOARD         "RpiDraw uses IQaudIO-Zero"
//
#define  LED_R                5     // GPIO24 Pin #18
#define  LED_G                4     // GPIO23 Pin #16
#define  LED_Y
#define  LED_W
//
#define  IO_BUTTON            2     // GPIO27 Pin #13
//
#define  INP_GPIO(x)          pinMode(x, INPUT)
#define  OUT_GPIO(x)          pinMode(x, OUTPUT)
#define  IN(x)               !digitalRead(x)
#define  OUT(x, y)            digitalWrite(x, y)
#endif

#define DATA_VALID_SIGNATURE        0xDEADBEEF           // Valid signature
#define DATA_VERSION_MAJOR          2                    // Change to clear MMAP
#define DATA_VERSION_MINOR          0                    // Does NOT clear MMAP !
//
// The build process determines RELEASE or DEBUG builds through the
// DEBUG_ASSERT compile-time switch.
//
#ifdef  DEBUG_ASSERT
#define VERSION                     "1.00-CR015"
#define RPI_BASE_NAME               "rpidraw"
//
#else   //DEBUG_ASSERT
#define VERSION                     "1.00.015"
#define RPI_BASE_NAME               "rpidraw"
//
#endif  //DEBUG_ASSERT

#define RPI_MAP_FILE                RPI_BASE_NAME ".map"
#define RPI_LOG_FILE                RPI_BASE_NAME ".log"
#define RPI_ERR_FILE                RPI_BASE_NAME ".err"
#define RPI_CSV_FILE                RPI_BASE_NAME ".csv"
#define RPI_SHELL_FILE              RPI_BASE_NAME ".txt"
#define RPI_ALL_FILES               RPI_BASE_NAME ".*"
//
// Target RAM disk for high load file access
//
#define RPI_APP_TEXT                "RpiDraw"
#define RPI_WORK_DIR                "/mnt/rpicache/"
#define RPI_RAMFS_DIR               "/mnt/rpicache/"
#define RPI_BACKUP_DIR              "/usr/local/share/rpi/"
#define RPI_PUBLIC_WWW              "/home/public/www/"
#define RPI_PUBLIC_DEFAULT          "index.html"
//
#define RPI_LOG_PATH                RPI_WORK_DIR RPI_LOG_FILE
#define RPI_MAP_PATH                RPI_WORK_DIR RPI_MAP_FILE
#define RPI_CSV_PATH                RPI_WORK_DIR RPI_CSV_FILE
#define RPI_MAP_NEWPATH             RPI_WORK_DIR RPI_BASE_NAME "new.map"

#endif /* _CONFIG_H_ */
