/*  (c) Copyright:  2022  Patrn, Confidential Data
 *
 *  Workfile:           rpi_func.h
 *  Purpose:            Headerfile for rpi_func functions
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_FUNC_H_
#define _RPI_FUNC_H_

//
// Directory/File Attributes
//
#define  ATTR_RW_R__R__       (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)
#define  ATTR_RW_RW_R__       (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH)
#define  ATTR_RWXR_XR_X       (S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH)
#define  ATTR_RWXRWXR_X       (S_IRWXU|S_IRWXG|S_IROTH|S_IXOTH)
//
// Global functions
//
bool  RPI_DirectoryCreate           (char *, mode_t, bool);
bool  RPI_DirectoryExists           (char *);
bool  RPI_FileExists                (char *);
bool  RPI_GetFlag                   (int);
int   RPI_GetFlags                  (void);
int   RPI_GetVerboseLevel           (void);

#endif /* _RPI_FUNC_H_ */
