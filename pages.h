/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           pages.h
 *  Purpose:            Dynamic HTML snd JSON pages
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// The default (empty) URL page MUST be the first in this list
//
//          tUrl,             tType,         iTimeout,   iFlags,        pcUrl,               pfDynCb;
EXTRACT_DYN(DYN_HTML_EMPTY,   HTTP_HTML,     0,          DYN_FLAG_PORT, "",                  http_DynPageEmpty          )
EXTRACT_DYN(DYN_HTML_INFO,    HTTP_HTML,     0,          DYN_FLAG_PORT, "info.html",         http_DynPageDefault        )
EXTRACT_DYN(DYN_JSON_INFO,    HTTP_JSON,     0,          DYN_FLAG_PORT, "info.json",         http_DynPageDefault        )
EXTRACT_DYN(DYN_NONE_PARMS,   HTTP_JSON,     0,          DYN_FLAG_PORT, "parms",             http_DynPageParms          )
EXTRACT_DYN(DYN_JSON_PARMS,   HTTP_JSON,     0,          DYN_FLAG_PORT, "parms.json",        http_DynPageParms          )
EXTRACT_DYN(DYN_HTML_PARMS,   HTTP_HTML,     0,          DYN_FLAG_PORT, "parms.html",        http_DynPageParms          )
EXTRACT_DYN(DYN_NONE_LOG,     HTTP_HTML,     0,          DYN_FLAG_PORT, "log",               http_DynPageLog            )
EXTRACT_DYN(DYN_HTML_LOG,     HTTP_HTML,     0,          DYN_FLAG_PORT, "log.html",          http_DynPageLog            )
EXTRACT_DYN(DYN_JSON_LOG,     HTTP_JSON,     0,          DYN_FLAG_PORT, "log.json",          http_DynPageLog            )
EXTRACT_DYN(DYN_HTML_NEXIT,   HTTP_HTML,     0,          DYN_FLAG_NONE, "exit.html",         http_DynPageExit           )
EXTRACT_DYN(DYN_JSON_NEXIT,   HTTP_JSON,     0,          DYN_FLAG_PORT, "exit.json",         http_DynPageExit           )
EXTRACT_DYN(DYN_HTML_CSV,     HTTP_HTML,     0,          DYN_FLAG_PORT, "csv.html",          http_DynPageCsvFiles       )

