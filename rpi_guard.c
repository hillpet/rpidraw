/*  (c) Copyright:  2022 Patrn, Confidential Data
 *
 *  Workfile:           rpi_guard.c
 *  Purpose:            This thread monitors all other rpidraw threads
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
//
#include "rpi_main.h"
#include "rpi_func.h"
#include "rpi_guard.h"
//
#define USE_PRINTF
#include <printx.h>

//
// Local functions
//
static int     grd_Execute                (void);
//
static bool    grd_SignalRegister         (sigset_t *);
static void    grd_ReceiveSignalSegmnt    (int);
static void    grd_ReceiveSignalInt       (int);
static void    grd_ReceiveSignalTerm      (int);
static void    grd_ReceiveSignalUser1     (int);
static void    grd_ReceiveSignalUser2     (int);

//
// Local arguments
//
static int  fThreadRunning = TRUE;

/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   GRD_Init
// Purpose:    Handle running state monitor for PiKrellCam
//
// Parms:      
// Returns:    Exit codes
// Note:       Called from main() to split off the guard thread
//
int GRD_Init()
{
   int      iCc;
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         GLOBAL_PidPut(PID_GUARD, tPid);
         break;

      case 0:
         // Child:
         GEN_Sleep(500);
         iCc = grd_Execute();
         LOG_Report(0, "GRD", "GRD-Init():Exit normally, cc=%d", iCc);
         exit(iCc);
         break;

      case -1:
         // Error
         GEN_Printf("GRD-Init(): Error!" CRLF);
         LOG_Report(errno, "GRD", "GRD-Init():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(GLOBAL_GRD_INI);
}

/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   grd_Execute
// Purpose:    Handle CncDraw running state monitor
//
// Parms:      
// Returns:    Exit codes
// Note:       
//
static int grd_Execute()
{
   int      iOpt, iCc=EXIT_CC_OKEE;
   sigset_t tBlockset;
   PSAUX   *pstInfo;

   pstInfo = (PSAUX *)safemalloc(sizeof(PSAUX));
   //
   if(grd_SignalRegister(&tBlockset) == FALSE)
   {
      LOG_Report(errno, "GRD", "grd-Execute():Exit Register ERROR:");
      return(EXIT_CC_GEN_ERROR);
   }
   GLOBAL_SemaphoreInit(PID_GUARD);
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_GRD_INI);
   //
   // Init ready
   // Wait for Host Run/Execute
   //
   while(fThreadRunning)
   {
      iOpt = GLOBAL_SignalWait(PID_GUARD, GLOBAL_HST_ALL_RUN, 1000);
      if(iOpt == 0) break;
   }
   PRINTF("grd-Execute():Running" CRLF);
   //
   // Run/Execute
   // Main loop of the Guard: wait for the semaphore posted by the parent 
   //
   while(fThreadRunning)
   {
      iOpt = GLOBAL_SemaphoreWait(PID_GUARD, 5000);
      switch(iOpt)
      {
         case 0:
            if(GLOBAL_GetSignalNotification(PID_GUARD, GLOBAL_HST_ALL_MID)) 
            {
               // Midnight
               PRINTF("grd-Execute():Midnight" CRLF);
               LOG_Report(0, "GRD", "grd-Execute():Midnight");
            }
            break;

         case 1:
            // Timeout: 
            break;

         default:
         case -1:
            // Error
            LOG_Report(errno, "GRD", "grd-Execute():ERROR GLOBAL_SemaphoreWait");
            PRINTF("grd-Execute():ERROR GLOBAL_SemaphoreWait(errno=%d)" CRLF, errno);
            break;
      }
   }
   safefree(pstInfo);
   GLOBAL_SemaphoreDelete(PID_GUARD);
   return(iCc);
}


/*------  Local functions separator ------------------------------------
__SIGNAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

// 
// Function:   grd_SignalRegister
// Purpose:    Register all SIGxxx
// 
// Parameters: Blockset
// Returns:    TRUE if delivered
// 
static bool grd_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGSEGV:
  if( signal(SIGSEGV, &grd_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "GRD", "grd-SignalRegister(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // SIGUSR1:
  if( signal(SIGUSR1, &grd_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "GRD", "grd-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &grd_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "GRD", "grd-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &grd_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "GRD", "grd-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &grd_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "GRD", "grd-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
  }
  return(fCC);
}

//
// Function:   grd_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void grd_ReceiveSignalSegmnt(int iSignal)
{
   GLOBAL_SegmentationFault(__FILE__, __LINE__);
   LOG_SegmentationFault(__FILE__, __LINE__);
   exit(EXIT_CC_GEN_ERROR);
}

// 
// Function:   grd_ReceiveSignalTerm
// Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
// 
// Parameters: 
// Returns:    TRUE if delivered
// 
static void grd_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "GRD", "grd-ReceiveSignalTerm()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_GUARD);
}

//
// Function:   grd_ReceiveSignalInt
// Purpose:    System callback for SIGINT (Ctl-C)
//
// Parms:
// Returns:    TRUE if delivered
//
static void grd_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "GRD", "grd-ReceiveSignalInt()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_GUARD);
}

//
// Function:   grd_ReceiveSignalUser1
// Purpose:    System callback for SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:
//
static void grd_ReceiveSignalUser1(int iSignal)
{
}

//
// Function:   grd_ReceiveSignalUser2
// Purpose:    System callback for SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:
//
static void grd_ReceiveSignalUser2(int iSignal)
{
}


/*----------------------------------------------------------------------
______________COMMENT_FUNCTIONS(){}
------------------------------x----------------------------------------*/
#ifdef COMMENT

#endif   //COMMENT
