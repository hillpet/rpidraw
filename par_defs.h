/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           par_defs.h
 *  Purpose:            Extract macros for the Rpi Draw utility
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// Macro elements:                  Example
// 
//       a  Enum              x     PAR_VERSION
//       b  iFunction         x     WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL
//       c  pcJson            x     "Version"
//       d  pcHtml            x     "vrs="
//       e  pcOption          x     "-v"
//       f  iValueOffset      x     offsetof(RPIMAP, G_pcSwVersion)
//       g  iValueSize        x     MAX_PARM_LEN
//       h  iChangedOffset    x     ALWAYS_CHANGED or offsetof(RPIMAP, G_ubVersionChanged)
//       i  iChanged          x     0
//       j  pcDefault         x     "v1.00-cr103"
//       k  pfHttpFun         x     http_CollectXxxxxx
//
// Note: 
//       WB    (WarmBoot)  : the default value is restored on every restart of the App.
//       PAR_A (Ascii)     : ASCII parameter
//       PAR_F (Float)     : Floating point parameter (double representation)
//       PAR_B (Bcd)       : Integer parameter (BCD representation) 
//       PAR_H (Hex)       : Integer parameter (HEX representation) 
//
//       JSN_TXT           : JSON Text representation    ("Variable")
//       JSN_INT           : JSON Number representation  ( Variable )
//
//
// Macro    a                 b                                                                    c                 d        e        f                                      g                  h                                     i           j                        k
//          Enum,             ____iFunction___________________________________________________     pcJson,           pcHtml   pcOption iValueOffset                           iValueSize        iChangedOffset                         iChanged    pcDefault                pfHttpFun
//                               Txt/Int
EXTRACT_PAR(PAR_VERSION,     (WB|PAR_A|JSN_TXT|PAR_ALL|PAR____|PAR____|PAR____|PAR____|PAR____),   "Version",        "vrs=",  "",      offsetof(RPIMAP, G_pcVersionSw),       MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,          VERSION,                 NULL)
EXTRACT_PAR(PAR_COMMAND,     (WB|PAR_A|JSN_TXT|PAR_ALL|PAR____|PAR____|PAR____|PAR____|PAR____),   "Command",        "cmd=",  "",      offsetof(RPIMAP, G_pcCommand),         MAX_PARM_LEN,     offsetof(RPIMAP, G_ubCommandChanged),  0,          "",                      NULL)
EXTRACT_PAR(PAR_HOSTNAME,    (   PAR_A|JSN_TXT|PAR_ALL|PAR____|PAR____|PAR____|PAR____|PAR____),   "Hostname",       "",      "",      offsetof(RPIMAP, G_pcHostname),        MAX_URL_LEN,      ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_HOST_IP,     (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_PRM),   "HostIp",         "",      "",      offsetof(RPIMAP, G_pcHostIp),          MAX_ADDR_LEN,     NEVER_CHANGED,                         0,          "",                      NULL)
EXTRACT_PAR(PAR_HTTP_PORT,   (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_PRM),   "HttpPort",       "",      "",      offsetof(RPIMAP, G_iHttpPort),         sizeof(int),      NEVER_CHANGED,                         0,          "80",                    NULL)
EXTRACT_PAR(PAR_UDP_PORT,    (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_PRM),   "UdpPort",        "",      "",      offsetof(RPIMAP, G_iUdpPort),          sizeof(int),      NEVER_CHANGED,                         0,          "3000",                  NULL)
EXTRACT_PAR(PAR_WWW_DIR,     (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_PRM),   "WwwDir",         "",      "",      offsetof(RPIMAP, G_pcWwwDir),          MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          RPI_PUBLIC_WWW,          NULL)
EXTRACT_PAR(PAR_DEFAULT,     (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_PRM),   "Default",        "",      "",      offsetof(RPIMAP, G_pcDefault),         MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          RPI_PUBLIC_DEFAULT,      NULL)
EXTRACT_PAR(PAR_LAST_FILE,   (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_PRM),   "LastFile",       "",      "",      offsetof(RPIMAP, G_pcLastFile),        MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_GCODE_FILE,  (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_PRM),   "Gcode",          "",      "",      offsetof(RPIMAP, G_pcGcode),           MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_GCODE,       (WB|PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_PRM),   "RunGcode",       "",      "",      offsetof(RPIMAP, G_iGcode),            sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     http_CollectGcode)
//
EXTRACT_PAR(PAR_LOG,         (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_PRM),   "LogFile",        "",      "",      offsetof(RPIMAP, G_pcLogFile),         MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "",                      NULL)
EXTRACT_PAR(PAR_VERBOSE,     (   PAR_H|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_PRM),   "Verbose",        "",      "",      offsetof(RPIMAP, G_iVerbose),          sizeof(int),      ALWAYS_CHANGED,                        0,          "0",                     NULL)
// USB
EXTRACT_PAR(PAR_USB_DEV0,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_PRM),   "UsbDev0",        "",      "",      offsetof(RPIMAP, G_pcUsbDev0),         MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "/dev/ttyACM0",          NULL)
EXTRACT_PAR(PAR_USB_DEV1,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_PRM),   "UsbDev1",        "",      "",      offsetof(RPIMAP, G_pcUsbDev1),         MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,          "/dev/ttyACM1",          NULL)
