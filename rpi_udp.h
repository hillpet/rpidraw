/*  (c) Copyright:  2022  Patrn, Confidential Data
 *
 *  Workfile:           rpi_udp.h
 *  Purpose:            Headerfile for UDP thread
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Created from template
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_UDP_H_
#define _RPI_UDP_H_


#define  UDP_PROTOCOL            "udp"
#define  MAX_DATAGRAM_SIZE       1500

//
// Global functions
//
int UDP_Init               (void);

#endif /* _RPI_UDP_H_ */
