/*  (c) Copyright:  2022 Patrn, Confidential Data
 *
 *  Workfile:           rpi_cnc.c
 *  Purpose:            This CNC thread handles the comms between rpidraw and the CNC App
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Created from template
 *    29 May 2022:      Separate USB Rd/Wr threads
 *    30 May 2022:      Add STH
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *               
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
//
#include "rpi_func.h"
#include "rpi_json.h"
#include "rpi_usb.h"
#include "rpi_sth.h"
#include "cnc_func.h"
//
#include "rpi_cnc.h"
//
#define USE_PRINTF
#include <printx.h>

//
// Local functions
//
static int     cnc_Execute                (void);
static void    cnc_HandleDebRecord        (RPIUSB *, CNCPICO *, char *, int);
static bool    cnc_HandleDebConnection    (RPIUSB *);
static void    cnc_HandleCmdRecord        (RPIUSB *, char *, int);
static bool    cnc_HandleCmdConnection    (RPIUSB *);
//
static bool    cnc_SignalRegister         (sigset_t *);
static void    cnc_ReceiveSignalSegmnt    (int);
static void    cnc_ReceiveSignalInt       (int);
static void    cnc_ReceiveSignalTerm      (int);
static void    cnc_ReceiveSignalUser1     (int);
static void    cnc_ReceiveSignalUser2     (int);
//
static CNCSTH  cnc_SthError               (const STH *, void *);
static CNCSTH  cnc_SthHome                (const STH *, void *);
static CNCSTH  cnc_SthReady               (const STH *, void *);
static CNCSTH  cnc_SthSetting1            (const STH *, void *);
static CNCSTH  cnc_SthSetting2            (const STH *, void *);
static CNCSTH  cnc_SthSetting3            (const STH *, void *);
static CNCSTH  cnc_SthFileOpen            (const STH *, void *);
static CNCSTH  cnc_SthFileRead            (const STH *, void *);
static CNCSTH  cnc_SthFileClose           (const STH *, void *);
//
static bool    cnc_NfyMidnight            (void *, CNCSTH);
static bool    cnc_NfyCmdConnection       (void *, CNCSTH);
static bool    cnc_NfyDebConnection       (void *, CNCSTH);
static bool    cnc_NfyCncExecute          (void *, CNCSTH);
static bool    cnc_NfyCncInitialize       (void *, CNCSTH);
static bool    cnc_NfyCmdRecord           (void *, CNCSTH);
static bool    cnc_NfyDebRecord           (void *, CNCSTH);
//
static char   *pcPicoDebInit        = "-$";
static char   *pcPicoCmdInit        = "N1\n";
//
// State handlers
//
#define  EXTRACT_STH(a,b,c,d,e)   {a,b,c,d,e},
static const STH stCncStates[] =
{
   #include "cncstates.h"
   {STH_CNC_ERROR, STH_CNC_ERROR, STH_CNC_ERROR, cnc_SthError, "STH Error state !"  }
};
#undef   EXTRACT_STH
//
// Notification handlers
//
static NFY stCncNotifications[] =
{
//    tPid     iNotify              pfCb(void *, CNCSTH)       tState
   {  PID_CNC, GLOBAL_HST_ALL_MID,  cnc_NfyMidnight,           0                 },
   {  PID_CNC, GLOBAL_CMD_CNC_CNX,  cnc_NfyCmdConnection,      0                 },
   {  PID_CNC, GLOBAL_DEB_CNC_CNX,  cnc_NfyDebConnection,      0                 },
   {  PID_CNC, GLOBAL_HST_CNC_INI,  cnc_NfyCncInitialize,      STH_CNC_SETTING1  },
   {  PID_CNC, GLOBAL_SVR_CNC_RUN,  cnc_NfyCncExecute,         STH_CNC_OPEN      },
   {  PID_CNC, GLOBAL_CMD_CNC_REC,  cnc_NfyCmdRecord,          0                 },
   {  PID_CNC, GLOBAL_DEB_CNC_REC,  cnc_NfyDebRecord,          0                 }
};
static int iNumNotifications = sizeof(stCncNotifications) / sizeof(NFY);
//
// Local arguments
//
static int     fCmdConnected  = FALSE;
static int     fDebConnected  = FALSE;
static int     fThreadRunning = TRUE;
static CNCSTH  tCncState      = -1;

//
// CNC parameter list:
//
// iFunction:        Define if a parameter affects certain CNC function
//                      JSN_TXT : For JSON API: parameter is formatted as JSON text    ("value" = "xxxxx")
//                      JSN_INT :               parameter is formatted as JSON number  ("value" = xxxxx)
//
//                      CNC_PIX : Paramater concerns Picture file retrieval
//                      CNC_PRM :                    Parameter definition
//                      CNC_STP :                    Stopping command-in-progress
//                      CNC_ALL :                    All HTTP commands
//                      CNC____ :                    None of the commands
//
// iPrec:            Precision
// pcJson:           JSON API parameter name
// iMapOffset:       Global mmap structure offset of the parameter
// iSize:            Global mmap parameter size (0 if value)
//
// CNC Stoare/Vars xref list
//
#define  EXTRACT_CNC(a,b,c,d,e,f)   {b,c,d,e,f},
static const CNCPAR stCncParameters[] =
{
   #include "par_cnc.h"
   {0, 0, NULL, 0, 0  }
};
#undef   EXTRACT_CNC

/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

/*
 * Function    : CNC_BuildJsonObject
 * Description : Build applicable JSON array objects
 *
 * Parameters  : Buffer, size, Parameter set
 * Returns     : Actual JSON object size
 * Note        : JSON object returns:
 *                {
 *                   "Command":"xxxxxxxx",
 *                   "...": "....",
 *                   "Number": "XXX"
 *                }
 *
 */
int CNC_BuildJsonObject(char *pcBuffer, int iLength)
{
   int            iSize=0;
   char           *pcValue;
   char           *pcTemp;
   RPIDATA        *pstJson=NULL;
   const CNCPAR   *pstParm=stCncParameters;

   pcTemp = (char *) safemalloc(MAX_PATH_LEN);
   //
   // Build JSON object:
   // {
   //    "Command":"xxxxxxxx",
   //
   pstJson = JSON_InsertParameter(pstJson, "Command", "variables", JE_CURLB|JE_TEXT|JE_COMMA|JE_CRLF);
   //
   // Insert global JSON info:
   //    "xxxx":"xxxxxxxx",
   //
   while(pstParm->iFunction)
   {
      //
      // This data is encapsulated in the G_stCnc data structure
      //
      pcValue = CNC_GetFromValue(pstParm, pcTemp, MAX_PATH_LEN);
      //
      if(pcValue)
      {
         PRINTF("CNC-BuildJsonObject():%10s=[%s]" CRLF, pstParm->pcJson, pcValue);
         if(pstParm->iFunction & JSN_TXT) pstJson = JSON_InsertParameter(pstJson, pstParm->pcJson, pcValue, JE_TEXT|JE_COMMA|JE_CRLF);
         else                             pstJson = JSON_InsertParameter(pstJson, pstParm->pcJson, pcValue,         JE_COMMA|JE_CRLF);
      }
      else
      {
         PRINTF("CNC-BuildJsonObject():%10s=Not found !!" CRLF, pstParm->pcJson);
      }
      pstParm++;
   }
   pstJson = JSON_TerminateObject(pstJson);
   //
   // return the resulting JSON object, if it fits !
   //
   iSize = JSON_GetObjectSize(pstJson);
   if(iSize < iLength)
   {
      GEN_STRCPY(pcBuffer, pstJson->pcObject);
   }
   JSON_ReleaseObject(pstJson);
   //
   safefree(pcTemp);
   return(iSize);
}

//
// Function:   CNC_Init
// Purpose:    Handle running state monitor for PiKrellCam
//
// Parms:      
// Returns:    Exit codes
// Note:       Called from main() to split off the guard thread
//
int CNC_Init()
{
   int      iCc;
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         GLOBAL_PidPut(PID_CNC, tPid);
         break;

      case 0:
         // Child:
         GEN_Sleep(500);
         iCc = cnc_Execute();
         LOG_Report(0, "CNC", "CNC-Init():Exit normally, cc=%d", iCc);
         exit(iCc);
         break;

      case -1:
         // Error
         GEN_Printf("CNC-Init(): Error!" CRLF);
         LOG_Report(errno, "CNC", "CNC-Init():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(GLOBAL_CNC_INI);
}

/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   cnc_Execute
// Purpose:    Handle CncDraw running state monitor
//
// Parms:      
// Returns:    Exit codes
// Note:       
//
static int cnc_Execute()
{
   int      iOpt, iCc=EXIT_CC_OKEE;
   int      iTimeout=1000;
   sigset_t tBlockset;
   RPIXFR  *pstRpiXfr;

   if(cnc_SignalRegister(&tBlockset) == FALSE)
   {
      LOG_Report(errno, "CNC", "cnc-Execute():Exit Register ERROR:");
      return(EXIT_CC_GEN_ERROR);
   }
   pstRpiXfr = safemalloc(sizeof(RPIXFR));
   GEN_MEMSET(pstRpiXfr, 0x00, sizeof(RPIXFR));
   pstRpiXfr->iMax   = MAX_ARG_LEN;
   pstRpiXfr->pstUsb = &pstMap->G_stCmd;
   //
   STH_Init(stCncStates);
   GLOBAL_SemaphoreInit(PID_CNC);
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_CNC_INI);
   //
   // Init ready
   // Wait for Host Run/Execute
   //
   while(fThreadRunning)
   {
      iOpt = GLOBAL_SignalWait(PID_CNC, GLOBAL_HST_ALL_RUN, 1000);
      if(iOpt == 0) break;
   }
   PRINTF("cnc-Execute():Running" CRLF);
   //
   // Run/Execute
   //
   while(fThreadRunning)
   {
      iOpt = GLOBAL_SemaphoreWait(PID_CNC, iTimeout);
      switch(iOpt)
      {
         case 0:
            GLOBAL_ExecuteNotifications(stCncNotifications, iNumNotifications, pstRpiXfr);
            break;

         case 1:
            // Timeout: 
            // Flush XMT buffers (if any)
            USB_Flush(&pstMap->G_stCmd);
            USB_Flush(&pstMap->G_stDeb);
            //
            // Run STH
            //
            //PRINTF("cnc-Execute():Run State=%d" CRLF, tCncState);
            if(tCncState == -1) iTimeout = 1000;
            else              
            {
               tCncState = STH_Execute(stCncStates, tCncState, pstRpiXfr);
               PRINTF("cnc-Execute():Return State=%d" CRLF, tCncState);
               iTimeout = 100;
            }
            break;

         default:
         case -1:
            // Error
            LOG_Report(errno, "CNC", "cnc-Execute():ERROR GLOBAL_SemaphoreWait");
            PRINTF("cnc-Execute():ERROR GLOBAL_SemaphoreWait(errno=%d)" CRLF, errno);
            break;
      }
   }
   safefree(pstRpiXfr);
   GLOBAL_SemaphoreDelete(PID_CNC);
   return(iCc);
}

/*------  Local functions separator ------------------------------------
______________GENERIC_FUNCTIONS(){};
------------------------------X---------------------------------------------*/

//
// Function:   cnc_HandleDebRecord
// Purpose:    Handle Cnc debug record
//
// Parms:      USB Rcv, Dest struct, Buffer, buffer max len
// Returns:    
// Note:       
//
static void cnc_HandleDebRecord(RPIUSB *pstUsb, CNCPICO *pstPico, char *pcBuffer, int iLen)
{
   USB_CopyRecord(pstUsb, pcBuffer, iLen);
   if(CNC_UpdatePicoVariable(pstPico, pcBuffer))   
   {
      PRINTF("cnc-HandleDebRecord():Pico var(s):[%s]" CRLF, pcBuffer);
   }
   else
   {
      PRINTF("cnc-HandleDebRecord():Pico var(s):[%s] Not found" CRLF, pcBuffer);
   }
}

//
// Function:   cnc_HandleDebConnection
// Purpose:    Handle Cnc Debug record
//
// Parms:      USB struct
// Returns:    
// Note:       
//
static bool cnc_HandleDebConnection(RPIUSB *pstUsb)
{
   bool  fCc=FALSE;

   if(!USB_ConnectedWrite(pstUsb))
   {
      USB_OpenWrite(pstUsb);
   }
   //
   // If Open OK: write info to USB Port
   //
   if(USB_ConnectedWrite(pstUsb))
   {
      //
      // Request initial PICO debug data
      //
      pstUsb->pcXmt = pcPicoDebInit;
      if( USB_Write(pstUsb) >= 0) fCc = TRUE;
      else PRINTF("cnc-HandleDebConnection():write ERROR:%s" CRLF, strerror(errno));
   }
   return(fCc);
}

//
// Function:   cnc_HandleCmdRecord
// Purpose:    Handle Cnc Command record
//
// Parms:      USB Rcv, Buffer, buffer max len
// Returns:    
// Note:       Returns from Pico are like:
//             "STOP(00):[1023] N1 X0 Y0 Z0 \n"
//
static void cnc_HandleCmdRecord(RPIUSB *pstUsb, char *pcBuffer, int iLen)
{
   CNCVAR *pstCncVar = &pstMap->G_stCncVar;

   USB_CopyRecord(pstUsb, pcBuffer, iLen);
   if(CNC_ParsePicoStatus(pstCncVar, pcBuffer))
   {
      //PRINTF("cnc-HandleCmdRecord():" CRLF);
      //PRINTF("cnc-HandleCmdRecord(): Status %d"   CRLF, pstCncVar->tStatus);
      //PRINTF("cnc-HandleCmdRecord(): Axis   %02X" CRLF, pstCncVar->iCncAxis);
      PRINTF("cnc-HandleCmdRecord(): Free   %d"   CRLF, pstCncVar->iCncFree);
      PRINTF("cnc-HandleCmdRecord(): Line   %d"   CRLF, pstCncVar->iCncLine);
      //PRINTF("cnc-HandleCmdRecord(): X-Pos  %d"   CRLF, pstCncVar->iCncXpos);
      //PRINTF("cnc-HandleCmdRecord(): Y-Pos  %d"   CRLF, pstCncVar->iCncYpos);
      //PRINTF("cnc-HandleCmdRecord(): Z-Pos  %d"   CRLF, pstCncVar->iCncZpos);
   }
   else PRINTF("cnc-HandleCmdRecord():ERROR [%s]" CRLF, pcBuffer);
}

//
// Function:   cnc_HandleCmdConnection
// Purpose:    Handle Cnc record
//
// Parms:      USB struct
// Returns:    
// Note:       
//
static bool cnc_HandleCmdConnection(RPIUSB *pstUsb)
{
   bool  fCc=FALSE;

   if(!USB_ConnectedWrite(pstUsb))
   {
      USB_OpenWrite(pstUsb);
   }
   //
   // If Open OK: Setup STH to run initial setup G-Code
   //
   if(USB_ConnectedWrite(pstUsb))
   {
      //
      // Request initial PICO Command data
      //
      pstUsb->pcXmt = pcPicoCmdInit;
      if( USB_Write(pstUsb) >= 0) fCc = TRUE;
      else PRINTF("cnc-HandleCmdConnection():write ERROR:%s" CRLF, strerror(errno));
      //
      tCncState = STH_CNC_INIT;
      fCc       = TRUE;
   }
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__________________NFY_FUNCTIONS(){};
------------------------------X---------------------------------------------*/

//
//  Function:  cnc_NfyMidnight
//  Purpose:   Notification Midnight
//
//  Parms:     Parameter ptr, STH State
//  Returns:   
//
static bool cnc_NfyMidnight(void *pvParm, CNCSTH tState)
{
   LOG_Report(0, "CNC", "cnc-NfyMidnight():Midnight");
   return(TRUE);
}

//
//  Function:  cnc_NfyCmdConnection
//  Purpose:   Notification USB CNC command port
//             CNC Command USB connection changed
//  Parms:     Parameter ptr, STH State
//  Returns:   
//
static bool cnc_NfyCmdConnection(void *pvParm, CNCSTH tState)
{
   fCmdConnected = cnc_HandleCmdConnection(&pstMap->G_stCmd);
   LOG_Report(0, "CNC", "cnc-NfyCmdConnection():%d", fCmdConnected);
   return(TRUE);
}

//
//  Function:  cnc_NfyDebConnection
//  Purpose:   Notification CNC Debug port
//             CNC Debug USB connection changed
//  Parms:     Parameter ptr, STH State
//  Returns:   
//
static bool cnc_NfyDebConnection(void *pvParm, CNCSTH tState)
{
   fDebConnected = cnc_HandleDebConnection(&pstMap->G_stDeb);
   LOG_Report(0, "CNC", "cnc-NfyCmdConnection():%d", fDebConnected);
   return(TRUE);
}

//
//  Function:  cnc_NfyCncExecute
//  Purpose:   Notification STH Execution
//
//  Parms:     Parameter ptr, STH State
//  Returns:   
//
static bool cnc_NfyCncExecute(void *pvParm, CNCSTH tState)
{
   tCncState = STH_Execute(stCncStates, tState, NULL);
   LOG_Report(0, "CNC", "cnc-NfyCncExecute():%d", tCncState);
   return(TRUE);
}

//
//  Function:  cnc_NfyCncInitialize
//  Purpose:   Notification STH Init Execution
//
//  Parms:     Parameter ptr, STH State
//  Returns:   
//
static bool cnc_NfyCncInitialize(void *pvParm, CNCSTH tState)
{
   tCncState = STH_Execute(stCncStates, tState, NULL);
   LOG_Report(0, "CNC", "cnc-NfyCncInitialize():%d", tCncState);
   return(TRUE);
}

//
//  Function:  cnc_NfyCmdRecord
//  Purpose:   Notification CNC Command record
//
//  Parms:     Parameter ptr, STH State
//  Returns:   
//
static bool cnc_NfyCmdRecord(void *pvParm, CNCSTH tState)
{
   char    *pcBuffer;

   pcBuffer = (char *) safemalloc(MAX_ARG_LENZ);
   cnc_HandleCmdRecord(&pstMap->G_stCmd, pcBuffer, MAX_ARG_LEN);
   safefree(pcBuffer);
   return(TRUE);
}

//
//  Function:  cnc_NfyDebRecord
//  Purpose:   Notification CNC Debug record
//
//  Parms:     Parameter ptr, STH State
//  Returns:   
//
static bool cnc_NfyDebRecord(void *pvParm, CNCSTH tState)
{
   CNCPICO  stPico;
   char    *pcBuffer;

   pcBuffer = (char *) safemalloc(MAX_ARG_LENZ);
   //
   GEN_MEMSET(&stPico, 0x00, sizeof(CNCPICO));
   //
   cnc_HandleDebRecord(&pstMap->G_stDeb, &stPico, pcBuffer, MAX_ARG_LEN);
   // PRINTF("cnc-NfyDebRecord():Pico Secs=%d" CRLF, stPico.iSeconds);
   // PRINTF("cnc-NfyDebRecord():Pico Pos =%d, %d, %d" CRLF, stPico.iPosCur[0],  stPico.iPosCur[1],  stPico.iPosCur[2]);
   // PRINTF("cnc-NfyDebRecord():Pico Axis=%d, %d, %d" CRLF, stPico.iRunAxis[0], stPico.iRunAxis[1], stPico.iRunAxis[2]);
   // PRINTF("cnc-NfyDebRecord():Pico Stop=%d, %d, %d" CRLF, stPico.iStopDel[0], stPico.iStopDel[1], stPico.iStopDel[2]);
   //
   safefree(pcBuffer);
   return(TRUE);
}

/*------  Local functions separator ------------------------------------
__________________STH_FUNCTIONS(){};
------------------------------X---------------------------------------------*/

//
//  Function:  cnc_SthError
//  Purpose:   Error state (generic)
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static CNCSTH cnc_SthError(const STH *pstSth, void *pvParm)
{
   return(pstSth->tNxtState);
}

//
//  Function:  cnc_SthFileClose
//  Purpose:   Close the G-Code file
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static CNCSTH cnc_SthFileClose(const STH *pstSth, void *pvParm)
{
   CNCSTH   tState;
   RPIXFR  *pstRpiXfr = (RPIXFR *)pvParm;

   PRINTF("cnc-SthFileClose()" CRLF);
   if(pstRpiXfr)
   {
      safeclose(pstRpiXfr->iFd);
      tState = pstSth->tNxtState;
   }
   else tState = pstSth->tErrState;
   return(tState);
}

//
//  Function:  cnc_SthFileOpen
//  Purpose:   Open the G_Code file
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static CNCSTH cnc_SthFileOpen(const STH *pstSth, void *pvParm)
{
   CNCSTH   tState=STH_CNC_OPEN;
   char    *pcGcode;
   RPIXFR  *pstRpiXfr=(RPIXFR *)pvParm;

   if(pstMap->G_stCncVar.iCncFree)
   {
      pcGcode = GLOBAL_GetParameter(PAR_GCODE_FILE);
      if( pcGcode && GEN_STRLEN(pcGcode) && pstRpiXfr )
      {
         PRINTF("cnc-SthFileOpen(): [%s]" CRLF, pcGcode);
         if( (pstRpiXfr->iFd = safeopen(pcGcode, O_RDONLY)) )
         {
            pstRpiXfr->iNr = pstMap->G_stCncVar.iCncFree;
            PRINTF("cnc-SthFileOpen():Free space = %d" CRLF, pstRpiXfr->iNr);
            tState = pstSth->tNxtState;
         }
         else tState = pstSth->tErrState;
      }
      else tState = pstSth->tErrState;
   }
   else PRINTF("cnc-SthFileOpen():Waiting for PICO info..." CRLF);
   return(tState);
}

//
//  Function:  cnc_SthFileRead
//  Purpose:   Read a record from the G-Code file and send it to the CNC
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static CNCSTH cnc_SthFileRead(const STH *pstSth, void *pvParm)
{
   int      iNr, iRd;
   CNCSTH   tState=STH_CNC_READ;
   RPIXFR  *pstRpiXfr=(RPIXFR *)pvParm;
   RPIUSB  *pstUsb=pstRpiXfr->pstUsb;

   PRINTF("cnc-SthFileRead()" CRLF);
   if(pstRpiXfr)
   {
      if(pstRpiXfr->iFd)
      {
         if(pstRpiXfr->iNr > pstRpiXfr->iMax) iNr = pstRpiXfr->iMax;
         else                                 iNr = pstRpiXfr->iNr;
         iRd = saferead(pstRpiXfr->iFd, pstRpiXfr->pcBuffer, iNr);
         //
         PRINTF("cnc-SthFileRead():Read %d bytes" CRLF, iRd);
         if(     iRd <  0) tState = pstSth->tErrState;
         else if(iRd == 0) tState = pstSth->tNxtState;
         else
         {
            pstRpiXfr->pcBuffer[iRd] = 0;
            pstUsb->pcXmt = pstRpiXfr->pcBuffer;
            if( USB_Write(pstUsb) >= 0) tState = pstSth->tErrState;
            else PRINTF("cnc-SthFileRead():write ERROR:%s" CRLF, strerror(errno));
            //
            // More to read
            //
            pstRpiXfr->iNr -= iRd;
         }
      }
      else tState = pstSth->tErrState;
   }
   else tState = pstSth->tErrState;
   return(tState);
}

//
//  Function:  cnc_SthHome
//  Purpose:   Move to the HOME position (Xpos=W/2, Ypos=0)
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static CNCSTH cnc_SthHome(const STH *pstSth, void *pvParm)
{
   CNCSTH   tState;

   tState = pstSth->tNxtState;
   return(tState);
}

//
//  Function:  cnc_SthReady
//  Purpose:   
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static CNCSTH cnc_SthReady(const STH *pstSth, void *pvParm)
{
   return(-1);
}

//
//  Function:  cnc_SthSetting1
//  Purpose:   Handle settings transfer
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//  Note:      
//
static CNCSTH cnc_SthSetting1(const STH *pstSth, void *pvParm)
{
   CNCSTH   tState;

   tState = pstSth->tNxtState;
   return(tState);
}

//
//  Function:  cnc_SthSetting2
//  Purpose:   Handle Pulsewidth settings transfer
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//  Note:      
//
static CNCSTH cnc_SthSetting2(const STH *pstSth, void *pvParm)
{
   CNCSTH   tState;

   tState = pstSth->tNxtState;
   return(tState);
}

//
//  Function:  cnc_SthSetting3
//  Purpose:   Handle Speed/Accel settings transfer
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//  Note:      
//
static CNCSTH cnc_SthSetting3(const STH *pstSth, void *pvParm)
{
   CNCSTH   tState;

   tState = pstSth->tNxtState;
   return(tState);
}


/*------  Local functions separator ------------------------------------
_______________SIGNAL_FUNCTIONS(){};
------------------------------X---------------------------------------------*/

// 
// Function:   cnc_SignalRegister
// Purpose:    Register all SIGxxx
// 
// Parameters: Blockset
// Returns:    TRUE if delivered
// 
static bool cnc_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGSEGV:
  if( signal(SIGSEGV, &cnc_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "CNC", "cnc-SignalRegister(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // SIGUSR1:
  if( signal(SIGUSR1, &cnc_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "CNC", "cnc-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &cnc_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "CNC", "cnc-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &cnc_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "CNC", "cnc-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &cnc_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "CNC", "cnc-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
  }
  return(fCC);
}

//
// Function:   cnc_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void cnc_ReceiveSignalSegmnt(int iSignal)
{
   GLOBAL_SegmentationFault(__FILE__, __LINE__);
   LOG_SegmentationFault(__FILE__, __LINE__);
   exit(EXIT_CC_GEN_ERROR);
}

// 
// Function:   cnc_ReceiveSignalTerm
// Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
// 
// Parameters: 
// Returns:    TRUE if delivered
// 
static void cnc_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "CNC", "cnc-ReceiveSignalTerm()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_CNC);
}

//
// Function:   cnc_ReceiveSignalInt
// Purpose:    System callback for SIGINT (Ctl-C)
//
// Parms:
// Returns:    TRUE if delivered
//
static void cnc_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "CNC", "cnc-ReceiveSignalInt()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_CNC);
}

//
// Function:   cnc_ReceiveSignalUser1
// Purpose:    System callback for SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:
//
static void cnc_ReceiveSignalUser1(int iSignal)
{
}

//
// Function:   cnc_ReceiveSignalUser2
// Purpose:    System callback for SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:
//
static void cnc_ReceiveSignalUser2(int iSignal)
{
}


/*----------------------------------------------------------------------
______________COMMENT_FUNCTIONS(){}
------------------------------x----------------------------------------*/
#ifdef COMMENT

{
   int      iRead, iTotal=0;
   FILE    *ptFile;
   char    *pcBuffer;
   char    *pcRead;

   ptFile = safefopen(pcFile, "r");

   if(ptFile)
   {
      pcBuffer = safemalloc(COPY_BUFSIZE);
      //
      PRINTF("json_InsertTextFile(): File=<%s>"CRLF, pcFile);
      do
      {
         pcRead = fgets(pcBuffer, COPY_BUFSIZE, ptFile);
         if(pcRead)
         {
            iRead = GEN_STRLEN(pcBuffer);
            //PRINTF("json_InsertTextFile(): %s(%d)"CRLF, pcBuffer, iRead);
            pstCam = json_Insert(pstCam, pcBuffer);
            iTotal += iRead;
         }
      }
      while(pcRead);
      //PRINTF("json_InsertTextFile(): %d bytes written."CRLF, iTotal);
      safefclose(ptFile);
      safefree(pcBuffer);
   }
   else
   {
      PRINTF("json_InsertTextFile(): Error open file <%s>"CRLF, pcFile);
   }
   return(pstCam);
}
#endif   //COMMENT
