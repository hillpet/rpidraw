/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           usb_db.h
 *  Purpose:            USB Debug functions header file
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from rpicnc
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _USB_DB_H_
#define _USB_DB_H_

//
// Global prototypes
//
int      USB_InitDebug        (void);

#endif  /*_USB_DB_H_ */

