/*  (c) Copyright:  2022 Patrn, Confidential Data
 *
 *  Workfile:           cnc_func.c
 *  Purpose:            CNC functions
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    26 Apr 2022:      Ported from rpicnc
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *               
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
//
#include "cnc_func.h"
//
#define USE_PRINTF
#include <printx.h>

//
// Local data
//
static const char *pcStop     = "STOP";
static const char *pcExec     = "EXEC";
static const char *pcOkee     = "OKEE";
static const char *pcNack     = "NACK";
//
static bool    cnc_ValueToStorageFloat       (char *, void *, int, int);
static bool    cnc_ValueToStorageText        (char *, void *, int, int);
static bool    cnc_ValueToStorageLong        (char *, void *, int, int);
static bool    cnc_ValueToStorageInt         (char *, void *, int, int);
//
// Pico Callback Functions
//
static bool    pico_Double                   (char *, const VARPICO *, CNCPICO *);
static bool    pico_Integer                  (char *, const VARPICO *, CNCPICO *);

static const VARPICO stCncPicoParameters[] =
{
#define  EXTRACT_PICO(a,b,c,d,e)            {b,c,d,e},
#include "par_pico.h"   
#undef   EXTRACT_PICO
};
static const int iNumCncPicoParameters = sizeof(stCncPicoParameters) / sizeof(VARPICO);

/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function    : CNC_InitParms
// Description : Init Cnc Parms
//
// Parameters  : 
// Returns     : TRUE if OKee
//
bool CNC_InitParms()
{
   return(TRUE);
}

//
// Function    : CNC_GetFromValue
// Description : Get correct data from the Values struct
//
// Parameters  : Parms struct, buffer^, size
// Returns     : Buffer^
//
char *CNC_GetFromValue(const CNCPAR *pstParm, char *pcDst, int iSize)
{
   int      iOffset;
   void    *pvSrc;
   RPICNC  *pstRpiCnc=&pstMap->G_stRpiCnc;

   iOffset = pstParm->iMapOffset;
   if(iOffset != -1)
   {
      pvSrc = (void *) pstRpiCnc + iOffset; 
      //
           if(pstParm->iFunction & CNC_FLT) cnc_ValueToStorageFloat(pcDst, pvSrc, iSize, pstParm->iPrec);
      else if(pstParm->iFunction & CNC_TXT) cnc_ValueToStorageText (pcDst, pvSrc, iSize, pstParm->iPrec);
      else if(pstParm->iFunction & CNC_LNG) cnc_ValueToStorageLong (pcDst, pvSrc, iSize, pstParm->iPrec);
      else if(pstParm->iFunction & CNC_INT) cnc_ValueToStorageInt  (pcDst, pvSrc, iSize, pstParm->iPrec);
   }
   return(pcDst);
}

//
// Function:   CNC_ParsePicoStatus
// Purpose:    ParsePico status
//
// Parms:      Dest struct, status
// Returns:    TRUE if OK
//
bool CNC_ParsePicoStatus(CNCVAR *pstSts, char *pcBuffer)
{
   int   iValue;
   char *pcSrc;
   char *pcRes;

   GEN_MEMSET(pstSts, 0x00, sizeof(CNCVAR));
   //
   pcSrc = &pcBuffer[CNC_POS_ABS_STATUS];
   PRINTF("CNC-ParsePicoStatus():[%s]" CRLF, pcSrc);
   if(     GEN_STRNCMP(pcSrc, pcStop, 4) == 0) pstSts->tStatus = CNC_ST_STOP;
   else if(GEN_STRNCMP(pcSrc, pcExec, 4) == 0) pstSts->tStatus = CNC_ST_EXEC;
   else if(GEN_STRNCMP(pcSrc, pcOkee, 4) == 0) pstSts->tStatus = CNC_ST_OKEE;
   else if(GEN_STRNCMP(pcSrc, pcNack, 4) == 0) pstSts->tStatus = CNC_ST_NACK;
   //
   // CNC Axis status
   pcSrc  = &pcBuffer[CNC_POS_ABS_AXIS];
   errno  = 0;
   iValue = (int) strtol(pcSrc, &pcRes, 10);
   if(errno) 
   {
      PRINTF("CNC-ParsePicoStatus():ERROR Axis [%s]" CRLF, pcSrc);
      return(FALSE);
   }
   pstSts->iCncAxis = iValue;
   //
   // CNC Receive buffer free
   pcSrc  = &pcBuffer[CNC_POS_ABS_FREE];
   errno  = 0;
   iValue = (int) strtol(pcSrc, &pcRes, 10);
   if(errno) 
   {
      PRINTF("CNC-ParsePicoStatus():ERROR Free space [%s]" CRLF, pcSrc);
      return(FALSE);
   }
   pstSts->iCncFree = iValue;
   //
   // CNC Line number
   pcSrc  = &pcRes[CNC_POS_REL_LINE];
   errno  = 0;
   iValue = (int) strtol(pcSrc, &pcRes, 10);
   if(errno) 
   {
      PRINTF("CNC-ParsePicoStatus():ERROR Line nr [%s]" CRLF, pcSrc);
      return(FALSE);
   }
   pstSts->iCncLine = iValue;
   //
   // CNC X position
   pcSrc  = &pcRes[CNC_POS_REL_XPOS];
   errno  = 0;
   iValue = (int) strtol(pcSrc, &pcRes, 10);
   if(errno) 
   {
      PRINTF("CNC-ParsePicoStatus():ERROR X pos [%s]" CRLF, pcSrc);
      return(FALSE);
   }
   pstSts->iCncXpos = iValue;
   //
   // CNC Y position
   pcSrc  = &pcRes[CNC_POS_REL_YPOS];
   errno  = 0;
   iValue = (int) strtol(pcSrc, &pcRes, 10);
   if(errno) 
   {
      PRINTF("CNC-ParsePicoStatus():ERROR Y pos [%s]" CRLF, pcSrc);
      return(FALSE);
   }
   pstSts->iCncYpos = iValue;
   //
   // CNC Z position
   pcSrc  = &pcRes[CNC_POS_REL_ZPOS];
   errno  = 0;
   iValue = (int) strtol(pcSrc, &pcRes, 10);
   if(errno) 
   {
      PRINTF("CNC-ParsePicoStatus():ERROR Z pos [%s]" CRLF, pcSrc);
      return(FALSE);
   }
   pstSts->iCncZpos = iValue;
   return(TRUE);
}

//
// Function:   CNC_UpdatePicoVariable
// Purpose:    Update Pico variable from the strcture list
//
// Parms:      Dest, Variable name
// Returns:    TRUE if updated
//
bool CNC_UpdatePicoVariable(CNCPICO *pstDest, char *pcParm)
{
   bool           fCc=FALSE;
   int            x, iLen;
   const VARPICO *pstPico=stCncPicoParameters;

   for(x=0; x<iNumCncPicoParameters; x++)
   {
      iLen = GEN_STRLEN(pstPico->pcParm);
      if(iLen)
      {
         if(GEN_STRNCMP(pcParm, pstPico->pcParm, iLen) == 0)
         {
            //PRINTF("CNC-UpdatePicoVariable():[%s]" CRLF, pcParm);
            //
            // Pico variable found, execute callback, if any
            //
            if(pstPico->pfCallback)
            {
               fCc = pstPico->pfCallback(&pcParm[iLen], pstPico, pstDest);
            }
            else fCc = TRUE;
            break;
         }
      }
      pstPico++;
   }
   return(fCc);
}

/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/


/*
 * Function    : cnc_ValueToStorageFloat
 * Description : Update the storage with actual data
 *
 * Parameters  : Dest, Src, size, precision
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_ValueToStorageFloat(char *pcDest, void *pvSrc, int iSize, int iPrec)
{
   float fVal;

   fVal = *(float *)pvSrc;
   //
   switch(iPrec)
   {
      default:
         snprintf(pcDest, iSize, "%f", fVal);
         break;

      case 0:
         snprintf(pcDest, iSize, "%.0f", fVal);
         break;

      case 1:
         snprintf(pcDest, iSize, "%.1f", fVal);
         break;

      case 2:
         snprintf(pcDest, iSize, "%.2f", fVal);
         break;

      case 3:
         snprintf(pcDest, iSize, "%.3f", fVal);
         break;

      case 4:
         snprintf(pcDest, iSize, "%.4f", fVal);
         break;

      case 5:
         snprintf(pcDest, iSize, "%.5f", fVal);
         break;
   }
   //PRINTF("cnc_ValueToStorageFloat():V=%f, S=%s" CRLF, fVal, pcDest);
   return(TRUE);
}

/*
 * Function    : cnc_ValueToStorageLong
 * Description : Update the storage with actual data
 *
 * Parameters  : Dest, Src, size, precision
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_ValueToStorageLong(char *pcDest, void *pvSrc, int iSize, int iPrec)
{
   long  lVal;

   lVal = *(long *)pvSrc;
   snprintf(pcDest, iSize, "%ld", lVal);
   //PRINTF("cnc_ValueToStorageLong():V=%ld, S=%s" CRLF, lVal, pcDest);
   return(TRUE);
}

/*
 * Function    : cnc_ValueToStorageInt
 * Description : Update the storage with actual data
 *
 * Parameters  : Dest, Src, size, precision
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_ValueToStorageInt(char *pcDest, void *pvSrc, int iSize, int iPrec)
{
   int iVal;

   iVal = *(int *)pvSrc;
   snprintf(pcDest, iSize, "%d", iVal);
   //PRINTF("cnc_ValueToStorageInt():V=%d, S=%s" CRLF, iVal, pcDest);
   return(TRUE);
}

/*
 * Function    : cnc_ValueToStorageText
 * Description : Update the storage with actual data
 *
 * Parameters  : Dest, Src, size, precision
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_ValueToStorageText(char *pcDest, void *pvSrc, int iSize, int iPrec)
{
   char  *pcVal;

   pcVal = (char *)pvSrc;
   strncpy(pcDest, pcVal, iSize);
   //PRINTF("cnc_ValueToStorageText():V=%s, S=%s" CRLF, pcVal, pcDest);
   return(TRUE);
}


/*----------------------------------------------------------------------
________PICO_CALLBACK_FUNCTIONS(){}
------------------------------x----------------------------------------*/

/*
 * Function    : pico_Double
 * Description : Callback for float parameter
 *
 * Parameters  : Source data ptr, parameter struct ptr, Dest struct ptr
 * Returns     : TRUE if OKee
 *
 */
static bool pico_Double(char *pcData, const VARPICO *pstParm, CNCPICO *pstPico)
{
   int      iOffset;
   int      iElements;
   double   flValue;
   double  *pflDest;
   char    *pcSrc=pcData;
   char    *pcRes;

   iOffset   = pstParm->iOffset;
   iElements = pstParm->iElements;

   if(iOffset != -1)
   {
      pflDest = (double *) ((void *)pstPico + iOffset);
      do
      {
         if( (pcSrc = GEN_FindDelimiter(pcSrc, DELIM_INTEGER)) == NULL)
         {
            PRINTF("pico-Double():ERROR: not found in [%s]" CRLF, pcData);
            return(FALSE);
         }
         errno   = 0;
         flValue = strtod(pcSrc, &pcRes);
         //
         // Store converted int if all went OKee
         //
         if(errno) 
         {
            PRINTF("pico-Double():ERROR: [%s]" CRLF, pcSrc);
            return(FALSE);
         }
         //
         // Store value and find out if there is more
         //
         //PRINTF("pico-Double():Found: %.3f [%s] Pico=%p + Offset=%d = Dest=%p" CRLF, flValue, pcSrc, pstPico, iOffset, pflDest);
         *pflDest++ = flValue;
         pcSrc      = pcRes;
      }
      while(--iElements);
   }
   return(TRUE);
}

/*
 * Function    : pico_Integer
 * Description : Callback for generic parameter
 *
 * Parameters  : Source data ptr, parameter struct ptr, Dest struct ptr
 * Returns     : TRUE if OKee
 *
 */
static bool pico_Integer(char *pcData, const VARPICO *pstParm, CNCPICO *pstPico)
{
   int      iOffset, iValue;
   int      iElements;
   int     *piDest;
   char    *pcSrc=pcData;
   char    *pcRes;

   iOffset   = pstParm->iOffset;
   iElements = pstParm->iElements;

   if(iOffset != -1)
   {
      piDest = (int *) ((void *)pstPico + iOffset);
      do
      {
         if( (pcSrc = GEN_FindDelimiter(pcSrc, DELIM_INTEGER)) == NULL)
         {
            PRINTF("pico-Integer():ERROR: not found in [%s]" CRLF, pcData);
            return(FALSE);
         }
         errno  = 0;
         iValue = (int) strtol(pcSrc, &pcRes, 10);
         //
         // Store converted int if all went OKee
         //
         if(errno) 
         {
            PRINTF("pico-Integer():ERROR: [%s]" CRLF, pcSrc);
            return(FALSE);
         }
         //PRINTF("pico-Integer():Found: %d [%s] Pico=%p + Offset=%d = Dest=%p" CRLF, iValue, pcSrc, pstPico, iOffset, piDest);
         //
         // Store value and find out if there is more
         //
         *piDest++ = iValue;
         pcSrc     = pcRes;
      }
      while(--iElements);
   }
   return(TRUE);
}

