/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           cnc_wr.h
 *  Purpose:            CNC Write functions header file
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from rpicnc
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _USB_WR_H_
#define _USB_WR_H_

//
// Global prototypes
//
int      USB_InitWrite        (void);

#endif  // _USB_WR_H_

