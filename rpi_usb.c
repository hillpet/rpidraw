/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_usb.c
 *  Purpose:            USB support for Pico CncDraw
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from rpicnc
 *    29 May 2022:      Separate USB Rd/Wr threads
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <termios.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <common.h>
#include "config.h"
#include "globals.h"
//
#include "rpi_usb.h"

#define USE_PRINTF
#include <printx.h>

//
// Local prototypes
//
static int usb_InitRead    (char *, int);
static int usb_InitWrite   (char *);


/*------  Local functions separator ------------------------------------
_______________GLOBAL_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//
//  Function:  USB_CloseRead
//  Purpose:   Close USB devide for reading
//
//  Parms:     Device info struct
//  Returns:   True
//
bool USB_CloseRead(RPIUSB *pstUsb)
{
   if(pstUsb->fConnectedRd)
   {
      pstUsb->fConnectedRd = FALSE;
      close(pstUsb->iFdRd);
   }
   return(TRUE);
}

//
//  Function:  USB_CloseWrite
//  Purpose:   Close USB devide for writing
//
//  Parms:     Device info struct
//  Returns:   True
//
bool USB_CloseWrite(RPIUSB *pstUsb)
{
   if(pstUsb->fConnectedWr)
   {
      pstUsb->fConnectedWr = FALSE;
      close(pstUsb->iFdWr);
   }
   return(TRUE);
}

//
//  Function:  USB_ConnectedRead
//  Purpose:   Check if USB port is connected for reading
//  Parms:     USB struct
//
//  Returns:   TRUE if connected
//  Note:      
//
bool USB_ConnectedRead(RPIUSB *pstUsb)
{
   return(pstUsb->fConnectedRd);
}

//
//  Function:  USB_ConnectedWrite
//  Purpose:   Check if USB port is connected for writing
//  Parms:     USB struct
//
//  Returns:   TRUE if connected
//  Note:      
//
bool USB_ConnectedWrite(RPIUSB *pstUsb)
{
   return(pstUsb->fConnectedWr);
}

//
//  Function:  USB_CopyRecord
//  Purpose:   Get a single record from the circular buffer
//  Parms:     USB struct, Dest buffer, size
//
//  Returns:   Nr of chars in this record, 0 if no more data
//  Note:      +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//             |   | B | B | B | B | B | B | B |         | B |   |   |   |   
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//                   ^                                         ^
//                   iRcvGet                                   iRcvPut
//
int USB_CopyRecord(RPIUSB *pstUsb, char *pcDest, int iMax)
{
   bool  fMore=TRUE;
   int   iNr=0;
   char  cData;

   while(fMore)
   {
      if(pstUsb->iRcvGet == pstUsb->iRcvPut) 
      {
         fMore = FALSE;
      }
      else
      {
         cData = pstUsb->cRcv[pstUsb->iRcvGet++];
         pstUsb->iRcvGet &= CIR_RCV_MASK;
         switch(cData)
         {
            case CR:
               // Ignore
               break;

            case LF:
               *pcDest = 0;
               fMore   = FALSE;
               break;
      
            default:
               *pcDest++ = cData;
               if(iNr < iMax) iNr++;
               else
               {
                  LOG_Report(0, "USB", "USB-CopyRecord():ERROR:Overflow");
                  fMore = FALSE;
               }
               break;
         }
      }
   }
   return(iNr);   
}

//  
// Function:   USB_DumpData
// Purpose:    Dump USB data
// 
// Parameters: Usb struct ptr
// Returns:    
// Note:       
//  
void USB_DumpData(RPIUSB *pstUsb)
{
   LOG_Report  (0, "USB", "USB-DumpData():Dev#    : %d", pstUsb->iDevice);
   LOG_Report  (0, "USB", "USB-DumpData():Path    : %s", pstUsb->pcDevPath);
   LOG_ListData("USB-DumpData():RCV Buf",                pstUsb->cRcv, CIR_RCV_LENZ);
   LOG_Report  (0, "USB", "USB-DumpData():Fd      : %d", pstUsb->iFdRd);
   LOG_Report  (0, "USB", "USB-DumpData():iRcvGet : %d", pstUsb->iRcvGet);
   LOG_Report  (0, "USB", "USB-DumpData():iRcvPut : %d", pstUsb->iRcvPut);
   LOG_ListData("USB-DumpData():XMT Buf",                pstUsb->cXmt, CIR_XMT_LENZ);
   LOG_Report  (0, "USB", "USB-DumpData():Fd      : %d", pstUsb->iFdWr);
   LOG_Report  (0, "USB", "USB-DumpData():iXmtGet : %d", pstUsb->iXmtGet);
   LOG_Report  (0, "USB", "USB-DumpData():iXmtPut : %d", pstUsb->iXmtPut);
   LOG_Report  (0, "USB", "");
};

//
//  Function:  USB_Flush
//  Purpose:   Write data from circular buffer to USB device
//
//  Parms:     Device Info struct
//  Returns:   
//  Note:      Situation A: Put > Get
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//             |   | x | x | x | x | x | x | x |         | x | A | A | A |   
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//                   ^                                         ^          ^
//                   iGet                                      iPut       iMax
//
//             Situation B: Put < Get
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//             | x | B | B | B | B | B | B | B |         | B |   | x | x |   
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//                   ^                                             ^      ^
//                   iPut                                          iGet   iMax
//
void USB_Flush(RPIUSB *pstUsb)
{
   int   iNr, iWrite;
   int   iIdx = pstUsb->iXmtGet;

   if(pstUsb->fConnectedWr)
   {
      iWrite = USB_GetCircularCont(iIdx, pstUsb->iXmtPut, pstUsb->iXmtMax);
      if(iWrite)
      {
         iNr = write(pstUsb->iFdWr, &pstUsb->cXmt[iIdx], iWrite);
         if(iNr < 0) 
         {
            PRINTF("USB-Flush():Error: " CRLF, strerror(errno));
            USB_CloseWrite(pstUsb);
            return;
         }
         iIdx += iNr;
         iIdx &= pstUsb->iXmtMax;
         pstUsb->iXmtGet = iIdx;
         PRINTF("USB-Flush():Get/Put=%d, %d" CRLF, iIdx, pstUsb->iXmtPut);
      }
   }
}

//
//  Function:  USB_GetCircularFree
//  Purpose:   Calculate the free space in a circular buffer
//  Parms:     Get idx, Put idx, Max circular size
//
//  Returns:   Free space
//  Note:      Situation A: Put > Get
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//             |   |   |   |   |   |   |   |   |         | A | A | A | A |   
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//                   ^                                     ^              ^
//                   iGet                                  iPut           iMax
//
//             Situation B: Put < Get
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//             |   | B | B | B | B | B | B | B |         | B |   |   |   |   
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//                   ^                                             ^      ^
//                   iPut                                          iGet   iMax
//
int USB_GetCircularFree(int iGet, int iPut, int iMax)
{
   int   iFree=0;

   if(iPut >= iGet) 
   {  // A
      iFree = iMax + iGet - iPut - 1;
   }
   else
   {  // B
      iFree = iGet - iPut - 1;
   }
   return(iFree);
}

//
//  Function:  USB_GetCircularCont
//  Purpose:   Calculate the max contigeous space in a circular buffer
//  Parms:     Get idx, Put idx, Max circular size
//
//  Returns:   Max contigeous space
//  Note:      Situation A: Put > Get
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//             |   | x | x | x | x | x | x | x |         | x | A | A | A |   
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//                   ^                                         ^          ^
//                   iGet                                      iPut       iMax
//
//             Situation B: Put < Get
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//             | x | B | B | B | B | B | B | B |         | B |   | x | x |   
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//                   ^                                             ^      ^
//                   iPut                                          iGet   iMax
//
int USB_GetCircularCont(int iGet, int iPut, int iMax)
{
   int   iCont;

   if(iPut >= iGet) 
   {  // A
      iCont = iGet - iPut;
   }
   else
   {  // B
      iCont = iMax - iPut;
   }
   //PRINTF("USB-GetCircularCont():%d Chars" CRLF, iCont);
   return(iCont);
}

//
//  Function:  USB_GetCircularUsed
//  Purpose:   Calculate the used space in a circular buffer
//  Parms:     Get idx, Put idx, Max circular size
//
//  Returns:   Used space
//  Note:      Situation A: Put > Get
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//             |   | A | A | A | A | A | A | A |         | A |   |   |   |   
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//                   ^                                         ^          ^
//                   iGet                                      iPut       iMax
//
//             Situation B: Put < Get
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//             | B |   |   |   |   |   |   |   |         |   |   | B | B |   
//             +---+---+---+---+---+---+---+---+-.......-+---+---+---+---+
//                   ^                                             ^      ^
//                   iPut                                          iGet   iMax
//
int USB_GetCircularUsed(int iGet, int iPut, int iMax)
{
   int   iUsed=0;

   if(iPut >= iGet) 
   {  // A
      iUsed = iPut - iGet;
   }
   else
   {  // B
      iUsed = iMax - iGet + iPut;
   }
   return(iUsed);
}

//
//  Function:  USB_GetDeviceInfo
//  Purpose:   Get info struct of a USB device
//
//  Parms:     Device nr
//  Returns:   Device info struct
//
RPIUSB *USB_GetDeviceInfo(int iDevNr)
{
   RPIUSB  *pstUsb=NULL;

   switch(iDevNr)
   {
      case USB_DEV_DB:
         pstUsb = &(pstMap->G_stDeb);
         GEN_MEMSET(pstUsb, 0x00, sizeof(RPIUSB));
         //
         pstUsb->iDevice      = USB_DEV_DB;
         pstUsb->iReadTimeout = 100;
         pstUsb->iRcvMax      = CIR_RCV_LENZ;
         pstUsb->iXmtMax      = CIR_XMT_LENZ;
         pstUsb->pcDevPath    = GLOBAL_GetParameter(PAR_USB_DEV0);
         PRINTF("USB-GetDeviceInfo(): Usb-DEB: Setup USB serial port [%s]" CRLF, pstUsb->pcDevPath);
         break;

      case USB_DEV_WR:
         pstUsb = &(pstMap->G_stCmd);
         GEN_MEMSET(pstUsb, 0x00, sizeof(RPIUSB));
         //
         pstUsb->iDevice      = USB_DEV_WR;
         pstUsb->iReadTimeout = 100;
         pstUsb->iRcvMax      = CIR_RCV_LENZ;
         pstUsb->iXmtMax      = CIR_XMT_LENZ;
         pstUsb->pcDevPath    = GLOBAL_GetParameter(PAR_USB_DEV1);
         PRINTF("USB-GetDeviceInfo(): Usb-CNC: Setup USB serial port [%s]" CRLF, pstUsb->pcDevPath);
         break;

      default:
         LOG_Report(0, "USB", "USB-GetDeviceInfo():Unknown device nr %d", iDevNr);
         PRINTF("USB-GetDeviceInfo():Unknown device nr %d" CRLF, iDevNr);
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(pstUsb);
}

//
//  Function:  USB_OpenRead
//  Purpose:   Open USB device for reading
//
//  Parms:     Device info struct
//  Returns:   True if OKee 
//
bool USB_OpenRead(RPIUSB *pstUsb)
{
   bool  fCc=FALSE;

   pstUsb->iRcvGet = pstUsb->iRcvPut = 0;
   //
   if ( (pstUsb->iFdRd = usb_InitRead(pstUsb->pcDevPath, pstUsb->iReadTimeout)) == -1)
   {
      LOG_Report(errno, "USB", "USB-OpenRead(): Error-Unable to open USB serial port [%s]", pstUsb->pcDevPath);
      PRINTF("USB-OpenRead():Error-Unable to open USB serial port [%s]" CRLF, pstUsb->pcDevPath);
   }
   else 
   {
      PRINTF("USB-OpenRead():USB serial port [%s] OKee" CRLF, pstUsb->pcDevPath);
      fCc = TRUE;
   }
   pstUsb->fConnectedRd = fCc;
   return(fCc);
}

//
//  Function:  USB_OpenWrite
//  Purpose:   Open USB device for writing
//
//  Parms:     Device info struct
//  Returns:   True if OKee 
//
bool USB_OpenWrite(RPIUSB *pstUsb)
{
   bool  fCc=FALSE;

   pstUsb->iXmtGet = pstUsb->iXmtPut = 0;
   //
   if ( (pstUsb->iFdWr = usb_InitWrite(pstUsb->pcDevPath)) == -1)
   {
      LOG_Report(errno, "USB", "USB-OpenWrite(): Error-Unable to open USB serial port [%s]", pstUsb->pcDevPath);
      PRINTF("USB-OpenWrite():Error-Unable to open USB serial port [%s]" CRLF, pstUsb->pcDevPath);
   }
   else 
   {
      PRINTF("USB-OpenWrite():USB serial port [%s] OKee" CRLF, pstUsb->pcDevPath);
      fCc = TRUE;
   }
   pstUsb->fConnectedWr = fCc;
   return(fCc);
}

//
//  Function:  USB_Read
//  Purpose:   Read data from USB devide
//
//  Parms:     Device Info struct
//  Returns:   Number read, or -1 on error
//
int USB_Read(RPIUSB *pstUsb)
{
   int   iTot=0;
   char  cData;
   
   if(!pstUsb->fConnectedRd) return(-1);
   //
   do
   {
      //
      // Blocking read a single char until we detect EOL
      //
      if( read(pstUsb->iFdRd, &cData, 1) < 0)
      {
         PRINTF("USB-Read():Error: " CRLF, strerror(errno));
         USB_CloseRead(pstUsb);
         return(-1);
      }
      if(USB_GetCircularFree(pstUsb->iRcvGet, pstUsb->iRcvPut, pstUsb->iRcvMax) == 0) return(-1);
      pstUsb->cRcv[pstUsb->iRcvPut++] = cData;
      pstUsb->iRcvPut &= CIR_RCV_MASK;
      iTot++;
   }
   while(cData != LF);
   return(iTot);
}

//
//  Function:  USB_Write
//  Purpose:   Write data to USB devide
//
//  Parms:     Device Info struct
//  Returns:    0: All data put out to circular buffer
//             +x: Still to xmit
//             -1: Error
//
int USB_Write(RPIUSB *pstUsb)
{
   int   iWr=0, iNr=0;
   char *pcRecord = pstUsb->pcXmt;

   if(!pstUsb->fConnectedWr) return(-1);
   //
   if(pcRecord)
   {
      iWr = GEN_STRLEN(pcRecord);
      if(iWr)
      {
         iNr = write(pstUsb->iFdWr, pcRecord, iWr);
         if(iNr < 0) 
         {
            PRINTF("USB-Write():Error: " CRLF, strerror(errno));
            USB_CloseWrite(pstUsb);
            return(-1);
         }
         if(iNr == iWr) pstUsb->pcXmt = NULL;
         else           pstUsb->pcXmt = &pcRecord[iNr];
      }
      else pstUsb->pcXmt = NULL;
   }
   return(iWr - iNr);
}


/*------  Local functions separator ------------------------------------
________________LOCAL_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//  
// Function:   usb_InitRead
// Purpose:    Init the serial comms for reading
// 
// Parameters: Pathname, Read timeout in mSecs
// Returns:    Fd or -1 on error
// Note:       
//  
static int usb_InitRead(char *pcPath, int iTimeout)
{
   int            iFd;
   struct termios stOpt;

   //
   // OPEN THE UART
   // The flags (defined in fcntl.h):
   // Access modes (use 1 of these):
   //    O_RDONLY -  Open for reading only.
   //    O_RDWR   -  Open for reading and writing.
   //    O_WRONLY -  Open for writing only.
   //
   // O_NDELAY    
   // O_NONBLOCK  -  Enables nonblocking mode. When set read requests on the file can return immediately with a failure status
   //                if there is no input immediately available (instead of blocking). Likewise, write requests can also return
   //                immediately with a failure status if the output can't be written immediately.
   //
   // O_NOCTTY -     When set and path identifies a terminal device, open() shall not cause the terminal device to become the 
   //                controlling terminal for the process.
   //
   if( (iFd = open(pcPath, O_RDONLY)) == -1)
   {
      LOG_Report(errno, "COM", "usb-InitRead(): Error - Unable to open serial port.  Ensure it is not in use by another application");
      PRINTF("usb-InitRead(): Error - Unable to open serial port.  Ensure it is not in use by another application" CRLF);
      return(-1);
   }
   //
   // Configure the serial port:
   //
   // The flags (defined in /usr/include/termios.h:
   // Baud rate:  B1200, B2400, B4800, B9600, B19200, B38400, B57600, B115200, ...
   // CSIZE:      CS5, CS6, CS7, CS8
   // CLOCAL:     Ignore modem status lines
   // CREAD:      Enable receiver
   //
   // IGNPAR:     Ignore parity errors
   // PARENB:     Parity enable
   // PARODD:     Odd parity (else even)
   //
   // ICANON:     Canonical mode
   //
   GEN_MEMSET(&stOpt, 0x00, sizeof(struct termios));
   tcgetattr(iFd, &stOpt);
   //
   stOpt.c_cflag = B115200|CS8|CLOCAL|CREAD;
   stOpt.c_iflag = IGNPAR;
   stOpt.c_lflag = 0;                     // NON-Canonical
   //
   // Blocking/Non-blocking:
   //    
   //    MIN == 0, TIME == 0 (polling read)
   //       If data is available, read(2) returns immediately, with
   //       the lesser of the number of bytes available, or the number
   //       of bytes requested.  If no data is available, read(2)
   //       returns 0.
   //    
   //    MIN > 0, TIME == 0 (blocking read)
   //       read(2) blocks until MIN bytes are available, and returns
   //       up to the number of bytes requested.
   //    
   //    MIN == 0, TIME > 0 (read with timeout)
   //       TIME specifies the limit for a timer in tenths of a
   //       second.  The timer is started when read(2) is called.
   //       read(2) returns either when at least one byte of data is
   //       available, or when the timer expires.  If the timer
   //       expires without any input becoming available, read(2)
   //       returns 0.  If data is already available at the time of
   //       the call to read(2), the call behaves as though the data
   //       was received immediately after the call.
   //    
   //    MIN > 0, TIME > 0 (read with interbyte timeout)
   //       TIME specifies the limit for a timer in tenths of a
   //       second.  Once an initial byte of input becomes available,
   //       the timer is restarted after each further byte is
   //       received.  read(2) returns when any of the following
   //       conditions is met:
   //          * MIN bytes have been received.
   //          * The interbyte timer expires.
   //    
   //          * The number of bytes requested by read(2) has been
   //            received.  (POSIX does not specify this termination
   //            condition, and on some other implementations read(2)
   //            does not return in this case.)
   //    
   //       Because the timer is started only after the initial byte
   //       becomes available, at least one byte will be read.  If
   //       data is already available at the time of the call to
   //       read(2), the call behaves as though the data was received
   //       immediately after the call.
   //
   #ifdef FEATURE_COMM_NON_BLOCKING
   stOpt.c_cc[VMIN]  = 0;                 // Min # chars
   stOpt.c_cc[VTIME] = iTimeout/100;      // * 0.1 seconds read timeout
   #else    //FEATURE_COMM_NON_BLOCKING
   stOpt.c_cc[VMIN]  = 1;                 // Min # chars
   stOpt.c_cc[VTIME] = 0;                 // * 0.1 seconds read timeout
   #endif   //FEATURE_COMM_NON_BLOCKING

   tcflush(iFd, TCIFLUSH);
   tcsetattr(iFd, TCSANOW, &stOpt);
   //
   PRINTF("usb-InitRead(): Serial port at [%s] Open OKee" CRLF, pcPath);
   return(iFd);
}

//  
// Function:   usb_InitWrite
// Purpose:    Init the serial comms for writing
// 
// Parameters: Pathname
// Returns:    Fd or -1 on error
// Note:       
//  
static int usb_InitWrite(char *pcPath)
{
   int            iFd;
   struct termios stOpt;

   //
   // OPEN THE UART
   // The flags (defined in fcntl.h):
   // Access modes (use 1 of these):
   //    O_RDONLY -  Open for reading only.
   //    O_RDWR   -  Open for reading and writing.
   //    O_WRONLY -  Open for writing only.
   //
   // O_NDELAY    
   // O_NONBLOCK  -  Enables nonblocking mode. When set read requests on the file can return immediately with a failure status
   //                if there is no input immediately available (instead of blocking). Likewise, write requests can also return
   //                immediately with a failure status if the output can't be written immediately.
   //
   // O_NOCTTY -     When set and path identifies a terminal device, open() shall not cause the terminal device to become the 
   //                controlling terminal for the process.
   //
   if( (iFd = open(pcPath, O_WRONLY)) == -1)
   {
      LOG_Report(errno, "COM", "usb-InitWrite(): Error - Unable to open serial port.  Ensure it is not in use by another application");
      PRINTF("usb-InitWrite(): Error - Unable to open serial port.  Ensure it is not in use by another application" CRLF);
      return(-1);
   }
   //
   // Configure the serial port:
   //
   // The flags (defined in /usr/include/termios.h:
   // Baud rate:  B1200, B2400, B4800, B9600, B19200, B38400, B57600, B115200, ...
   // CSIZE:      CS5, CS6, CS7, CS8
   // CLOCAL:     Ignore modem status lines
   // CREAD:      Enable receiver
   //
   // IGNPAR:     Ignore parity errors
   // PARENB:     Parity enable
   // PARODD:     Odd parity (else even)
   //
   // ICANON:     Canonical mode
   //
   GEN_MEMSET(&stOpt, 0x00, sizeof(struct termios));
   tcgetattr(iFd, &stOpt);
   //
   stOpt.c_cflag = B115200|CS8|CLOCAL|CREAD;
   stOpt.c_iflag = IGNPAR;
   stOpt.c_lflag = 0;                     // NON-Canonical
   stOpt.c_cc[VMIN]  = 9;                 // Min # chars
   stOpt.c_cc[VTIME] = 0;                 // * 0.1 seconds read timeout

   tcflush(iFd, TCIFLUSH);
   tcsetattr(iFd, TCSANOW, &stOpt);
   //
   PRINTF("usb-InitWrite(): Serial port at [%s] Open OKee" CRLF, pcPath);
   return(iFd);
}


/*------  Local functions separator ------------------------------------
____________DISCARDED_FUNCTIONS(){};
------------------------------x----------------------------------------*/


#ifdef COMMENT


#endif