/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           globals.h
 *  Purpose:            Define the mmap variables, common over all threads. If the structure 
 *                      needs to change, roll the revision number to make sure a new mmap 
 *                      file is created !
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <semaphore.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <netinet/in.h>

#ifdef DEFINE_GLOBALS
   #define EXTERN
#else
   #define EXTERN extern
#endif
//
// We MUST define these sizes here as well (if no pre-processor)
//
#define  SIZEOF_INT           4
#define  SIZEOF_DOUBLE        8
//
//
// HTTP Functions (callback through HTML or JSON)
//
#define  PAR____                          0x00000000     // No command
#define  PAR_PIX                          0x00000001     // List of pictures
#define  PAR_EML                          0x00000002     // List of Mailx
#define  PAR_PRM                          0x00000004     // List of Generic Parameters
#define  PAR_CNC                          0x00000008     // List of Cnc     Parameters
#define  PAR_ERR                          0x00008000     // Error replies
#define  PAR_ALL                          0x0000FFFF     // All HTTP functions
//
// JSON markers for text-style-vars ("value" = "123456")
//             or number-style-vars ("value" =  123456 )
//
#define  JSN_TXT                          0x10000000     // JSON Variable is text
#define  JSN_INT                          0x20000000     // JSON Variable is integer
#define  WB                               0x80000000     // Warm boot var
#define  PAR_A                            0x08000000     // Parm is ASCII
#define  PAR_B                            0x04000000     // Parm is BCD
#define  PAR_F                            0x02000000     // Parm is FLOAT
#define  PAR_H                            0x01000000     // Parm is HEX
#define  PAR_TYPE_MASK                    0x0F000000     // Parm is HEX
//
#define  ALWAYS_CHANGED                   -1
#define  NEVER_CHANGED                    -2
//
#define  ONEMEGABYTE                      (1024*1024)
#define  ONEGIGABYTE                      (1024*ONEMEGABYTE)
#define  ONEMILLION                       1000000L
#define  ONEBILLION                       1000000000L
//
#define  MAX_CMD_LEN                      256
#define  MAX_CMDLINE_LEN                  (MAX_CMD_LEN + MAX_ARG_LEN)
//
#define  MAX_NUM_REPORT_LINES             10000
#define  MAX_DYN_PAGES                    64
//
// Global area array sizes
//
#define  MAX_PATH_LEN                     255
#define  MAX_PATH_LENZ                    MAX_PATH_LEN+1
#define  MAX_URL_LEN                      31
#define  MAX_URL_LENZ                     MAX_URL_LEN+1
#define  MAX_MAC_LEN                      31
#define  MAX_MAC_LENZ                     MAX_MAC_LEN+1
#define  MAX_ADDR_LEN                     INET_ADDRSTRLEN
#define  MAX_ADDR_LENZ                    INET_ADDRSTRLEN+1
#define  MAX_PARM_LEN                     15
#define  MAX_PARM_LENZ                    MAX_PARM_LEN+1
#define  MAX_ARG_LEN                      255
#define  MAX_ARG_LENZ                     MAX_ARG_LEN+1
//
// Global notification flags
// Activations:
//
#define  GLOBAL_GRD_INI       0x01000000     // Ini: Guard
#define  GLOBAL_SVR_INI       0x02000000     // Ini: HTTP Server
#define  GLOBAL_UWR_INI       0x04000000     // Ini: USB Write
#define  GLOBAL_UDB_INI       0x08000000     // Ini: USB Debug
#define  GLOBAL_UDP_INI       0x10000000     // Ini: UDP faemon
#define  GLOBAL_CNC_INI       0x20000000     // Ini: CNC daemon
#define  GLOBAL_XXX_INI       0xFF000000     // Init Mask
//
// These flags are thread specific
// Dest HOST:
#define  GLOBAL_ALL_HST_FLT   0x00000001     // Segmentation fault host nfy (exit)
#define  GLOBAL_ALL_HST_END   0x00000002     // Host Exit
#define  GLOBAL_ALL_HST_OFF   0x00000004     // Power Off button
#define  GLOBAL_ALL_HST_RST   0x00000008     // Restart theApp
#define  GLOBAL_ALL_HST_RBT   0x00000010     // Reboot request
#define  GLOBAL_GRD_HST_NFY   0x00000020     // Guard Nfy
#define  GLOBAL_XXX_HST_MSK   0x0000FFFF     // Mask
// Dest GUARD:
#define  GLOBAL_HST_GRD_RUN   0x00000001     // Run
#define  GLOBAL_XXX_GRD_MSK   0x0000FFFF     // Mask
// Dest SRVR:
#define  GLOBAL_XXX_SVR_RUN   0x00000001     // 
// Dest UDP:
#define  GLOBAL_SVR_UDP_RUN   0x00000001     // Server has connection
// Dest CNC:
#define  GLOBAL_HST_CNC_INI   0x00000001     // Run init Gcode
#define  GLOBAL_CMD_CNC_REC   0x00000002     // Incoming USB CNC data
#define  GLOBAL_DEB_CNC_REC   0x00000004     // Incoming USB DEB data
#define  GLOBAL_CMD_CNC_CNX   0x00000008     // USB CMD data connection status change
#define  GLOBAL_DEB_CNC_CNX   0x00000010     // USB DEB data connection status change
#define  GLOBAL_SVR_CNC_RUN   0x00000020     // Run Server Gcode from G_pcGcode
// Dest ALL:
#define  GLOBAL_GRD_ALL_RUN   0x00010000     // Guard Run
#define  GLOBAL_HST_ALL_MID   0x00020000     // Midnight signal from Host
#define  GLOBAL_SVR_ALL_NFY   0x00040000     // Http Server notify to threads
#define  GLOBAL_HST_ALL_RUN   0x00020000     // Run/Execute command from Host
#define  GLOBAL_XXX_ALL_MSK   0x00FF0000     // Mask
//
typedef enum _global_parameters_
{
#define  EXTRACT_PAR(a,b,c,d,e,f,g,h,i,j,k)    a,
#include "par_defs.h"
#undef EXTRACT_PAR
   NUM_GLOBAL_DEFS
}  GLOPAR;
//
// Generic status strings/enums
//
typedef enum _genstat_
{
#define  EXTRACT_ST(a,b)   a,
#include "gen_stats.h"
#undef   EXTRACT_ST
   //
   NUM_GEN_STATUS,
   GEN_STATUS_ASK                         // Return current status
}  GENSTAT;
//
typedef struct _rpidata_
{
   char    *pcObject;
   FTYPE    tType;
   int      iIdx;
   int      iSize;
   int      iFree;
   int      iLastComma;
}  RPIDATA;
//
//
// Enums PIDs
//
typedef enum _proc_pids_
{
   #define  EXTRACT_PID(a,b,c)   a,
   #include "par_pids.h"
   #undef EXTRACT_PID
   //
   NUM_PIDT
}  PIDT;
//
// CNC states
//
#define  EXTRACT_STH(a,b,c,d,e) a,
typedef enum _cnc_sth_
{
   #include "cncstates.h"
   NUM_CNC_STATES
}  CNCSTH;
#undef   EXTRACT_STH
//
typedef struct _proc_pidlist_
{
   pid_t       tPid;
   sem_t      *ptSem;
   int         iFlag;
   int         iNotify;
   const char *pcName;
   const char *pcHelp;
}  PIDL;
//
typedef struct _globals_
{
   int         iFunction;
   const char *pcOption;
   const char *pcDefault;
   int         iChanged;
   int         iChangedOffset;
   int         iValueOffset;
   int         iValueSize;
}  GLOBALS;
//
typedef enum _varreset_
{
   VAR_COLD    = 0,                       // Cold reset
   VAR_WARM,                              // Warm reset
   VAR_UPDATE,                            // Update

   NUM_VARRESETS
}  VARRESET;
//
typedef enum _rpi_mapstate_
{
   MAP_CLEAR    = 0,                      // Clear all the mapped memory
   MAP_SYNCHR,                            // Synchronize the map
   MAP_RESTART,                           // Restart the counters using the last stored data

   NUM_MAPSTATES
}  MAPSTATE;
//
// CNC Statuc from PICO
//
typedef enum _cnc_status_
{
   CNC_ST_INIT = 0,                    // Init
   CNC_ST_STOP,                        // All Axis have stopped
   CNC_ST_EXEC,                        // Axis moving
   CNC_ST_OKEE,                        // Command ack
   CNC_ST_NACK                         // Command nack
}  CNCST;
//
// Dynamic page registered URLs
//
typedef bool(*PFVOIDPI)(void *, int);
typedef struct _dynr_
{
   int         tUrl;                      // DYN_HTML_... (from EXTRACT_DYN pages.h, ...)
   FTYPE       tType;                     // HTTP_JSON, HTTP_HTML
   int         ePid;                      // Owner pid enum PID_xxxx
   int         iTimeout;                  // Estimated call duration 
   int         iPort;                     // Port
   char        pcUrl[MAX_URL_LENZ];       // URL
   PFVOIDPI    pfCallback;                // Callback function
}  DYNR;
//
// Global notification struct
//
typedef struct NFY
{
   PIDT     tPid;                         // Pid
   int      iFlag;                        // Notification
   bool   (*pfCb)(void *, CNCSTH);        // Callback
   CNCSTH   tState;                       // STH State
}  NFY;
//
// The USB Comms stucture
//
#define USB_DEV_DB      0                 // USB Debug device 
#define USB_DEV_WR      1                 // USB Write device 
//
#define NUM_AXIS        3                 // X, Y and Z axis
//
#define CIR_RCV_MASK    2047              // Circular Rcv buffer mask
#define CIR_RCV_LENZ    CIR_RCV_MASK+1    // Circular Rcv buffer 
#define CIR_XMT_MASK    1023              // Circular Xmt buffer mask
#define CIR_XMT_LENZ    CIR_XMT_MASK+1    // Circular Xmt buffer 
#define CMD_BUF_LEN     255               // USB Command xfer buffer size
#define CMD_BUF_LENZ    CMD_BUF_LEN+1     // 
//
typedef struct RPIUSB
{
   bool     fConnectedRd;                 // USB is connected
   bool     fConnectedWr;                 // USB is connected
   int      iDevice;                      // USB Device number
   int      iFdRd, iFdWr;                 // USB Device File descriptors Rd/Wr
   int      iReadTimeout;                 // Read timeout in mSecs 
   int      iRcvGet, iRcvPut, iRcvMax;    // Circular Rcv buffer Get/Put/Max
   int      iXmtGet, iXmtPut, iXmtMax;    // Circular Xmt buffer Get/Put/Max
   char    *pcXmt;                        // USB Xmit ptr
   char    *pcDevPath;                    // USB Device Pathname
   char     cRcv[CIR_RCV_LENZ];           // USB PICO Circular Rcv buffer
   char     cXmt[CIR_XMT_LENZ];           // USB PICO Circular Xmt buffer
}  RPIUSB;
//
// Communication structure RPi UDP --> QT5 theApp
//
typedef struct _rpi_comms_
{
   u_int8   ubStatus;
   u_int8   ubCommand;                    // Current CNC command
   long     lLineNr;                      //             line number
   //
   char     pcCmd[CMD_BUF_LENZ];          // USB command xfer  buffer
   char     pcRep[CMD_BUF_LENZ];          // USB command Reply buffer
}  RPICMD;
//
// File Xfer data
//
typedef struct _rpi_xfer_
{
   int      iFd;                          // Fd
   int      iNr;                          // Nr in buffer
   int      iMax;                         // Buffer Max size
   RPIUSB  *pstUsb;                       // USB port
   char     pcBuffer[MAX_ARG_LENZ];       // USB command xfer  buffer
}  RPIXFR;

//
// Main CNC parameter struct
//
typedef struct _rpicnc_
{
   int      iSeq;                         // Sequence number
   int      iCncMotion;                   // CNC motion status
   int      iCncDelay;                    // CNC motion delay
   int      iCncPulse;                    // CNC stepper pulse delay
   int      iDrawMode;                    // CNC Pattern/drawing mode
   int      iMoveSteps;                   // CNC Pattern/moving steps
   int      iDrawSteps;                   // CNC Pattern/drawing steps
   // Settings
   double   flCanvW;                      // Total canvas  size  (mm)
   double   flCanvH;                      //
   double   flDrawW;                      // Total drawing size  (mm)
   double   flDrawH;                      //
   double   flSrev;                       // Steps per mm 
   double   flRad;                        // Radius of cogwheel   (mm)
   // Variables
   double   flXc, flYc, flZc;             // Current coords       (mm)
   double   flXn, flYn, flZn;             // New     coords       (mm)
   double   flXs, flYs, flZs;             // Corner lo left       (mm)
   double   flXe, flYe, flZe;             // Corner Hi right      (mm)
   double   flResXm;                      // Movement res X       (mm)
   double   flResYm;                      // Movement res Y       (mm)
   double   flResZm;                      // Movement res Z       (mm)
   double   flResXd;                      // Drawing  res X       (mm)
   double   flResYd;                      // Drawing  res Y       (mm)
   double   flResZd;                      // Drawing  res Z       (mm)
   double   flVar1, flVar2;               // Misc variables
   //
   double   flAccSet    [NUM_AXIS];       // steppers Acceleration
   double   flSpdMin    [NUM_AXIS];       // steppers Minimum speed
   double   flSpdMax    [NUM_AXIS];       // steppers Maximum speed
   long     lCurPos     [NUM_AXIS];       // steppers Current position
   long     lNewPos     [NUM_AXIS];       // Steppers New position
}  RPICNC;
//
typedef struct CNCVAR
{
   CNCST    tStatus;                      // CNC status
   int      iCncAxis;                     // CNC Axis moving 0000.0000.0000.0ZYX
   int      iCncFree;                     // CNC 
   int      iCncLine;                     // CNC 
   int      iCncXpos;                     // CNC 
   int      iCncYpos;                     // CNC 
   int      iCncZpos;                     // CNC 
}  CNCVAR;

//=============================================================================
// Global parameters mapped to a shared MMAP file 
//=============================================================================
typedef struct _rpimap_
{
   int               G_iSignature1;
   int               G_iVersionMajor;
   int               G_iVersionMinor;
   bool              G_fInit;
   u_int32           G_ulStartTimestamp;
   pthread_mutex_t   G_tMutex;
   //
   //=============== Start WB RESET ZERO area =================================
   int               G_iResetStart;
   //
   int64             G_llLogFilePos;
   u_int32           G_ulDebugMask;
   u_int32           G_ulSecondsCounter;
   //
   int               G_iMallocs;
   int               G_iSegFaultLine;
   int               G_iSegFaultPid;
   int               G_iGuards;
   int               G_iTraceCode;
   //
   bool              G_Connected;
   //
   char              G_pcCommand          [MAX_PARM_LENZ];
   char              G_pcStatus           [MAX_PARM_LENZ];
   char              G_pcDelete           [MAX_PARM_LENZ];
   char              G_pcSegFaultFile     [MAX_PATH_LENZ];
   //
   GLOG              G_stLog;
   //
   // PID list (Auto installed)
   // Misc semaphores
   //
   PIDL              G_stPidList          [NUM_PIDT];
   sem_t             G_tSemList           [NUM_PIDT];
   //
   // Dynamic HTTP page list
   //
   char              G_pcHostIp           [MAX_ADDR_LENZ];
   int               G_iHttpStatus;
   int               G_iHttpArgs;
   int               G_iNumDynPages;
   int               G_iCurDynPage;
   DYNR              G_stDynPages         [MAX_DYN_PAGES];
   //
   // User settings
   //
   char              G_pcCncDelay         [MAX_PARM_LENZ];
   char              G_pcCncPulse         [MAX_PARM_LENZ];
   char              G_pcDrawMode         [MAX_PARM_LENZ];
   char              G_pcCurPos[NUM_AXIS] [MAX_PARM_LENZ];
   char              G_pcNewPos[NUM_AXIS] [MAX_PARM_LENZ];
   char              G_pcStaPos[NUM_AXIS] [MAX_PARM_LENZ];
   char              G_pcEndPos[NUM_AXIS] [MAX_PARM_LENZ];
   char              G_pcAccSet[NUM_AXIS] [MAX_PARM_LENZ];
   char              G_pcSpdMin[NUM_AXIS] [MAX_PARM_LENZ];
   char              G_pcSpdMax[NUM_AXIS] [MAX_PARM_LENZ];
   //
   char              G_pcCanvasWidth      [MAX_PARM_LENZ];
   char              G_pcCanvasHeight     [MAX_PARM_LENZ];
   char              G_pcDrawWidth        [MAX_PARM_LENZ];
   char              G_pcDrawHeight       [MAX_PARM_LENZ];
   char              G_pcRadius           [MAX_PARM_LENZ];
   char              G_pcStepRevs         [MAX_PARM_LENZ];
   char              G_pcMoveSteps        [MAX_PARM_LENZ];
   char              G_pcDrawSteps        [MAX_PARM_LENZ];
   //
   char              G_pcNumFiles         [MAX_PARM_LENZ];
   char              G_pcFilter           [MAX_PARM_LENZ];
   char              G_pcPixWidth         [MAX_PARM_LENZ];
   char              G_pcPixHeight        [MAX_PARM_LENZ];
   char              G_pcPixFileExt       [MAX_PARM_LENZ];
   char              G_pcDetectPixel      [MAX_PARM_LENZ];
   char              G_pcDetectLevel      [MAX_PARM_LENZ];
   char              G_pcDetectSeq        [MAX_PARM_LENZ];
   char              G_pcDetectRed        [MAX_PARM_LENZ];
   char              G_pcDetectGreen      [MAX_PARM_LENZ];
   char              G_pcDetectBlue       [MAX_PARM_LENZ];
   char              G_pcDetectZoomX      [MAX_PARM_LENZ];
   char              G_pcDetectZoomY      [MAX_PARM_LENZ];
   char              G_pcDetectZoomW      [MAX_PARM_LENZ];
   char              G_pcDetectZoomH      [MAX_PARM_LENZ];
   char              G_pcDetectAvg1       [MAX_PARM_LENZ];
   char              G_pcDetectAvg2       [MAX_PARM_LENZ];
   //
   RPIUSB            G_stDeb;
   RPIUSB            G_stCmd;
   //
   RPICNC            G_stRpiCnc;
   CNCVAR            G_stCncVar;
   RPICMD            G_stRpiCmd;
   //
   //
   // Changed markers
   //
   u_int8            G_ubCommandChanged;

   //--------------------------------------------------------------------------
   // Take additional power-on reset parms from this pool:
   //--------------------------------------------------------------------------
   #define SPARE_POOL_ZERO 1000
   #define EXTRA_POOL_ZERO 0
   //
   #if     EXTRA_POOL_ZERO > SPARE_POOL_ZERO
   #error  Spare zero-init pool too small
   #endif
   //
   // Extra from zero-init pool
   //
   char              G_pcSparePoolZero    [SPARE_POOL_ZERO-EXTRA_POOL_ZERO];
   //
   //--------------------------------------------------------------------------
   int               G_iResetEnd;
   //=============== End WB reset area ========================================


   //=============== Start Persistent storage==================================
   char              G_pcHostname         [MAX_URL_LENZ];
   char              G_pcVersionSw        [MAX_PARM_LENZ];
   char              G_pcWwwDir           [MAX_PATH_LENZ];
   //
   char              G_pcLogFile          [MAX_PATH_LENZ];
   char              G_pcDebugMask        [MAX_PARM_LENZ];
   char              G_pcLastFile         [MAX_PATH_LENZ];
   char              G_pcLastFileSize     [MAX_PARM_LENZ];
   char              G_pcDefault          [MAX_PATH_LENZ];
   char              G_pcGcode            [MAX_PATH_LENZ];
   //
   int               G_iGcode;
   int               G_iHttpPort;
   int               G_iUdpPort;
   int               G_iVerbose;
   //
   char              G_pcUsbDev0          [MAX_PATH_LENZ];    
   char              G_pcUsbDev1          [MAX_PATH_LENZ];    
   //
   //--------------------------------------------------------------------------
   // Take additional persistent parms from this pool:
   //--------------------------------------------------------------------------
   // Add:  
   //
   #define SPARE_POOL_PERS 1000
   #define EXTRA_POOL_PERS 0
   //
   #if     EXTRA_POOL_PERS > SPARE_POOL_PERS
   #error  Spare persistent pool too small
   #endif
   //
   // Extra from non-volatile pool
   //
   char              G_pcSparePool2          [SPARE_POOL_PERS-EXTRA_POOL_PERS];
   //
   //=============== End Persistent storage ===================================
   //
   //--------------------------------------------------------------------------
   int               G_iSignature2;          // Signature bottom
}  RPIMAP;

//=============================================================================
// GLOBAL MMAP structure pointer
//=============================================================================
EXTERN RPIMAP *pstMap;

//
// Global functions
//
bool           GLOBAL_CheckDelete            (char *);
bool           GLOBAL_Close                  (bool);
u_int32        GLOBAL_ConvertDebugMask       (bool);
int            GLOBAL_ExecuteNotifications   (const NFY *, int, void *);
void           GLOBAL_ExpandMap              (int, int);
int            GLOBAL_FlushLog               (void);
bool           GLOBAL_Init                   (void);
u_int32        GLOBAL_GetDebugMask           (void);
char          *GLOBAL_GetHostName            (void);
GLOG          *GLOBAL_GetLog                 (void);
int            GLOBAL_GetMallocs             (void);
const GLOBALS *GLOBAL_GetParameters          (void);
void          *GLOBAL_GetParameter           (GLOPAR);
int            GLOBAL_GetParameterSize       (GLOPAR);
int            GLOBAL_GetParameterType       (GLOPAR);
int            GLOBAL_GetSignal              (int);
bool           GLOBAL_GetSignalNotification  (int, int);
int            GLOBAL_GetTraceCode           (void);
int            GLOBAL_HostNotification       (int);
int            GLOBAL_Lock                   (void);
int            GLOBAL_Notify                 (int, int, int);
u_int32        GLOBAL_ReadSecs               (void);
void           GLOBAL_SegmentationFault      (const char *, int);
void           GLOBAL_SetDebugMask           (u_int32);
int            GLOBAL_SetSignalNotification  (int, int);
bool           GLOBAL_Share                  (void);
int            GLOBAL_Status                 (int);
bool           GLOBAL_Sync                   (void);
int            GLOBAL_Unlock                 (void);
bool           GLOBAL_UpdateParameter        (GLOPAR, char *);
void           GLOBAL_UpdateSecs             (void);
//
// Global PID functions
//
bool           GLOBAL_PidCheckGuards         (void);
bool           GLOBAL_PidGetGuard            (PIDT);
bool           GLOBAL_PidSetGuard            (PIDT);
void           GLOBAL_PidClearGuards         (void);
void           GLOBAL_PidSaveGuard           (PIDT, int);
pid_t          GLOBAL_PidGet                 (PIDT);
const char    *GLOBAL_PidGetName             (PIDT);
const char    *GLOBAL_PidGetHelp             (PIDT);
bool           GLOBAL_PidPut                 (PIDT, pid_t);
int            GLOBAL_PidsLog                (void);
int            GLOBAL_PidsTerminate          (int);
void           GLOBAL_PutMallocs             (int);
void           GLOBAL_Signal                 (PIDT, int);
void           GLOBAL_SignalPid              (pid_t, int);
int            GLOBAL_SignalWait             (int, int, int);
//
bool           GLOBAL_SemaphoreDelete        (int);
bool           GLOBAL_SemaphoreInit          (int);
bool           GLOBAL_SemaphorePost          (int);
int            GLOBAL_SemaphoreWait          (int, int);

#endif /* _GLOBALS_H_ */
