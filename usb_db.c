/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           usb_db.c
 *  Purpose:            USB Debug functions
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from rpicnc
 *    29 May 2022:      Separate USB Rd/Wr threads
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <errno.h>
#include <sys/select.h>
#include <sys/signal.h>
#include <sys/types.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_func.h"
#include "rpi_usb.h"
#include "usb_db.h"

//#define USE_PRINTF
#include "printf.h"

#ifdef FEATURE_USB_DEBUG_TRACES
//
// Local prototypes
//
static int     usb_ExecuteDebug        (void);
//
static void    usb_ReceiveSignalUser1  (int);
static void    usb_ReceiveSignalUser2  (int);
static void    usb_ReceiveSignalInt    (int);
static void    usb_ReceiveSignalTerm   (int);
static bool    usb_SignalRegister      (sigset_t *);
//
// Global data
//
static bool    fThreadRunning = TRUE;
//
// PICO debug commands:
//======================================================================
//    'A' ... 'Z' : Turn on  G_Verbose filters 
//    'a' ... 'z' : Turn off G_Verbose filters 
//
//    ' '         : Report CNC status (Text)
//    '['         : Start Xfer mode: copy incoming to command buffer
//    ']'         : End Xfer mode
//    '>'         : Enable  contineous status updates
//    '<'         : Disable contineous status updates
//    '='         : Reset Global vars
//    '-'         : Reset Verbose flags
//    '?'         : Run test Gcode
//    '!'         : Report buffer status (Record)
//    '$'         : Report CNC    status (Record)
//
//======================================================================
//
// At USB connect:
//    '-'   : Reset Verbose flags
//    '>'   : Enable  contineous status updates to USB_Command
//    '!'   : Report buffer status to USB-Debug :
//                "SECact= 215\n"
//                "CNCmin= 1\n"
//                "CNCact= 744\n"
//                "CNCmax= 1062\n"
//                "RCVget= 0\n"
//                "RCVput= 0\n"
//                "RCVmax= 1024\n"
//                "CMDget= 0\n"
//                "CMDput= 0\n"
//                "CMDmax= 256\n"
//    '$'   : Report CNC status to USB-Debug :
//                "SECact= 215\n"
//                "RUNact= 0, 0, 0\n"
//                "POScur= 1200, 800, 0\n"
//                "POSnew= 1200, 800, 50\n"
//                "STPexa= 100, 100, 100\n"
//                "SPDact= 0.000, 0.000, 0.000\n"
//                "SPDlin= 400.000, 400.000, 400.000\n"
//                "SPDset= 50.000, 40.000, 0.000\n"
//                "SPDmax= 50.000, 40.000, 0.000\n"
//                "SCLset= 0.000, 0.000, 0.000\n"
//                "ACCset= 200.000, 200.000, 200.000\n"
//

/*------  Local functions separator ------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:  USB_InitDebug
//  Purpose:   Init Debug communicating with the USB port
//
//  Parms:     
//  Returns:   Completion code
//
int USB_InitDebug()
{
   int      iCc;
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         GLOBAL_PidPut(PID_USBDB, tPid);
         break;

      case 0:
         // Child:
         GEN_Sleep(500);
         iCc = usb_ExecuteDebug();
         LOG_Report(0, "UDB", "USB-InitDebug():Exit normally, cc=%d", iCc);
         exit(iCc);
         break;

      case -1:
         // Error
         printf("USB-InitDebug(): Error!" CRLF);
         LOG_Report(errno, "UDB", "USB-InitDebug():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(GLOBAL_UDB_INI);
}


/*------  Local functions separator ------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:  usb_ExecuteDebug
//  Purpose:   
//
//  Parms:     
//  Returns:   Cc
//
static int usb_ExecuteDebug()
{
   int        iReported=0;
   int         iOpt, iNr, iCc=EXIT_CC_GEN_ERROR;
   RPIUSB     *pstUsb;
   sigset_t    tBlockset;

   if(usb_SignalRegister(&tBlockset) == FALSE) return(EXIT_CC_GEN_ERROR);
   //
   pstUsb = USB_GetDeviceInfo(USB_DEV_DB);
   GLOBAL_SemaphoreInit(PID_USBDB);
   //
   // Init ready
   //
   GLOBAL_PidSaveGuard(PID_USBDB, GLOBAL_UDB_INI);
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_UDB_INI);
   //
   // Init ready
   // Wait for Host Run/Execute
   //
   while(fThreadRunning)
   {
      iOpt = GLOBAL_SignalWait(PID_USBDB, GLOBAL_HST_ALL_RUN, 1000);
      if(iOpt == 0) break;
   }
   PRINTF("usb-ExecuteDebug():Init OKee: [%s]" CRLF, pstUsb->pcDevPath);
   PRINTF("usb-ExecuteDebug():Running" CRLF);
   //
   // Run/Execute
   //
   while(fThreadRunning)
   {
      if(USB_ConnectedRead(pstUsb))
      {
         //
         //====================================================================
         // CNC Debug Deamon RUN loop :
         //====================================================================
         iOpt = GLOBAL_SemaphoreWait(PID_USBDB, 50);
         switch(iOpt)
         {
            case 0:
               //
               // We return here after the reception of :
               //    o SIGUSR1
               //    o SIGUSR2
               //    o SIGINT 
               //    o SIGTERM
               //    
               if(GLOBAL_GetSignalNotification(PID_USBDB, GLOBAL_HST_ALL_MID)) 
               {
                  // Midnight
                  PRINTF("usb-ExecuteDebug():Midnight" CRLF);
                  LOG_Report(0, "UDB", "usb-ExecuteDebug():Midnight");
               }
               break;

            case 1:
               // Timeout: 
               // Blocking Read CNC Debug USB serial port data until EOL
               //
               iNr = USB_Read(pstUsb);
               if(iNr > 0)
               {
                  if(RPI_GetFlag(FLAGS_LOG_CNC)) LOG_ListData("usb-ExecuteDebug()", pstUsb->cRcv, CIR_RCV_LENZ);
                  if(RPI_GetFlag(FLAGS_PRN_CNC)) LOG_DumpData("usb-ExecuteDebug()", pstUsb->cRcv, CIR_RCV_LENZ);
                  //
                  // USB CNC Debug port has incoming data stored in the circular RCV buffer
                  //    pstMap->G_stDeb.cRcv        : All received data
                  //    pstMap->G_stDeb.iRcvGet/Put : Get/Put index into the circular buffer
                  //
                  // Somebody MUST get these out before the buffer is full!
                  //
                  GLOBAL_SetSignalNotification(PID_CNC, GLOBAL_DEB_CNC_REC);
               }
               else if(iNr < 0) 
               {
                  PRINTF("cnc-ExecuteDebug():ERROR from USB" CRLF);
               }
               else PRINTF("cnc-ExecuteDebug():No data from USB" CRLF);
               break;

            default:
            case -1:
               // Error
               LOG_Report(errno, "UDB", "usb-ExecuteDebug():ERROR GLOBAL_SemaphoreWait");
               PRINTF("usb-ExecuteDebug():ERROR GLOBAL_SemaphoreWait(errno=%d)" CRLF, errno);
               break;
         }
      }
      else // NOT Connected
      {
         if(iReported > 0) 
         {
            //Only Once
            iReported = -1;
            USB_CloseRead(pstUsb);
            PRINTF("usb-ExecuteDebug():Disconnected:Retry open...." CRLF);
         }
         if( USB_OpenRead(pstUsb) )
         {
            //Only Once
            iReported = +1;
            LOG_Report(0, "UDB", "usb-ExecuteDebug():USB Device Connected!");
            PRINTF("usb-ExecuteDebug():USB Device Connected!" CRLF);
            GLOBAL_SetSignalNotification(PID_CNC, GLOBAL_DEB_CNC_CNX);
         }
         else // USB Port (still) not connected: wait till it is
         {
            if(iReported >= 0)
            {
               //Only Once
               iReported = -1;
               LOG_Report(0, "UDB", "usb-ExecuteDebug():USB Device NOT connected!");
               PRINTF("usb-ExecuteDebug():USB Device NOT connected!" CRLF);
               GLOBAL_SetSignalNotification(PID_CNC, GLOBAL_DEB_CNC_CNX);
            }
            GEN_Sleep(USB_CNX_TIMEOUT);
         }
      }
   }
   //
   // Thread exit
   //
   PRINTF("usb-ExecuteDebug():Exit" CRLF);
   USB_CloseRead(pstUsb);
   GLOBAL_SemaphoreDelete(PID_USBDB);
   return(iCc);
}

/*------  Local functions separator ------------------------------------
_______________SIGNAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:  usb_ReceiveSignalUser1
//  Purpose:   SIGUSR1 signal
//
//  Parms:
//  Returns:    
//  Note:      SIGUSR1 to accept a new GCode sequence
//
static void usb_ReceiveSignalUser1(int iSignal)
{
   LOG_Report(0, "UDB", "usb-ReceiveSignalUser1()");
}

//
//  Function:  usb_ReceiveSignalUser2
//  Purpose:   SIGUSR2 signal
//
//  Parms:
//  Returns:    
//  Note:      SIGUSR2 
//
static void usb_ReceiveSignalUser2(int iSignal)
{
   LOG_Report(0, "UDB", "usb-ReceiveSignalUser2()");
}

//
//  Function:  usb_ReceiveSignalInt
//  Purpose:   SIGINT signal
//
//  Parms:
//  Returns:    
//  Note:      SIGINT to exit
//
static void usb_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "UDB", "usb-ReceiveSignalInt()");
   PRINTF ("usb-ReceiveSignalInt()" CRLF);
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_USBDB);
}

//
//  Function:  usb_ReceiveSignalTerm
//  Purpose:   SIGTERM signal
//
//  Parms:
//  Returns:    
//  Note:      SIGTERM: Stops ALL CNC motion
//
static void usb_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "UDB", "usb-ReceiveSignalTerm()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_USBDB);
}

//
//  Function:  usb_SignalRegister
//  Purpose:   Register all SIGxxx
//
//  Parms:     Blockset
//  Returns:   TRUE if delivered
//
static bool usb_SignalRegister(sigset_t *ptBlockset)
{
  bool fCc = TRUE;

  //
  // SIGUSR1 is used to talk between parent and child 
  //
  if( signal(SIGUSR1, &usb_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "UDB", "usb-SignalRegister(): SIGUSR1 ERROR");
     fCc = FALSE;
  }
  //
  // SIGUSR2 is not used
  //
  if( signal(SIGUSR2, &usb_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "UDB", "usb-SignalRegister(): SIGUSR2 ERROR");
     fCc = FALSE;
  }
  //
  // SIGINT  is used to exit this thread
  //
  if( signal(SIGINT, &usb_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "UDB", "usb-SignalRegister(): SIGINT ERROR");
     fCc = FALSE;
  }
  //
  // SIGTERM is used to stop CNC movement
  //
  if( signal(SIGTERM, &usb_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "UDB", "usb-SignalRegister(): SIGTERM ERROR");
     fCc = FALSE;
  }
  //
  if(fCc)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGTERM);
  }
  return(fCc);
}

#endif   //FEATURE_USB_DEBUG_TRACES
