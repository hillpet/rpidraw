/*  (c) Copyright:  2022  Patrn, Confidential Data
 *
 *  Workfile:           rpi_main.h
 *  Purpose:            Headerfile for main thread
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_MAIN_H_
#define _RPI_MAIN_H_

//
// External declarations
//
extern const char *pcLogfile;
extern const char *pcMapFile;
//
#define  TIMEOUT_MSECS                 500        // Host Timeout in msecs
//
#define  GUARD_CHECK_SECS              120         // Monitor interval
//

#endif /* _RPI_MAIN_H_ */
