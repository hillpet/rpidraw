/*  (c) Copyright:  2022 Patrn, Confidential Data
 *
 *  Workfile:           par_cnc.h
 *  Purpose:            Extract macro JSON reference list
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    26 Apr 2022:      Ported from rpicnc
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *
 *
**/

//
// CNC JSON reference list
//
//          ___Enum____   _____________ iFunction _______________    iPrec pcJson,        ____ iMapOffset _______          iSize      
EXTRACT_CNC(CNC_VAR_SEQU, JSN_INT|CNC_INT|CNC_DGM|CNC_PRM|CNC____,   0,    "CncSeq",      offsetof(RPICNC, iSeq),          0              )
EXTRACT_CNC(CNC_VAR_CNCD, JSN_TXT|CNC_INT|CNC____|CNC_PRM|CNC____,   0,    "CncDel",      offsetof(RPICNC, iCncDelay),     0              )
EXTRACT_CNC(CNC_VAR_CNCP, JSN_TXT|CNC_INT|CNC____|CNC_PRM|CNC____,   0,    "CncPul",      offsetof(RPICNC, iCncPulse),     0              )
EXTRACT_CNC(CNC_VAR_MODE, JSN_TXT|CNC_INT|CNC____|CNC_PRM|CNC_ALL,   0,    "Mode",        offsetof(RPICNC, iDrawMode),     0              )
EXTRACT_CNC(CNC_VAR_CURX, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   2,    "CurX",        offsetof(RPICNC, flXc),          0              )
EXTRACT_CNC(CNC_VAR_CURY, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   2,    "CurY",        offsetof(RPICNC, flYc),          0              )
EXTRACT_CNC(CNC_VAR_CURZ, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   2,    "CurZ",        offsetof(RPICNC, flZc),          0              )
EXTRACT_CNC(CNC_VAR_NEWX, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   2,    "NewX",        offsetof(RPICNC, flXn),          0              )
EXTRACT_CNC(CNC_VAR_NEWY, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   2,    "NewY",        offsetof(RPICNC, flYn),          0              )
EXTRACT_CNC(CNC_VAR_NEWZ, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   2,    "NewZ",        offsetof(RPICNC, flZn),          0              )
EXTRACT_CNC(CNC_VAR_STAX, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   2,    "PosXs",       offsetof(RPICNC, flXs),          0              )
EXTRACT_CNC(CNC_VAR_STAY, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   2,    "PosYs",       offsetof(RPICNC, flYs),          0              )
EXTRACT_CNC(CNC_VAR_STAZ, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   2,    "PosZs",       offsetof(RPICNC, flZs),          0              )
EXTRACT_CNC(CNC_VAR_ENDX, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   2,    "PosXe",       offsetof(RPICNC, flXe),          0              )
EXTRACT_CNC(CNC_VAR_ENDY, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   2,    "PosYe",       offsetof(RPICNC, flYe),          0              )
EXTRACT_CNC(CNC_VAR_ENDZ, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   2,    "PosZe",       offsetof(RPICNC, flZe),          0              )
EXTRACT_CNC(CNC_VAR_CANW, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   1,    "CanvasW",     offsetof(RPICNC, flCanvW),       0              )
EXTRACT_CNC(CNC_VAR_CANH, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   1,    "CanvasH",     offsetof(RPICNC, flCanvH),       0              )
EXTRACT_CNC(CNC_VAR_DRAW, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   1,    "DrawingW",    offsetof(RPICNC, flDrawW),       0              )
EXTRACT_CNC(CNC_VAR_DRAH, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   1,    "DrawingH",    offsetof(RPICNC, flDrawH),       0              )
EXTRACT_CNC(CNC_VAR_RADS, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   5,    "Radius",      offsetof(RPICNC, flRad),         0              )
EXTRACT_CNC(CNC_VAR_DISP, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   0,    "StRev",       offsetof(RPICNC, flSrev),        0              )
EXTRACT_CNC(CNC_VAR_MOVS, JSN_TXT|CNC_INT|CNC____|CNC_PRM|CNC____,   0,    "MoveSt",      offsetof(RPICNC, iMoveSteps),    0              )
EXTRACT_CNC(CNC_VAR_DRWS, JSN_TXT|CNC_INT|CNC____|CNC_PRM|CNC____,   0,    "DrawSt",      offsetof(RPICNC, iDrawSteps),    0              )
//
EXTRACT_CNC(CNC_VAR_CURL, JSN_TXT|CNC_LNG|CNC_DGM|CNC_PRM|CNC____,   0,    "CurL",        offsetof(RPICNC, lCurPos[0]),    0              )
EXTRACT_CNC(CNC_VAR_CURR, JSN_TXT|CNC_LNG|CNC_DGM|CNC_PRM|CNC____,   0,    "CurR",        offsetof(RPICNC, lCurPos[1]),    0              )
EXTRACT_CNC(CNC_VAR_CURP, JSN_TXT|CNC_LNG|CNC_DGM|CNC_PRM|CNC____,   0,    "CurP",        offsetof(RPICNC, lCurPos[2]),    0              )
//
EXTRACT_CNC(CNC_VAR_NEWL, JSN_TXT|CNC_LNG|CNC_DGM|CNC_PRM|CNC____,   0,    "NewL",        offsetof(RPICNC, lNewPos[0]),    0              )
EXTRACT_CNC(CNC_VAR_NEWR, JSN_TXT|CNC_LNG|CNC_DGM|CNC_PRM|CNC____,   0,    "NewR",        offsetof(RPICNC, lNewPos[1]),    0              )
EXTRACT_CNC(CNC_VAR_NEWP, JSN_TXT|CNC_LNG|CNC_DGM|CNC_PRM|CNC____,   0,    "NewP",        offsetof(RPICNC, lNewPos[2]),    0              )
//
EXTRACT_CNC(CNC_VAR_ACCX, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   0,    "AccX",        offsetof(RPICNC, flAccSet[0]),   0              )
EXTRACT_CNC(CNC_VAR_ACCY, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   0,    "AccY",        offsetof(RPICNC, flAccSet[1]),   0              )
EXTRACT_CNC(CNC_VAR_ACCZ, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   0,    "AccZ",        offsetof(RPICNC, flAccSet[2]),   0              )
EXTRACT_CNC(CNC_VAR_MINX, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   1,    "MinX",        offsetof(RPICNC, flSpdMin[0]),   0              )
EXTRACT_CNC(CNC_VAR_MINY, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   1,    "MinY",        offsetof(RPICNC, flSpdMin[1]),   0              )
EXTRACT_CNC(CNC_VAR_MINZ, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   1,    "MinZ",        offsetof(RPICNC, flSpdMin[2]),   0              )
EXTRACT_CNC(CNC_VAR_MAXX, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   1,    "MaxX",        offsetof(RPICNC, flSpdMax[0]),   0              )
EXTRACT_CNC(CNC_VAR_MAXY, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   1,    "MaxY",        offsetof(RPICNC, flSpdMax[1]),   0              )
EXTRACT_CNC(CNC_VAR_MAXZ, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   1,    "MaxZ",        offsetof(RPICNC, flSpdMax[2]),   0              )
