/*  (c) Copyright:  2022  Patrn, Confidential Data
 *
 *  Workfile:           rpi_usb.h
 *  Purpose:            Headerfile for rpi_usb functions
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from rpicnc
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_USB_H_
#define _RPI_USB_H_

#define  USB_CNX_TIMEOUT         10000
//
// Global prototypes
//
bool     USB_CloseRead           (RPIUSB *);
bool     USB_CloseWrite          (RPIUSB *);
bool     USB_ConnectedRead       (RPIUSB *);
bool     USB_ConnectedWrite      (RPIUSB *);
int      USB_CopyRecord          (RPIUSB *, char *, int);
void     USB_DumpData            (RPIUSB *);
void     USB_Flush               (RPIUSB *);
int      USB_GetCircularFree     (int, int, int);
int      USB_GetCircularCont     (int, int, int);
int      USB_GetCircularUsed     (int, int, int);
RPIUSB  *USB_GetDeviceInfo       (int);
bool     USB_OpenRead            (RPIUSB *);
bool     USB_OpenWrite           (RPIUSB *);
int      USB_Read                (RPIUSB *);
int      USB_Write               (RPIUSB *);

#endif  /*_RPI_USB_H_ */

