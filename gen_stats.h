/*  (c) Copyright:  2022 Patrn, Confidential Data 
 *
 *  Workfile:           gen_stats.h
 *  Purpose:            Status defines
 *  
 * 
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
 
EXTRACT_ST(GEN_STATUS_IDLE,         "Idle"            )
EXTRACT_ST(GEN_STATUS_ERROR,        "Error"           )
EXTRACT_ST(GEN_STATUS_TIMEOUT,      "HTTP Timeout"    )
EXTRACT_ST(GEN_STATUS_REJECTED,     "HTTP Rejected"   )
EXTRACT_ST(GEN_STATUS_REDIRECT,     "HTTP Redirected" )
