#################################################################################
# 
# makefile:
#	$(TARGET) - Raspberry Pi CNC RpiDraw utility for Pico CncDraw
#
#  add usage of:
#	cp && echo copy_is_okee || echo copy_is_not_okee
#	cp file.ext ./  || :  --> does NOTHING (:) if file.ext is not found
#
#	Copyright (c) 2022 Peter Hillen 
#################################################################################
BOARD=$(shell cat ../../RPIBOARD)

ifeq ($(BOARD),8)
#
# RPi#8 (Rev-2): PicoCNC Draw
#
	BOARD_OK=yes
	TARGET=rpidraw
	COMMON=../common
	OBJM=
	OBJX=
	DEFS=-D FEATURE_IO_NONE
	LIBM=
	LIBW=
endif

ifeq ($(BOARD),10)
#
# RPi#10 (Rev-3): 
#
	BOARD_OK=yes
	TARGET=rpidraw
	COMMON=../common
	OBJM=
	OBJX=
	DEFS=-D FEATURE_IO_NONE
	LIBM=
	LIBW=
endif

ifeq ($(BOARD),11)
#
# RPi#11 (Rev-3): 
#
	BOARD_OK=yes
	TARGET=rpidraw
	COMMON=../common
	OBJM=
	OBJX=
	DEFS=-D FEATURE_IO_NONE
	LIBM=
	LIBW=
endif

ifndef BOARD_OK
#
# All other RPi's: 
#
	BOARD_OK=yes
	TARGET=rpidraw
	COMMON=../common
	OBJM=
	OBJX=
	DEFS=-D FEATURE_IO_NONE
	LIBM=
	LIBW=
endif

CC		   = gcc
CFLAGS	= -O2 -Wall -g
DEFS	   += -D BOARD_RPI$(BOARD)
INCDIR	+= -I/opt/vc/include
INCDIR	+= -I/opt/vc/include/interface/vmcs_host/linux
INCDIR	+= -I/opt/vc/include/interface/vcos/pthreads -DRPI=1
INCDIR	+= -I$(COMMON)
LDFLGS	= -pthread
DESTDIR	= ./bin
LIBFLGS	= -L/usr/local/lib -L/usr/lib -L/opt/vc/lib -L$(COMMON)
LIBS		= -lrt
LIBS		+= $(LIBM) 
LIBS		+= $(LIBW) 
LIBCOM	= -lcommon
DEPS		= config.h
OBJ		= rpi_main.o globals.o rpi_func.o rpi_usb.o rpi_guard.o \
				rpi_json.o rpi_srvr.o rpi_page.o gen_http.o http_func.o \
				usb_db.o usb_wr.o rpi_udp.o rpi_cnc.o cnc_func.o rpi_sth.o
OBJ		+= $(OBJM)
OBJ		+= $(OBJX)

debug: LIBCOM = -lcommondebug
debug: DEFS += -D FEATURE_LOG_PRINTF -D DEBUG_ASSERT
debug: all

all: modules $(DESTDIR)/$(TARGET)

modules:
$(DESTDIR)/$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) $(DEFS) -o $@ $(OBJ) $(LIBFLGS) $(LIBS) $(LIBCOM) $(LDFLGS)
	strip $(DESTDIR)/$(TARGET)

sync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp -u /mnt/rpi/softw/$(TARGET)/*.c ./ 2>/dev/null
	cp -u /mnt/rpi/softw/$(TARGET)/*.h ./ 2>/dev/null
	cp -u /mnt/rpi/softw/$(TARGET)/makefile ./ 2>/dev/null
	cp -u /mnt/rpi/softw/$(TARGET)/rebuild ./ 2>/dev/null || :
	cp -u /mnt/rpi/softw/$(TARGET)/$(TARGET).service ./ 2>/dev/null || :

forcesync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp /mnt/rpi/softw/$(TARGET)/*.c ./ 2>/dev/null
	cp /mnt/rpi/softw/$(TARGET)/*.h ./ 2>/dev/null
	cp /mnt/rpi/softw/$(TARGET)/makefile ./ 2>/dev/null
	cp /mnt/rpi/softw/$(TARGET)/rebuild ./ 2>/dev/null || :
	cp /mnt/rpi/softw/$(TARGET)/$(TARGET).service ./ 2>/dev/null || :

ziplog:
	@echo "Zip logfile: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo gzip -c /usr/local/share/rpi/$(TARGET).log >>/usr/local/share/rpi/$(TARGET)logs.gz
	sudo rm /usr/local/share/rpi/$(TARGET).log
	sudo rm /mnt/rpicache/$(TARGET)/$(TARGET).log

dellog:
	@echo "Delete logfiles: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo rm -rf /usr/local/share/rpi/$(TARGET).log
	sudo rm -rf /mnt/rpicache/$(TARGET).log

delmap:
	@echo "Delete mapfiles: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo rm /usr/local/share/rpi/$(TARGET).map
	sudo rm /mnt/rpicache/$(TARGET)/$(TARGET).map

clean:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	rm -rf *.o
	rm -rf $(DESTDIR)/$(TARGET)

restart:
	@echo "Restart: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo systemctl stop $(TARGET).service
	@echo "Zip logfile: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	@echo `date`: Restart RPi-$(TARGET) Board=RPi#$(BOARD) >>/usr/local/share/rpi/$(TARGET).log
	sudo gzip -c /usr/local/share/rpi/$(TARGET).log 2>/dev/null >>/usr/local/share/rpi/$(TARGET)logs.gz
	sudo rm -rf /usr/local/share/rpi/$(TARGET).log
	sudo rm -rf /mnt/rpicache/$(TARGET)/$(TARGET).log
	sudo systemctl restart $(TARGET).service
	sudo systemctl daemon-reload

setup:
	sudo cp -u $(TARGET).service /lib/systemd/system/$(TARGET).service 2>/dev/null || :
	sudo systemctl enable $(TARGET).service

install:
	@echo "Install: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo systemctl stop $(TARGET).service
	cp $(DESTDIR)/$(TARGET) $(DESTDIR)/$(TARGET).latest 2>/dev/null || :
	@echo "Zip logfile: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	@echo `date`: New Install RPi-$(TARGET) Board=RPi#$(BOARD) >>/usr/local/share/rpi/$(TARGET).log
	sudo gzip -c /usr/local/share/rpi/$(TARGET).log 2>/dev/null >>/usr/local/share/rpi/$(TARGET)logs.gz
	sudo rm -rf /usr/local/share/rpi/$(TARGET).log
	sudo rm -rf /mnt/rpicache/$(TARGET)/$(TARGET).log
	sudo cp $(TARGET).service /lib/systemd/system/ 2>/dev/null || :
	sudo cp $(DESTDIR)/$(TARGET) /usr/sbin/$(TARGET) 2>/dev/null || :
	sudo systemctl restart $(TARGET).service
	sudo systemctl daemon-reload

.c.o: $(DEPS)
	$(CC) -c $(CFLAGS) $(DEFS) $(INCDIR) $< 
