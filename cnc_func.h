/*  (c) Copyright:  2022 Patrn, Confidential Data
 *
 *  Workfile:           cnc_func.h
 *  Purpose:            CNC functions header file
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    26 Apr 2022:      Ported from rpicnc
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *               
 *
 *
**/

#ifndef _CNC_FUNC_H_
#define _CNC_FUNC_H_

#define  CNC____        0x00000000     // No command
//
// HTTP Functions (callback through HTML or JSON)
//
#define  CNC_PIX        0x00000001     // List of pictures
#define  CNC_PRM        0x00000004     // List of All Parameters
#define  CNC_RMP        0x00000008     // Remove fotos
#define  CNC_DEF        0x00000010     // Defaults
#define  CNC_STP        0x00000020     // Stop
#define  CNC_DRW        0x00000040     // Draw Mode parameters
#define  CNC_DGM        0x00000080     // CNC specific vars theApp gets through unsolicited datagrams
#define  CNC_ALL        0x00000FFF     // All HTTP functions
//
// Var types
//
#define  CNC_TXT        0x00001000     // Text
#define  CNC_DBL        0x00002000     // Double prec
#define  CNC_FLT        0x00004000     // Floating point
#define  CNC_LNG        0x00008000     // Long
#define  CNC_INT        0x00010000     // Integer
//
// CNC Status line definitions:
// "STOP(00):[1023] N1 X0 Y0 Z0 \n"
//
#define  CNC_POS_ABS_STATUS      0     // Absolute Status index
#define  CNC_POS_ABS_AXIS        5     // Absolute Axis moving index
#define  CNC_POS_ABS_FREE        10    // Absolute Rcv buffer space index
#define  CNC_POS_REL_LINE        3     // Relative Line nr index
#define  CNC_POS_REL_XPOS        2     // Relative X pos   index
#define  CNC_POS_REL_YPOS        2     // Relative Y pos   index
#define  CNC_POS_REL_ZPOS        2     // Relative Z pos   index
//
#define  CNC_AXIS_X              0x01  // X Axis status position
#define  CNC_AXIS_Y              0x02  // Y Axis status position
#define  CNC_AXIS_Z              0x04  // Z Axis status position
#define  CNC_AXIS_ALL            0x07  //   Axis status mask
//
typedef enum _pico_vars_
{
#define  EXTRACT_PICO(a,b,c,d,e)       a,
#include "par_pico.h"   
#undef   EXTRACT_PICO
} PICOVARS;
//
typedef struct CNCPAR
{
   int         iFunction;
   int         iPrec;
   const char *pcJson;
   int         iMapOffset;
   int         iSize;
}  CNCPAR;
//
typedef struct CNCPICO
{
   int         iSeconds;
   int         iRunAxis    [NUM_AXIS];
   int         iStopDel    [NUM_AXIS];
   int         iPosCur     [NUM_AXIS];
   int         iPosNew     [NUM_AXIS];
   double      flSpeedAct  [NUM_AXIS];
   double      flSpeedLin  [NUM_AXIS];
   double      flSpeedSet  [NUM_AXIS];
   double      flSpeedMax  [NUM_AXIS];
   double      flScaleSet  [NUM_AXIS];
   double      flAccelSet  [NUM_AXIS];
   int         iMoveRup    [NUM_AXIS];
   int         iMoveLin    [NUM_AXIS];
   int         iMoveRdn    [NUM_AXIS];
   double      fTimeRup    [NUM_AXIS];
   double      fTimeLin    [NUM_AXIS];
   double      fTimeRdn    [NUM_AXIS];
}  CNCPICO;
//
typedef struct VARPICO
{
   const char *pcParm;
   int         iElements;
   int         iOffset;
   bool      (*pfCallback)(char *, const struct VARPICO *, CNCPICO *);
}  VARPICO;

//
//
// Global prototypes
//
bool     CNC_InitParms              (void);
char    *CNC_GetFromValue           (const CNCPAR *, char *, int);
bool     CNC_ParsePicoStatus        (CNCVAR *, char *);
bool     CNC_UpdatePicoVariable     (CNCPICO *, char *);

#endif   //_CNC_FUNC_H_
