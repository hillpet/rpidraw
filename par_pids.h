/*  (c) Copyright:  2022  Patrn, Confidential Data
 *
 *  Workfile:           par_pids.h
 *  Purpose:            Headerfile for the RPI thread PIDs
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *    24 Apr 2022:      Add USB support
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 **/

//          ePid           pcPid          pcHelp
EXTRACT_PID(PID_HOST,      "Host   ",     "Host")
EXTRACT_PID(PID_USBWR,     "USB Wr ",     "USB Write")
EXTRACT_PID(PID_USBDB,     "USB Db ",     "USB Debug")
EXTRACT_PID(PID_GUARD,     "Guard  ",     "Guard")
EXTRACT_PID(PID_SRVR,      "Server ",     "HTTP Server")
EXTRACT_PID(PID_UDP,       "Udp    ",     "UDP Daemon")
EXTRACT_PID(PID_CNC,       "Cnc    ",     "CNC Daemon")

