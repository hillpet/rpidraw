/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           pagescnc.h
 *  Purpose:            CNC server specific dynamic HTML snd JSON pages
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//          tUrl,                   tType,      iTimeout,   iFlags,           pcUrl,                  pfDynCb;
EXTRACT_DYN(DYN_CNC_NONE_ALL,       HTTP_HTML,  0,          DYN_FLAG_PORT,    "all",                  srvr_DynPageCncCommand              )
EXTRACT_DYN(DYN_CNC_NONE_CMD,       HTTP_JSON,  0,          DYN_FLAG_PORT,    "cnc",                  srvr_DynPageCncCommand              )
EXTRACT_DYN(DYN_CNC_JSON_CMD,       HTTP_JSON,  0,          DYN_FLAG_PORT,    "cnc.json",             srvr_DynPageCncCommand              )
EXTRACT_DYN(DYN_CNC_HTML_CMD,       HTTP_HTML,  0,          DYN_FLAG_PORT,    "cnc.html",             srvr_DynPageCncCommand              )
