#!/usr/bin/env bash
#################################################################################
# rebuild
#	Simple rebuild and install script
#
#	Copyright (c) 2022 Patrn.nl
#################################################################################
# This file is part of the RpiDraw project:
#
# This script will rebuild and restart all files and service in this case.
# To rebuild all files, run:
#
# $ rpi-raptor
# 	 connect to //Raptor/proj/
# $	mkdir ~/proj
# $	cd ~/proj
# $	mkdir common
# $	mkdir rpidraw
# $	cd rpidraw
# $	mkdir bin
# $ mc 
# 	 copy from //Raptor/Rpi/softw/common:
# 		makefile
# 	 copy from //Raptor/Rpi/softw/rpidraw:
# 		makefile
# $	cd ~
# $	./rebuild all
#
#################################################################################

check_make_ok() {
  if [ $? != 0 ]; then
    echo ""
    echo "Make Failed..."
    echo "Please check the messages and fix any problems."
    echo ""
	 exit $?
  fi
}

if [ x$1 = "xdirs" ]; then
	if ! test -d proj
	then
	  mkdir proj
	  mkdir proj/common
	  mkdir proj/rpidraw
	  mkdir proj/rpidraw/bin
	fi
fi

if [ x$1 = "xinstall" ]; then
  echo
  echo "rpidraw install"
  echo "==============="
  sudo cp bin/rpidraw /usr/bin/rpidraw
  check_make_ok
  echo "Install rpidraw done."
  echo ""
fi

if [ x$1 = "xall" ]; then
  echo
  echo "RpiDraw Manager Re-Build script"
  echo "==============================="
  echo
  echo "Rebuild Common Library"
  cd ~/proj/common
  make sync
  make clean
  make debug
  check_make_ok
  make clean
  make all
  check_make_ok
  echo "Rebuild Common Library done."
#
  echo "Rebuild rpidraw (CNC Draw manager)"
  cd ~/proj/rpidraw
  make sync
  make clean
  make debug
  check_make_ok
  make clean
  make all
  check_make_ok
  echo "Rebuild rpidraw done."
  echo ""
fi

if [ x$1 = "xdebug" ]; then
  echo
  echo "rpidraw Re-Build script for DEBUG only!"
  echo "======================================="
  echo
  echo "Rebuild Common DEBUG Library"
  cd ~/proj/common
  make sync
  make clean
  make debug
  check_make_ok
  echo "Rebuild rpidraw"
  cd ~/proj/rpidraw
  make sync
  make clean
  make debug
  check_make_ok
  echo "Rebuild rpidraw DEBUG done."
  echo ""
fi

if [ x$1 = "xcommon" ]; then
  echo
  echo "common Re-Build script"
  echo "======================"
  echo
  echo "Rebuild Common Library"
  cd ~/proj/common
  make sync
  make clean
  make debug
  check_make_ok
  make clean
  make all
  check_make_ok
  echo "Rebuild done."
  cd ~/proj/rpidraw
  echo ""
fi

if [ x$1 = "x" ]; then
	echo ""
	echo "Rpi Draw rebuilder. Use to create all necessary files on a new or modified"
	echo "installation. Usage: $0 [all | debug | dirs | common | install]"
	echo ""
fi
