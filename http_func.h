/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           http_func.h
 *  Purpose:            Headerfile for http_func
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _HTTP_FUNC_H_
#define _HTTP_FUNC_H_

bool  RPI_BuildHtmlMessageBody               (NETCL *);
bool  RPI_BuildJsonMessageBody               (NETCL *, int);
bool  RPI_BuildJsonMessageArgs               (NETCL *, int);
bool  RPI_CheckDelete                        (void);
int   RPI_CollectParms                       (NETCL *, FTYPE);
void  RPI_ReportBusy                         (NETCL *);
void  RPI_ReportError                        (NETCL *);

#endif /* _HTTP_FUNC_H_ */
