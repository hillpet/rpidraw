/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_srvr.h
 *  Purpose:            HTTP Server header file
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Ported from PiKrellMan
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_SRVR_H_
#define _RPI_SRVR_H_

int      SRVR_Init         (void);


#endif /* _RPI_SRVR_H_ */
