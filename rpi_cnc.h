/*  (c) Copyright:  2022  Patrn, Confidential Data
 *
 *  Workfile:           rpi_cnc.h
 *  Purpose:            Headerfile for CNC thread
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       Pico CncDraw 
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    16 Apr 2022:      Created from template
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_CNC_H_
#define _RPI_CNC_H_

//
// Global functions
//
int CNC_Init                  (void);
int CNC_BuildJsonObject       (char *, int);

#endif //_RPI_CNC_H_
